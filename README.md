# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* License

None is provided to anyone yet, except that which is implied by publication here at BitBucket.  This BitBucket account is the only publication of this project done by its owner, modelSoftTech.  You have the right to read and study every document which I publish here in this BitBucket account, keith78201 / XComponents. You have the right to make one archival copy of each and every document that I publish here, provided you obtain it legally through the means provided by https://bitbucket.org.  You may make a project from the source code I publish here, run it, build it into an executable jar and run the jar, change the code and build it, and test it.  Testing it includes using it as an API in your own projects by including it as a library jar, so there is no need to paste in the source code or the files containing it.  Modifications can be made in your archival copy of the source. 

Violation of these licensing terms is a felony under U.S. law.

* Software Development Philosophy and Methodology:

Scrum:   http://en.wikipedia.org/wiki/Scrum_(software_development)

"Scrum was first defined as "a flexible, holistic product development strategy where a development team works as a unit to reach a common goal" as opposed to a "traditional, sequential approach" in 1986 by Hirotaka Takeuchi and Ikujiro Nonaka in the "New New Product Development Game".[1] Takeuchi and Nonaka later argued in "The Knowledge Creating Company"[2] that it is a form of "organizational knowledge creation, [...] especially good at bringing about innovation continuously, incrementally and spirally"."


* Quick summary

XComponents are JavaFX extensions which provide sophisticated PropertyList, TreeView, and other components with data models that do real-time inter-Node updates based on dependencies between the underlying properties.  

Each XComponent is either a Node or Pane extension, meaning it can be used in any JavaFX application as such.  There will be one special XComponent which is an application designer, allowing the user to select GUI templates and drag and drop XComponents and ordinary JavaFX components into them.  Layout templates so far include a property sheet style layout and a collapsible tree layout.  The collapsible tree layout is particularly exciting since you can fit a stack of IRS forms into a single Pane with it.  To see the advantages of the property list layout, compare it to that in ControlsFX, also hosted here at BitBucket.  I am open to integration discussions if my XComponents compete favorably against their ControlsFX.  Their address here is https://bitbucket.org/controlsfx/controlsfx/.  

I am currently adding add drag and drop layout capabilities, and will have an application designer prototype available here within a few weeks.

The underlying data model is independent of the views and will be switchable between different views and view layouts, including list, tree, and in the future grid or graph.  The underlying data model must be  homologous with the views that can display it, so for example a tree view can only accommodate a tree data structure.  However, these are XComponents, so any tree view will accommodate any tree data structure, and visa-versa.  This is completely consistent with strong typing, a principle essential to application security.   
A dependency graph is an interesting possibility for organizing work flows of interdependent activity views.

The underlying data models are themselves real-time models, meaning they can represent real-world systems or entities, which have inherent real-time dependencies between their properties.  In a branch circuit in a home electrical system, for instance, the power level depends on the resistances of the loads connected.  Many appliances have variable load levels, and on off switches as well as internal heat change the circuit path resistances in the various parallel branches of the circuit.  If you want to display the current power consumption in real time, you need an instance of a Class that represents a circuit.  For both input flexibility and generality, you need to store all of the properties needed to compute any others.  There are mutual inter-dependencies in the Ohms law properties and their derivatives, Voltage, Current, Resistance, Power, Time and energy.  To keep the view up to date at all times, any change which is input must be used immediately to recompute all properties whose value depends on the changed property.  The data model, the Ohms's law class instance, which is plugged into the view, therefore must be coded to do this, as well as to deliver these changes to the current view.  It is obvious that JavaFX's embedded view data models will not do this, since a class can have only one toString() to use to obtain data from it.  Further, JavaFX violates the Model-View-Control architectural standard by even having data embedded in the views.  ControlsFX also lacks this separation, and does not do real-time modelling.

I will be adding local and online JDBC DataSource mapping to the underlying data models.  Particular focus in the development of online data source search workflows will be placed on finding data sets that can be mapped into the ObservableList structure used by JavaFX's charts and graphs.  Generally, a table or result set with numerical columns which express functional dependencies between two variables are candidates.  A single column of result values constitutes a single data series if the data it was computed based on is also known.  If the series is a function of time, there will be a series of values with a time value and a measured value.  A set of employee records that includes age and yearly income information can be made into a series of age ranges with yearly average income for a barchart, for instance.  This can also be viewed as an XY graph if average yearly income is graphed as a function of age.  The DataSources will be filterable via SQL like select parameter settings in in the application designer's GUI.

One problem which will need solution is that professional data bases follow a standard called "normalization" which removes all data inter dependencies possible from records.  Computed values are left to the CPU, rather than placed on the disk.  This standard comes form a time of serious computing resource constraints, however, and since the price of data storage has dropped by many orders of magnitude since then, it might no longer be an enforced standard.

What I have in this repository so far, 08-02-2014, is an application which is a demo of the components.

If you like what you see here, please endorse me a LinkedIn.  My account name there is "Keith Watson".

Sample Documentation, TreeDescriptor Class:

This class stores a tree of controls for display in a JavaFX TreeView along with a parallel
tree of data.  It is a Tree Template for control layout.  This is the layer between the user 
programmer, via an API, or the GUI layout designer and the application created by either.  The 
layout designer will use the same API available to a programmer using this package as a library jar.
The purpose of this Class is to store the description of the controls and their layout which the
designer wants to use for user input and output in an application, along with the control data
buffers that store the data entered.  It is necessary to store a copy of the data outside of the 
Node's own data model in order for the view (Nodes + Layout) to be changed for the same set of 
data. This can be done using an "Adaptor" constructor of the other view template class which 
takes this view's data and restructures it into the other's control set and layout.  Since many 
controls are possible for any particular type of data, the GUI designer engine can offer layout 
template options that the user selects from, and then selects the particular controls they want 
to use for particular data along with their location in the layout.  This architecture provides separation between the GUI view component and its data, which retains its state between invocations of this view or its use on others.  The inherently heavyweight components of these view can then also be recycled between uses.


* XComponents as Enterprise Resource Planning platform components:

The application designer will allow the user to drag and drop default and designed Nodes (components) or Panes (Layouts, which are extensions of Nodes) into the tree, allowing infinite recursion, making it able to accommodate all of the input and output views any organization needs in a single hierarchical structure: A complete Enterprise Resource Planning system designer. Then it will have the ability to drag and drop DataSource connections directly to the Views, and drag and drop analytics into the stream, i.e. DataSource => Analytics => Custom Designed Views.  The DataSources or their individual components will be mapped by the user into the parameters that the Analytics require.  The output of the Analytics will be mapped by the user into the views.  Data mapping to views will be done when raw DataSources or Analytics are dropped into views. This is a fully general programming architecture, Input-Process-Output.  The only code required will be for the Analytics that the customer doesn't already have on hand or have available in the Cloud, since any java method is plug-able for use as an Analytic. 

Since the data architecture with which models are created is a real-time modelling one, real-time analytics are always available with no additional data source access except for the real-time change notifications which automatically update the models. I will be implementing a DataSource aggregator using Google App Engine which will do pull real-time from existing DataSources and then push changes out to the apps designed with this engine.

I should emphasize that the entire application designer is GUI and Drag and Drop, the only code being analytics and data modelling classes that the programmer-user does not already have. 

You in charge.

Real-Time, A.I., drag and drop application designer. Simple. 

For complete information on the requirements an ERP must meet, see: http://en.wikipedia.org/wiki/Enterprise_resource_planning.



* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

The setup is very easy.  In the right panel, click Downloads.   In the center panel of the next page, click on the link "Download repository".  Select a destination on you local drive and click the Save button.  Unzip the file youjust downloaded and navigate to the src directory. inside of it will be all of the source code packages.  I use NetBeans, so I will provide the instructions for setting up a NetBeans JavaFX project.  The steps are probably just as simple for other IDEs.  Create a JavaFX project of the type JavaFX application.  Delete its main class, that's the class with the public static main( String[] args ) method in it.  In Windows, place the NetBeans window side by side with the explorer window showing the source packages, and then drag each package folder into the Source folder in the project.  Right click the project and click Run.  NetBeans will ask you for the main() class showing only one selection.  Pick it and run the program.  

You now have the source code in a NetBeans project and can work with it.  


* Configuration

None required.

* Dependencies

Everything is included in the source code here.

* Database configuration

No database is currently needed or used. 

* How to run tests

Write your own.

* Deployment instructions

You are not permitted to deploy or distribute.


### Contribution guidelines ###

Contact me at my email listed below if you are interested in working on this project.

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Keith Watson, modelSoftTech@gmail.com or georgekeithwatson@gmail.com, at LinkedIn I'm Keith Watson.

* Other community or team contact

None. I (Keith Watson) am the only admin on this project, and the sole principle of the business organization that owns it, modelSoftTech.