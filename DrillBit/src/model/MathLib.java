/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          MathLib
 * Author           George Keith Watson
 * Date Started:    July 12, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;

/**
 *
 * @author George Keith Watson
 */
public class MathLib {
    
    public static final int[] sequenceOfPrimes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 
                                                    59, 61, 67, 71, 73, 79, 83, 89, 97, 101 };
    
    public static final int checkPrimes()    {
        System.out.println(  "checkPrimes()" );
        for ( int i = 1; i < sequenceOfPrimes.length; i++ ) {
            for ( int j = 0; j < i; j++ )   {
                if ( sequenceOfPrimes[i] % sequenceOfPrimes[j] == 0 ) {
                    System.out.println(  "Number is not Prime: " + sequenceOfPrimes[i] );
                    return sequenceOfPrimes[i];
                }
            }
        }
        return 0;
    }
    
    static public final void generatePrimes( final Long high )  {
        //OPTIONAL, better for large numbers.
    }
    
    /**
     * Find the least common divisor of two integers by finding the prime factors common to both and 
     * multiplying them.
     * @param left
     * @param right
     * @return 
     */
    static public final long GreatestCommonDivisor( final long left, final long right )    {
        long lcd    = 1;
        for ( int i = 0; i < sequenceOfPrimes.length; i++  )    {
            long leftArg    = left;
            long rightArg = right;
            while ( leftArg % sequenceOfPrimes[ i ] == 0 && rightArg % sequenceOfPrimes[ i ] == 0 ) {
                leftArg /= sequenceOfPrimes[ i ];
                rightArg /= sequenceOfPrimes[ i ];
                lcd *= sequenceOfPrimes[ i ];
            }
        }
        return lcd;
    }
    
}
