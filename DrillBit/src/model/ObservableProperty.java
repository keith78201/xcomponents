/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ObservableProperty
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import control.ControlType;
import control.EnumId;
import control.SimplePropertyType;
import model.filter.ValueFilter;

/**
 *
 * @author George Keith Watson
 * @param <DATA_TYPE>
 */
public class ObservableProperty < DATA_TYPE > extends XNode < DATA_TYPE > {
    
    private final   ControlType         controlType;
    private final   SimplePropertyType  propertyType;
    
    public ObservableProperty( final String name, final ControlType controlType, 
                                final SimplePropertyType type, final ValueFilter valueFilter )    {
        super( name, ( DATA_TYPE ) valueFilter.getCurrentValue(), type, valueFilter );
        this.controlType        = controlType;
        this.propertyType       = type;
    }
    
    public ControlType getControlType() {
        return controlType;
    }

    public SimplePropertyType getPropertyType() {
        return propertyType;
    }

}
