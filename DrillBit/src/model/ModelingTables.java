/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ModelingTables
 * Author           George Keith Watson
 * Date Started:    July 15, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;
import view.nodes.InteractiveModelingTable;

/**
 *
 * @author George Keith Watson
 */
public class ModelingTables extends ArrayList< InteractiveModelingTable< ? extends TableRecord > >{
    
}
