/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          XNode
 * Author           George Keith Watson
 * Date Started:    July 31, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      2014-08-01: Graph
 *          All edges are directed from this to the XNode in the ajacency list.  If an edge is bidirectional,
 *          it must be specified in both XNode's ajacency lists.
 */

package model;

import control.SimplePropertyType;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javafx.scene.Node;
import model.filter.EmailDescriptor;
import model.filter.FractionDescriptor;
import model.filter.IpAddressDescriptor;
import model.filter.PhoneNumberDescriptor;
import model.filter.PostalCodeDescriptor;
import model.filter.UrlDescriptor;
import model.filter.ValueFilter;


/**
 * This is the base class for all list, tree, and graph control and model node layout structures.
 * @author George Keith Watson
 * @param <DATA_TYPE>
 */
public class XNode < DATA_TYPE >{
    
    public final static String ATTR_NAME_PREF_WIDTH         = "prefred width";
    public final static String ATTR_NAME_PREF_HEIGHT        = "prefred height";
    public final static String ATTR_NAME_MAX_WIDTH          = "max width";
    public final static String ATTR_NAME_MAX_HEIGHT         = "max height";
    public final static String ATTR_NAME_MIN_WIDTH          = "min width";
    public final static String ATTR_NAME_MIN_HEIGHT         = "min height";
    public final static String ATTR_NAME_X_TRANSLATION      = "x translation";
    public final static String ATTR_NAME_Y_TRANSLATION      = "y translation";
    public final static String ATTR_NAME_CSS_STYLE          = "css style";
    
    private final String                name;
    private final ArrayList< XNode >    edges               = new ArrayList<  >(); 
    private final TreeMap< String, XNode > edgeMap          = new TreeMap<  >(); 
    private final TreeMap< String, Attribute > attributes   = new TreeMap<  >(); 
    private       DATA_TYPE             data;
    private       Node                  viewRef             = null;
    private final SimplePropertyType    type;
    private final ValueFilter           valueFilter;

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public XNode( final String name, final DATA_TYPE value, final SimplePropertyType type,
            final ValueFilter valueFilter)   {
        this.name           = name;
        this.type           = type;
        this.valueFilter    = valueFilter;
        if ( name == null || name.length() == 0 || attributes.containsKey( name ) || value == null )
            return;
        data = value;
        if ( !validType( value, type ) )
            data    = null;
    }
    
    public ValueFilter getValueFilter() {
    return valueFilter;
    }

    public String getPropertyView() {
        if ( valueFilter.getCurrentValue() != null )
            return valueFilter.getCurrentValue().toString();
        else return "";
    }

private Boolean validType( final DATA_TYPE value, final SimplePropertyType type )  {
        if ( !data.getClass().isInstance( value ) ) 
            return false;
        switch( type )  {
            case BOOLEAN:
                return Boolean.class.isInstance( value );
            case BYTE:
                return Byte.class.isInstance( value );
            case CHARACTER:
                return Character.class.isInstance( value );
            case DATE:
                return Date.class.isInstance( value );
            case DOUBLE:
                return Double.class.isInstance( value );
            case EMAIL:
                return EmailDescriptor.class.isInstance( value );
            case ENUM:
                return Enum.class.isInstance( value );
            case FLOAT:
                return Float.class.isInstance( value );
            case FRACTION:
                return FractionDescriptor.class.isInstance( value );
            case INTEGER:
                return Integer.class.isInstance( value );
            case IP_ADDR:
                return IpAddressDescriptor.class.isInstance( value );
            case LONG:
                return Long.class.isInstance( value );
            case PHONE:
                return PhoneNumberDescriptor.class.isInstance( value );
            case POSTAL_CODE:
                return PostalCodeDescriptor.class.isInstance( value );
            case SHORT:
                return Short.class.isInstance( value );
            case STRING:
                return String.class.isInstance( value );
            case TIME:
                return Date.class.isInstance( value );
            case URL:
                return UrlDescriptor.class.isInstance( value );
            case PROPERTY_NAME:
                return String.class.isInstance( value );
        }
        return false;
    }

    public final DATA_TYPE getData() {
        return data;
    }


    public SimplePropertyType getType() {
        return type;
    }
    
    /**
     * If a programmer wants to assign an assignment compatible type to the value, rather than the 
     * value's declared type, then they can declare that type each time they do.  <br>
     * This helps to enhance strict typing.   No assignment is made if the value is not assignment
     * compatible with the type specified.  For instance, if the type of the data is Integer, a programmer
     * could assign a Short or Character to it.  To make sure that this is the type they are actually
     * putting in, they can specify the more particular type using the "type" parameter.  
     * @param value
     * @param type
     */
    public final void setData( final DATA_TYPE value, final SimplePropertyType type ) {
        data    = value;
        if ( !validType( value, type ) )
            data    = null;
    }
    public final String getName() {
        return name;
    }

    public final Node getViewRef() {
        return viewRef;
    }

    public final void setViewRef( final Node viewRef) {
        this.viewRef = viewRef;
    }
    
    public final XNode addEdge( final XNode xNode ) {
        if ( xNode != null && !edges.contains( xNode ) )    {
            edges.add( xNode );
            edgeMap.put( xNode.getName(), xNode );
        }
        return xNode;
    }
    
    public final XNode removeEdge( final XNode xNode ) {
        if ( xNode != null && edges.contains( xNode ) )     {
            edges.remove( xNode );
            edgeMap.remove( xNode.getName() );
        }
        return xNode;
    }
    
    public final XNode getEdge( final String name ) {
        return edgeMap.get( name );
    }
    
    public final XNode getEdge( final Integer index ) {
        return edges.get( index );
    }
    
    public final ArrayList< XNode > getEdges()  {
        return edges;
    }
    
    
    /**
     * Add an attribute to this XNode.  This method ignores the request if EVERYTHING is not in order,
     * such as the name being null or already being in the attributes, or the value not being assignment
     * compatible with the type specified.
     * @param name
     * @param value
     * @param type
     * @return 
     */
    public final Attribute addAttribute( final String name, final Object value, final SimplePropertyType type )   {
        if ( name == null || name.length() == 0 || attributes.containsKey( name ) || value == null )
            return null;
        switch( type )  {
            case BOOLEAN:
                if ( Boolean.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Boolean > ( name, ( Boolean )value ) );
                    return attributes.get( name );
                }
                return null;
            case BYTE:
                if ( Byte.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Byte > ( name, ( Byte )value ) );
                    return attributes.get( name );
                }
                return null;
            case CHARACTER:
                if ( Character.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Character > ( name, ( Character )value ) );
                    return attributes.get( name );
                }
                return null;
            case DATE:
                if ( Date.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Date > ( name, ( Date )value ) );
                    return attributes.get( name );
                }
                return null;
            case DOUBLE:
                if ( Double.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Double > ( name, ( Double )value ) );
                    return attributes.get( name );
                }
                return null;
            case EMAIL:
                if ( EmailDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< EmailDescriptor > ( name, ( EmailDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case ENUM:
                if ( Enum.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Enum > ( name, ( Enum )value ) );
                    return attributes.get( name );
                }
                return null;
            case FLOAT:
                if ( Float.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Float > ( name, ( Float )value ) );
                    return attributes.get( name );
                }
                return null;
            case FRACTION:
                if ( FractionDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< FractionDescriptor > ( name, ( FractionDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case INTEGER:
                if ( Integer.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Integer > ( name, ( Integer )value ) );
                    return attributes.get( name );
                }
                return null;
            case IP_ADDR:
                if ( IpAddressDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< IpAddressDescriptor > ( name, ( IpAddressDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case LONG:
                if ( Long.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Long > ( name, ( Long )value ) );
                    return attributes.get( name );
                }
                return null;
            case PHONE:
                if ( PhoneNumberDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< PhoneNumberDescriptor > ( name, ( PhoneNumberDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case POSTAL_CODE:
                if ( PostalCodeDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< PostalCodeDescriptor > ( name, ( PostalCodeDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case SHORT:
                if ( Short.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Short > ( name, ( Short )value ) );
                    return attributes.get( name );
                }
                return null;
            case STRING:
                if ( String.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< String > ( name, ( String )value ) );
                    return attributes.get( name );
                }
                return null;
            case TIME:
                if ( Date.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< Date > ( name, ( Date )value ) );
                    return attributes.get( name );
                }
                return null;
            case URL:
                if ( UrlDescriptor.class.isInstance( value ) )    {
                    attributes.put( name, new Attribute< UrlDescriptor > ( name, ( UrlDescriptor )value ) );
                    return attributes.get( name );
                }
                return null;
            case PROPERTY_NAME:
                if ( String.class.isInstance( value ) ) {
                    attributes.put( name, new Attribute< String > ( name, ( String )value ) );
                    return attributes.get( name );
                }
                return null;
            
        }
        return null;
    }
    
    public final void addAllAttributes( final Map< String, Attribute > attributes )  {
        this.attributes.putAll( attributes ); 
    }
    
    public final Object setAttribute( final String name, final Object value )    {
        if ( name != null && name.length() > 0 && attributes.get( name ) != null && 
                                attributes.get( name ).getValue().getClass().isInstance( value ) )     {
            attributes.get( name ).setValue( value );
        }
        return null;
    }
    
    public final void setAllAttributes( final Map< String, Attribute > attributes )   {
        final Iterator< String > iterator = attributes.keySet().iterator();
        while ( iterator.hasNext() )    {
            final String key    = iterator.next();
            this.attributes.put(  key, attributes.get( key ) );
        }
    }
    
    public final Attribute removeAttribute(  final String name )    {
        if ( name != null && name.length() > 0 && attributes.get( name ) != null )
            return attributes.remove( name );
        return null;
    }
    
    public final void clearAttributes()    {
        attributes.clear();
    }
    
    public final void registerListener( final String eventId, final PropertyChangeListener listener ) {
        propertyChangeSupport.addPropertyChangeListener( eventId, listener );
    }
    public final void registerListener( final PropertyChangeListener listener ) {
        propertyChangeSupport.addPropertyChangeListener( listener );
    }
    public final void sendMessage( final String eventId, final Object message ) {
        propertyChangeSupport.firePropertyChange( eventId, this, message );
    }
    
    
}
