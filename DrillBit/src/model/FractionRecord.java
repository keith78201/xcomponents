/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          FractionRecord
 * Author           George Keith Watson
 * Date Started:    July 13, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public class FractionRecord  extends TableRecord {
    
    public static final String VIEW_FILD_NAME_FRACTION      = "Fraction";
    public static final String VIEW_FILD_NAME_DECIMAL       = "Decimal";
    public static final String VIEW_FILD_NAME_PROPER        = "Proper";
    public static final String VIEW_FILD_NAME_TERMINOLOGY   = "Terminology";
    public static final String VIEW_FILD_NAME_REDUCED       = "Reduced";
    
    private Long numerator      = null;
    private Long denominator    = null;
    private Long gcd            = null;
    
    private FractionFormat      fractionFormat  = FractionFormat.NORMAL;

    public FractionRecord( final Long numerator, final Long denominator )   {
        this.numerator      = numerator;
        this.denominator    = denominator;
        setGreatestCommonDivisor();
    }
    
    /**
     * 
     * @param fraction Normal String representation of a fraction: N / D.  Must be in this form.
     */
    public FractionRecord( final String fraction )   {
        final String[] parts    = fraction.split( "/" );
        
        if ( parts.length == 2 )    {
            numerator   = Long.parseLong( parts[0].trim() );
            denominator = Long.parseLong( parts[1].trim() );
        }
    }
    
    public FractionRecord( final FractionRecord fractionRecord ) {
        this.denominator    = fractionRecord.denominator;
        this.numerator      = fractionRecord.numerator;
        setGreatestCommonDivisor();
    }

    public void setNumerator(Long numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(Long denominator) {
        this.denominator = denominator;
    }

    public String getNumerator() {
        return numerator == null ? "" : numerator.toString();
    }

    public String getDenominator() {
        return denominator == null ? "" : denominator.toString();
    }
    
    public Double getValue( )   {
        return numerator == null || denominator == null  ? 0 : ( double )numerator/ denominator;
    }
    
    public String getFraction()    {
        return numerator == null || denominator == null  ? "" : numerator + " / " + denominator;
    }
    
    public String getDecimal()    {
        return numerator == null || denominator == null  ? "" : ( ( Double ) ( ( double )numerator / denominator ) ).toString();
    }
    
    public String getProper()    {
        return isProper() ? "true" : "false";
    }
    
    public String getMixed()    {
        if ( numerator == null || denominator == null )
            return "";
        final StringBuffer text = new StringBuffer();
        text.append( integerDivide().toString() );
        if ( modulus() != 0 )    {
            text.append(" ").append( modulus() ).append("/").append( denominator );
        }
        return text.toString();
    }
    

    public Long getGcd() {
        return gcd;
    }

    public void setGcd(Long gcd) {
        this.gcd = gcd;
    }

    public void generateTerminology( final ObservableList< String > languageList )    {
        languageList.set( 0, numerator + " over " + denominator );
        languageList.set( 1, numerator + " divided by " + denominator );
        languageList.set( 2, numerator + " items of a set of " + denominator + " items" );
        languageList.set( 3, ( double )numerator / ( double )denominator * 100 + " percent" );
    }
    
    public final String getReduced()    {
        if ( numerator == null || denominator == null )
            return "";
        setGreatestCommonDivisor();
        final StringBuffer reducedFractionText = new StringBuffer();
        reducedFractionText.append( ( ( Long )( numerator / gcd ) ).toString() ).append( "/" ).append( ( ( Long )( denominator / gcd ) ).toString() );
        return reducedFractionText.toString();
    }
    
    
    public FractionFormat getFractionFormat() {
        return fractionFormat;
    }

    public void setFractionFormat(FractionFormat fractionFormat) {
        this.fractionFormat = fractionFormat;
    }
    
    @Override
    public String toString()    {
        switch( fractionFormat )  {
            case NORMAL:
                return getFraction();
            case MIXED:
                return getMixed();
            case REDUCED:
                return getReduced();
            case TEXT:
                return numerator + " over " + denominator;
            default:
                return "";
        }
    }
    

    public Boolean isProper( )  {
        if ( numerator == null || denominator == null )
            return false;
        return numerator <= denominator;
    }
    
    private Long integerDivide()   {
        if ( numerator == null || denominator == null )
            return 0l;
        return  numerator / denominator;
    }
    
    private Long modulus()   {
        return  numerator % denominator;
    }
    
    private void setGreatestCommonDivisor() {
        if ( numerator == null || denominator == null )
            gcd = 1l;
        else
            gcd    = MathLib.GreatestCommonDivisor( numerator, denominator );
    }
    
    /**
     * Implicitly maps from the field names provided in the table column creation step to the internal
     * record buffer fields.  The table field names must therefore be provided by this class.
     * @param name
     * @param value 
     */
    @Override
    public void setField( final String name, final Object value )  {
        switch( name )  {
            case VIEW_FILD_NAME_FRACTION:
                break;
            case VIEW_FILD_NAME_DECIMAL:
                break;
            case VIEW_FILD_NAME_PROPER:
                break;
            case VIEW_FILD_NAME_TERMINOLOGY:
                break;
            case VIEW_FILD_NAME_REDUCED:
                break;
        }
    }

    @Override
    public String getField(String fieldName) {
        switch( fieldName )  {
            case VIEW_FILD_NAME_FRACTION:
                return getFraction();
            case VIEW_FILD_NAME_DECIMAL:
                return getDecimal();
            case VIEW_FILD_NAME_PROPER:
                return getProper();
            case VIEW_FILD_NAME_TERMINOLOGY:
                return "";
            case VIEW_FILD_NAME_REDUCED:
                return getReduced();
            default:
                return null;
        }
    }
    
    private void invert( )  {
        final Long temp = numerator;
        numerator = denominator;
        denominator = temp;
    }

    /**
     * Add this fractionRecord's value to the argument FractionRecord's value and return a new FractionRecord
     * with the resulting value in reduced form.
     * @param fractionRecord
     * @return 
     */
    public FractionRecord add( final FractionRecord fractionRecord )    {
        return combine( fractionRecord, true );
    }
    
    /**
     * Subtract the argument FractionRecord's value from this fractionRecord's value and return a new 
     * FractionRecord with the resulting value in reduced form.
     * @param fractionRecord
     * @return 
     */
    public FractionRecord subtract( final FractionRecord fractionRecord )    {
        return combine( fractionRecord, false );
    }
    
    private FractionRecord combine( final FractionRecord fractionRecord, final boolean add )    {
        final Long commonDenominator    = denominator * fractionRecord.denominator;
        final Long thisNumerator        = numerator * fractionRecord.denominator;
        final Long argNumerator         = fractionRecord.numerator * denominator;
        final Long newNumerator         = add ? thisNumerator + argNumerator : thisNumerator - argNumerator;
        final Long gcd = MathLib.GreatestCommonDivisor( newNumerator, commonDenominator );
        final FractionRecord resultFraction    = new FractionRecord( newNumerator / gcd, commonDenominator / gcd );
        return resultFraction;
    }
    
    /**
     * Multiply the argument FractionRecord's value by this fractionRecord's value and return a new 
     * FractionRecord with the resulting value in reduced form.
     * @param fractionRecord
     * @return 
     */
    public FractionRecord multiply( final FractionRecord fractionRecord )    {
        final Long gcdThis = MathLib.GreatestCommonDivisor( numerator, denominator );
        final Long gcdArg = MathLib.GreatestCommonDivisor( fractionRecord.numerator, fractionRecord.denominator );
        
        final FractionRecord resultFraction    = new FractionRecord( numerator * fractionRecord.numerator / gcdThis / gcdArg,
                                                denominator * fractionRecord.denominator / gcdThis / gcdArg );
        return resultFraction;
    }
    
    /**
     * Multiply the argument FractionRecord's value by this fractionRecord's value and return a new 
     * FractionRecord with the resulting value in reduced form.
     * @param fractionRecord
     * @return 
     */
    public FractionRecord divide( final FractionRecord fractionRecord )    {
        fractionRecord.invert();
        final FractionRecord resultFraction = multiply( fractionRecord );
        fractionRecord.invert();
        return resultFraction;
    }
    
    public Double power( final FractionRecord fractionRecord )  {
        return Math.pow( this.getValue(), fractionRecord.getValue() );
    }
    
    public FractionRecord max( final FractionRecord fractionRecord )  {
        if ( getValue() > fractionRecord.getValue() )   {
            return new FractionRecord( numerator, denominator );
        }
        else    {
            return new FractionRecord( fractionRecord.numerator, fractionRecord.denominator );
        }
    }
    
    public FractionRecord min( final FractionRecord fractionRecord )  {
        if ( getValue() < fractionRecord.getValue() )   {
            return new FractionRecord( numerator, denominator );
        }
        else    {
            return new FractionRecord( fractionRecord.numerator, fractionRecord.denominator );
        }
    }
    
    
    
}
