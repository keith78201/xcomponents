/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          TableRecord
 * Author           George Keith Watson
 * Date Started:    July 13, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

/**
 *
 * @author George Keith Watson
 */
public abstract class TableRecord {
    
    public abstract void setField( final String name, final Object value );
    
    public abstract String getField( final String fieldName );
    
}
