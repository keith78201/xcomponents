/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          LibraryNode
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

/**
 *
 * @author George Keith Watson
 */
public class LibraryNode {
    
    private final String name;
    private final long timeStamp;
    
    public LibraryNode( final String name )  throws Exception  {
       if ( name == null && name.length() == 0 )
            throw new Exception( "Null or empty name passed to LibraryNode constructor." );
        this.name   = name;
        timeStamp   = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

}
