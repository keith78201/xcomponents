/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          Attribute
 * Author           George Keith Watson
 * Date Started:    July 31, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

/**
 *
 * @author George Keith Watson
 */
public final class Attribute< TYPE > {
    private final   String  name;
    private         TYPE    value;
    public Attribute( final String name, final TYPE value )   {
        this.name   = name;
        this.value  = value;
    }
    public String getName() {   return name;        }
    public TYPE getValue() {    return value;       }
    public void setValue( final TYPE value ) {    
        this.value  = value;       
    }
    
}
