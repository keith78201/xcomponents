/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          FractionFormat
 * Author           George Keith Watson
 * Date Started:    July 17, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

/**
 *
 * @author George Keith Watson
 */
public enum FractionFormat {
    NORMAL,
    MIXED,
    REDUCED,
    TEXT;
    
}
