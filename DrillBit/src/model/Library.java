/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          Library
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;
import java.util.TreeMap;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.util.Callback;

/**
 *
 * @author George Keith Watson
 */
public class Library extends TreeView< String > {
    
    private final TreeItem< String > root = new TreeItem( "Library" );
    private final TreeMap< String, Object > treeItems     = new TreeMap< >();
    private final WebEngine webEngine;

    public Library( final WebEngine webEngine, final Subject ... subjects )    {
        this.webEngine      = webEngine;
        root.setExpanded( true );
        setRoot( root );
        for ( Subject subject : subjects )  {
            final TreeItem treeItem   = new TreeItem( subject.getName() );
            treeItems.put( subject.getName(), new TreeMap< String, Object >() );
            root.getChildren().add( treeItem );
            traverseSubject( treeItem, subject, ( TreeMap< String, Object > )treeItems.get( subject.getName() ) );
        }
        setEditable( true );
        setCellFactory(new Callback< TreeView< String >, TreeCell< String > >() {
            @Override
            public TreeCell<String> call(TreeView<String> param) {
                return new LabelTreeCellImpl( );
            }
        } );
    }
    
    class LabelTreeCellImpl extends TreeCell< String >  {
        private final Label label;
        
        public LabelTreeCellImpl( )   {
            label   = new Label( getItem() == null ? "" : getItem().toString() );
            label.setOnMouseClicked( new EventHandler< MouseEvent >() {
                @Override
                public void handle(MouseEvent event) {
                    switch( getItem().toString() )  {
                        case "Sun\'s Tutorials":
                            webEngine.load( "file:///root/Java/tutorial/reallybigindex.html" );
                            break;
                        case "JavaFX Area Chart":
                            webEngine.load( "file:///root/Java/JavaFx/Charts/Using JavaFX Charts  Area Chart   JavaFX 2 Tutorials and Documentation.htm" );
                            break;
                        case "JavaFX Bar Chart":
                            webEngine.load( "file:///root/Java/JavaFx/Charts/Using JavaFX Charts  Bar Chart   JavaFX 2 Tutorials and Documentation.htm" );
                            break;
                    }
                    System.out.println( "Mouse Click on Tree Node:\t" + getItem().toString() );
                }
            } );
        }
        
        @Override
        public void updateItem( final String text, final boolean empty )    {
            super.updateItem( text, empty );
            if ( empty )    {
                setText( null );
                setGraphic( null );
            }
            else    {
                if ( label != null )    {
                    label.setText( getItem() == null ? "" : getItem().toString() );
                    setText( null );
                    setGraphic( label);
                }
            }
            label.setText( text ); 
        }
    }
    
    private void traverseSubject( final TreeItem< String > currentBrahch, final Subject subject, TreeMap< String, Object > currentMap )  {
        final ArrayList< LibraryNode > children = subject.getChildren();
        for ( int i = 0; i < children.size(); i++ ) {
            final TreeItem< String > newChild  = new TreeItem( children.get( i ).getName() );
            currentBrahch.getChildren().add( newChild );
            if ( Subject.class.isInstance( children.get( i ) ) )    {
                currentMap.put( children.get( i ).getName(), new TreeMap< String, Object >() );
                traverseSubject( newChild, ( Subject )children.get( i ), ( TreeMap< String, Object > )currentMap.get( children.get( i ).getName() ) );
            }
            else        //it is a Reference and therefore a leaf of the Library tree.
                currentMap.put( children.get( i ).getName(), newChild );
                
        }
    }
    
    public TreeMap<String, Object> getTreeItems() {
        return treeItems;
    }
    
}
