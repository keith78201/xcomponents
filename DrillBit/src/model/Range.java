/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          Range
 * Author           George Keith Watson
 * Date Started:    July 26, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

/**
 *
 * @author George Keith Watson
 */
public class Range {
    
    private Double low  = null;
    private Double high = null;

    public Range( final Double low, final Double high ) throws Exception {
        if ( low > high )
            throw new Exception( "Invalid range in Range constructor, low > high." );
        this.low    = low;
        this.high   = high;
    }
    
    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }
    
    
    public double getRange()  {
        return high - low;
    }
    
}
