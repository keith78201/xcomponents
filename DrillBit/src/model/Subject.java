/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          Subject
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;

/**
 *
 * @author George Keith Watson
 */
public class Subject extends LibraryNode {
    
    private final ArrayList< LibraryNode > children = new ArrayList<  >();

    public Subject( final String name  ) throws Exception    {
        super( name );
    }
    
    public ArrayList<LibraryNode> getChildren() {
        return children;
    }
    
    public void addSubject( final Subject subject ) {
        if ( subject != null )  {
            children.add( subject );
        }
    }

    public void addReference( final Reference reference ) {
        if ( reference != null )  {
            children.add( reference );
        }
    }
}
