/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          EmailDescriptor
 * Author           George Keith Watson
 * Date Started:    July 31, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class EmailDescriptor extends ValueFilter  {
    
    private final String validatedEmail;
    /**
     * Check:
     * Format required: string.string. ... @ string.string. ... where each string can be have Java variable 
     * name format, i.e. alphabetic character followed by any combination of characters, numbers and 
     * underscores.
     * @param type
     * @param eMail 
     */
    public EmailDescriptor( final int type, final String eMail )  {
        super( type );
        validatedEmail  = validate( eMail );
    }

    public String getValidatedEmail() {
        return validatedEmail;
    }
    
    public final String validate( final String candidate )  {
        //IN PROGRESS:
        final String[] idAndLocation    = candidate.split( "@" );
        if ( idAndLocation.length == 2 )    {
            final String[] names    = idAndLocation[ 0 ].split( "'.'" );
        }
        
        
        return candidate;
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
}
