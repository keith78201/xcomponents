/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          NumberDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class NumberDescriptor extends ValueFilter {
    
    private Double low     = null;
    private Double high    = null;

    public NumberDescriptor( final int type )  {
        super ( type );
    }

    public Double getLow() {
        return low;
    }

    public void setLow( Double low ) {
        this.low = low;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh( Double high ) {
        this.high = high;
    }
    
    @Override
    public boolean checkValue( Object value ) {
        if ( ( low != null && ( Double )value < low ) || ( high != null && ( Double )value > high ) )
            return false;
        return true;
    }

    @Override
    public boolean checkCharacter( char ch ) {
        return true;
    }
    
}
