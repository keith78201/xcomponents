/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          ValueFilter
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public abstract class ValueFilter {
    
    public final static int TYPE_ENUM                   = 0;
    public final static int TYPE_LIST                   = 1;
    public final static int TYPE_STRING                 = 2;
    public final static int TYPE_TEXT                   = 3;
    public final static int TYPE_STRING_INTEGER         = 4;
    public final static int TYPE_STRING_DOUBLE          = 5;
    public final static int TYPE_STRING_FRACTION        = 5;
    public final static int TYPE_SETTING_DBL_RANGE      = 6;
    public final static int TYPE_SETTING_INT_RANGE      = 7;
    public final static int TYPE_SETTING_DATE_RANGE     = 8;
    public final static int TYPE_SETTING_TIME_RANGE     = 9;
    public final static int TYPE_MAP                    = 10;
    public final static int TYPE_TREE                   = 11;
    
    
    private Object currentValue;
    
    private final int filterType;

    public ValueFilter( final int type )    {
        this.filterType     = type;
    }
    
    public final int getFilterType() {
        return filterType;
    }

    public final Object getCurrentValue() {
        return currentValue;
    }

    public final void setCurrentValue(Object currentValue) {
        this.currentValue = currentValue;
    }
    
    public abstract boolean checkValue( final Object value );
    
    public abstract boolean checkCharacter( final char ch );
    
}
