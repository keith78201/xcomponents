/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          TreeNode
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      2014-07-27:
 *          This can be either a tree node, i.e. a leaf, or a tree branch.  A branch is a map of tree
 *          branches, meaning it can be either an end node, or it can be another branch in the
 *          nesting structure.
 *          If this is a branch, then its NODE_TYPE is TreeMap< String, TreeNode >.
 *          If this is a leaf, its NODE_TYPE is Object.  
 *
 *          Since a TreeNode can contain any type of data, type checking is not possible except through generics.
 */

package model.filter;

import control.TreeDescriptorException;

/**
 *
 * @author George Keith Watson
 * @param <TYPE> This can either be a JavaFX Node, GUI control, if this is a leaf, or a 
 * TreeMap< String, TreeNode > if this is a branch of the containing tree.
 */
public class TreeNode < TYPE > {
    ;
    private final String identifier;

    /**
     * NODE_TYPE == TreeMap< String, TreeNode > if this is a branch, Object if this is a Node
     */
    private TYPE data;
    
    public TreeNode( final String identifier, final TYPE data ) throws TreeDescriptorException {
        if ( identifier == null || identifier.length() == 0 )
            throw new TreeDescriptorException( "Null or empty identifier argument passed to TreeNode constructor." );
        this.identifier     = identifier;
        this.data           = data;
    }
    
    public String getIdentifier() {
        return identifier;
    }
    
    public final TYPE getData()    {
        return data;
    }
    
    public final void setData( final TYPE data )    {
        this.data   = data;
    }
    
}
