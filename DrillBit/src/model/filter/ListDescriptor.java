/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          ListDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

import control.ControlType;

/**
 *
 * @author George Keith Watson
 */
public class ListDescriptor< ELEMENT_TYPE > extends ValueFilter {

    private final AllowedValuesList< ELEMENT_TYPE > valueList;
    private final ControlType controlType;

    public ListDescriptor( final AllowedValuesList< ELEMENT_TYPE > valueList, final ControlType controlType )    {
        super( ValueFilter.TYPE_LIST );
        this.valueList  = valueList;
        this.controlType    = controlType;
    }

    public ControlType getControlType() {
        return controlType;
    }
    
    public AllowedValuesList< ELEMENT_TYPE > getValueList() {
        return valueList;
    }
            
    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
    
}
