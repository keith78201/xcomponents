/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          ListDescriptor
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

import java.util.TreeMap;

/**
 *
 * @author George Keith Watson
 */
public class MapDescriptor< CONTROL_TYPE, DATA_TYPE > extends ValueFilter {

    final TreeMap< String, CONTROL_TYPE > map         = new TreeMap< >();
    final TreeMap< String, DATA_TYPE >    valueMap    = new TreeMap< >();
    
    public MapDescriptor( ) {
        super( ValueFilter.TYPE_MAP );
    }
    
    public MapDescriptor( final TreeMap< String, CONTROL_TYPE > map ) {
        super( ValueFilter.TYPE_MAP );
        this.map.putAll( map ); 
    }
    
    public final boolean addItem( final String key, final CONTROL_TYPE value ) {
        if ( key == null || key.length() == 0 || value == null )
            return false;
        if ( !map.containsKey( key ) )  {
            map.put( key, value );
            return true;
        }
        return false;
    }
    
    public final boolean setItem( final String key, final CONTROL_TYPE value ) {
        if ( key == null || key.length() == 0 || value == null )
            return false;
        map.put( key, value );
        return true;
    }
    
    public final CONTROL_TYPE removeItem( final String key ) {
        if ( key == null || key.length() == 0 )
            return null;
        return map.remove( key );
    }
    
    public TreeMap< String, CONTROL_TYPE > getMap( )  {
        return map;
    }
    
    public final boolean setValue( final String name, final DATA_TYPE value )   {
        if ( name == null || name.length() == 0 )
            return false;
        if ( map.containsKey( name ) )  {
            valueMap.put( name, value );
            return true;
        }
        return false;
    }
    
    public final DATA_TYPE getValue( final String name ) {
        if ( name == null || name.length() == 0 )
            return null;
        return valueMap.get( name );
    }
    
    public final TreeMap< String, DATA_TYPE > getValueMap()    {
        return valueMap;
    }
    
    @Override
    public final boolean checkValue(Object value) {
        return true;
    }

    @Override
    public final boolean checkCharacter(char ch) {
        return true;
    }
    
}
