/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          AllowedValuesList
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 * @param <ELEMENT_TYPE>
 */
public class AllowedValuesList< ELEMENT_TYPE > extends  ArrayList< ELEMENT_TYPE > {
    
    public ObservableList< String > getObservableList() {
        final ObservableList< String > returnValue  = FXCollections.observableArrayList();
        for ( int i = 0; i < size(); i++ )  {
            returnValue.add( get( i ).toString() );
        }
        return returnValue;
    }
}
