/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          UrlDescriptor
 * Author           George Keith Watson
 * Date Started:    July 31, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class UrlDescriptor extends ValueFilter  {
    
    private final String validatedUrl;

    public UrlDescriptor( final int type, final String url )    {
        super( type );
        this.validatedUrl    = validate( url );
    }
    
    public final String validate( final String candidate )  {
        return candidate;
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
}
