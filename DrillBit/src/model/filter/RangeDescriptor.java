/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          RangeDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class RangeDescriptor extends ValueFilter  {
    
    public RangeDescriptor( final int type )  {
        super ( type );
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
    
}
