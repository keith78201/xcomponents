/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          TreeDescriptor
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      This class stores a tree of controls for display in a JavaFX TreeView along with a parallel
 *      tree of data.  It is a Tree Template for control layout.  This is the layer between the user 
 *      programmer, via an API, or the GUI layout designer and the application created by either.  The 
 *      layout designer used the same API available to a programmerr using this package as a library jar.
 *      The purpose of this Class is to store the description of the controls and their layout which the
 *      designer wants to use for user input and output in an application, along with the control data
 *      buffers that store the data entered.  It is necessary to store a copy of the data outside of the 
 *      Node's own data model in order for the view (Nodes + Layout) to be changed for the same set of 
 *      data. This can be done using an "Adaptor" constructor of the other view template class which 
 *      takes this view's data and restructures it into the other's control set and layout.  Since many 
 *      controls are possible for any particular type of data, the GUI designer engine can offer layout 
 *      template options that the user selects from, and then selects the particular controls they want 
 *      to use for particular data along with their location in the layout.
 *      This architecture provides separation between the GUI view component and its data, which retains
 *      its state between invocations of this viewor its use on others.  The inherently heavyweight 
 *      components of this view can then also be recycled between uses.
 *
 *      Variants of the TreeNode class must contain two types of data.  One is for the JavaFX Node tree, 
 *      the other is  for the data buffer tree.  When the node is a valueBranch, its data is a 
 *      TreeMap< String, TreeNode > in both cases. When the node is a leaf, its data is either a Node, 
 *      in the control tree, or an Object, in the data tree.  Data can also all be stored as String 
 *      to improve type security, but this creates more work: writing the code for all data to String 
 *      and String to data conversions.  
 *      Generics are the most secure and efficient means of implementng this variant class structure.
 *      
 */

package model.filter;

import control.ControlType;
import control.TreeDescriptorException;
import java.awt.Dimension;
import java.io.File;
import java.util.Iterator;
import java.util.TreeMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

/**
 *
 * @author George Keith Watson
 * @param <CONTROL_TYPE>
 * @param <DATA_TYPE>
 */
public class TreeDescriptor< CONTROL_TYPE, DATA_TYPE > extends ValueFilter {

    /**
     * For type security, API of this class must never return references to these trees.  
     * Generic data type control is enforceable only through the node adding methods.
     */
    final TreeNode      controlTree;
    final TreeNode      valueTree;
    final String        rootName;
    
    public TreeDescriptor( final String rootName ) throws TreeDescriptorException {
        super( ValueFilter.TYPE_TREE );
        this.rootName   = rootName;
        controlTree     = new TreeNode( rootName, null );
        valueTree       = new TreeNode( rootName, null );
    }
    
    public TreeNode getControlTree() {
        return controlTree;
    }

    public TreeNode getValueTree() {
        return valueTree;
    }

    public String getRootName() {
        return rootName;
    }
    
    @Override
    public final boolean checkValue(Object value) {
        return true;
    }

    @Override
    public final boolean checkCharacter(char ch) {
        return true;
    }
    
    /**
     * Adds a single valueBranch node at the location specified by locationId.  If will not construct a path
     * in the tree in order to add a valueBranch at the location specified.  Use constructPath() for that.
     * @param locationId
     * @param identifier
     * @throws TreeDescriptorException 
     */
    public final void addBranch( final String locationId, final String identifier )  throws TreeDescriptorException {
        if ( locationId == null || locationId.length() == 0 || identifier == null || identifier.length() == 0 )
            throw new TreeDescriptorException( "Null or emply locationId argument in attempt to add leaf to tree: " + rootName );
        String[] path = locationId.split( ":" );
        pathValidator( path, controlTree );
        pathValidator( path, valueTree );
        TreeNode  controlBranch     = getBranch( path, controlTree );
        TreeNode  valueBranch       = getBranch( path, valueTree );
        if ( controlBranch.getData() == null )
            controlBranch.setData( new TreeMap< String, TreeNode >() ); 
        if ( valueBranch.getData() == null )
            valueBranch.setData( new TreeMap< String, TreeNode >() ); 
        ( ( TreeMap< String, TreeNode > )controlBranch.getData() ).put( identifier, new TreeNode<  >( identifier, new TreeMap< String, TreeNode< CONTROL_TYPE > >() ) );
        ( ( TreeMap< String, TreeNode > )valueBranch.getData() ).put( identifier, new TreeNode<  >( identifier, new TreeMap< String, TreeNode< DATA_TYPE > >() ) );
    }
    
    public final void addLeaf( final String locationId, final String identifier, 
                              ControlType controlType, DATA_TYPE value )  throws TreeDescriptorException {
        if ( locationId == null || locationId.length() == 0 )
            throw new TreeDescriptorException( "Null or emply locationId argument in attempt to add leaf to tree: " + rootName );
        String[] path = locationId.split( ":" );
        pathValidator( path, controlTree );
        pathValidator( path, valueTree );
        TreeNode controlBranch     = getBranch( path, controlTree );
        TreeNode valueBranch       = getBranch( path, valueTree );
        Node node   = null;
        switch( controlType )   {
            case ACCORDION:
                node = new Accordion();
                break;
            case BUTTON:
                final Button button = new Button( value.toString() );
                button.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = new Button( value.toString() );
                break;
            case CHART_BAR:
                node = makeBarChart( new Dimension( 300, 300 ), ( XYChart.Series ) value );
                break;
            case CHART_AREA:
                break;
            case CHART_BUBBLE:
                break;
            case CHART_LINE:
                break;
            case CHART_PIE:
                node = makePieChart( new Dimension( 300, 300 ), ( ObservableList< PieChart.Data > ) value );
                break;
            case CHART_SCATTER:
                break;
            case CHART_STACKED_AREA:
                break;
            case CHART_STACKED_BAR:
                break;
            case CHECK_BOX:
                final CheckBox checkBox = new CheckBox( value.toString() );
                checkBox.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = checkBox;
                break;
            case CHOICE_BOX:
                final ChoiceBox choiceBox = new ChoiceBox( ( ObservableList< String > )value );
                choiceBox.valueProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = choiceBox;
                break;
            case COLOR_PICKER:
                final ColorPicker colorPicker = new ColorPicker( );
                colorPicker.valueProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = colorPicker;
                break;
            case COMBO_BOX:
                ComboBox comboBox = new ComboBox( ( ObservableList< String > )value );
                comboBox.valueProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = comboBox;
                break;
            case FILE_CHOOSER:
                final Button filePickerButton = new Button( value.toString() );
                filePickerButton.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        FileChooser fileChooser = new FileChooser();
                        File chosenFile = fileChooser.showOpenDialog( null );
                        if ( chosenFile != null )
                            filePickerButton.setText( chosenFile.getName() );
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = filePickerButton;
                break;
            case HYPER_LINK:
                final Hyperlink hyperlink = new Hyperlink( value.toString() );
                hyperlink.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = hyperlink;
                break;
            case LABEL:
                node = new Label( value.toString() );
                break;
            case LIST:
                final ListView listView = new ListView( ( ObservableList< String > )value );
                listView.getSelectionModel().selectionModeProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                    }
                } );
                
                listView.setOnMouseClicked( new EventHandler< MouseEvent > () {
                    @Override
                    public void handle(MouseEvent event) {
                    }
                } ); 
                node = listView;
                break;
            case PASSWORD_FIELD:
                final PasswordField passwordField = new PasswordField( );
                passwordField.setText( value.toString() );
                passwordField.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = passwordField;
                break;
            case PROGRESS_BAR:
                final ProgressBar progressBar = new ProgressBar( ( Double )value );
                node = progressBar;
                break;
            case RADIO_BUTTON:
                final RadioButton radioButton = new RadioButton( value.toString() );;
                radioButton.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = radioButton;
                break;
            case SCROLL_BAR:
                final ScrollBar scrollBar = new ScrollBar();
                scrollBar.valueProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = scrollBar;
                break;
            case SLIDER:
                final Slider slider = new Slider( 0.0, 10.0, 100.0 );
                slider.valueProperty().addListener( new ChangeListener() {
                    @Override
                    public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = slider;
                break;
            case TEXT_AREA:
                final TextArea textArea = new TextArea( value.toString() );
                textArea.setOnKeyTyped( new EventHandler< KeyEvent >() {
                    @Override
                    public void handle(KeyEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                }); 
                node = textArea;
                break;
            case TEXT_FIELD:
                final TextField textField = new TextField( value.toString() );
                textField.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = textField;
                break;
            case TOGGLE_BUTTON:
                final ToggleButton toggleButton = new ToggleButton( value.toString() );
                toggleButton.setOnAction( new EventHandler< ActionEvent >() {
                    @Override
                    public void handle(ActionEvent event) {
                        //Data storage and other user programmer desired activities go here.
                    }
                } ); 
                node = toggleButton;
                break;
            case TREE_VIEW: //Pointless, except for the case where someone has one they want to use in this application designer
                final TreeView treeView = new TreeView( ( TreeItem) value );
                node = treeView;
                break;
                                  
        }
        //The valueBranch found must hava a TreeList to have a TreeNode added to it
        if ( controlBranch.getData() == null )
            controlBranch.setData( new TreeMap< String, TreeNode >() );
        ( ( TreeMap< String, TreeNode > ) controlBranch.getData() ).put( identifier, new TreeNode< CONTROL_TYPE >( identifier, ( CONTROL_TYPE ) node ) );
        if ( valueBranch.getData() == null )
            valueBranch.setData( new TreeMap< String, TreeNode >() );
        ( ( TreeMap< String, TreeNode > ) valueBranch.getData() ).put( identifier, new TreeNode< DATA_TYPE >( identifier, value ) );
    }
    
    /**
     * Build an empty path in the tree, i.e. one that contains no leaves but which can be traversed 
     *  using the String[] path argument.  No other branches are created, just a linear linked list of
     * TreeNode's.  This method does nothing to the tree if the path already exists.  It just traverses 
     * the path checking to see if there is a need to add a valueBranch node at any point, then adds the
     * remaining valueBranch nodes named in the path.<br>
     * @param path 
     * @return  the TreeNode of the valueBranch at the end of the path constructed or found.
     */
    public final void constructPath( final String[] path )  throws TreeDescriptorException  {
        constructPath( path, controlTree );
        constructPath( path, valueTree );
    }
    
    public final void constructPath( final String[] path, TreeNode root )  throws TreeDescriptorException  {
        if ( path == null || path.length < 1 || !path[ 0 ].equals( rootName ) )
            throw new TreeDescriptorException( "Invalid path specified in path constructor." );
        if ( root .getData() == null )  {
            root.setData( new TreeMap< String, TreeNode >() ); 
        }
        TreeMap< String, TreeNode > valueBranch    = ( TreeMap< String, TreeNode > )root.getData();
        for ( int pathIdx = 1; pathIdx < path.length; pathIdx++ )    {
            if ( valueBranch.get( path[ pathIdx ] ) == null )    {
                // This is an empty location in the current valueBranch's children map.
                valueBranch.put( path[ pathIdx ], new TreeNode( path[ pathIdx ], new TreeMap< String, TreeNode >() ) );
                valueBranch  = ( TreeMap< String, TreeNode > )valueBranch.get( path[ pathIdx ] ).getData();
            }
            else if ( valueBranch.get( path[ pathIdx ] ).getData() != null )  {
                //Is a TreeNode referenced by the identified TreeNode's data? 
                //If so, then the next node is not a valueBranch but a leaf.  This is an error
                if ( TreeNode.class.isInstance( valueBranch.get( path[ pathIdx ] ).getData() ) )
                    throw new TreeDescriptorException( "Leaf found in path specified in path constructor." );
                else if ( TreeMap.class.isInstance( valueBranch.get( path[ pathIdx ] ).getData() ) )   {
                    //This the current name in the path specifies the next valueBranch node.
                    valueBranch = ( TreeMap< String, TreeNode > ) valueBranch.get( path[ pathIdx ] ).getData();
                    if ( valueBranch == null )   {
                        throw new TreeDescriptorException( "Leaf null branch found in path specified in path constructor." );
                    }
                }
                //The current TreeNode's type is not correct: integrety breach
                else    {
                    throw new TreeDescriptorException( "Type of branch found in path specified in path constructor in invalid: " + valueBranch.get( path[ pathIdx ] ).getData().getClass() );
                }
            }
            //The next node is an empty node and is the current valueBranch in the path.  Append the remaining
            //branches.  Since the type of the data in an empty node is specified at creation, the node
            //must be replaced with one which can only hold a TreeMap< String, TreeNode >.
            else    {
                valueBranch.put( path[ pathIdx ], new TreeNode( path[ pathIdx ], new TreeMap< String, TreeNode >() ) );
                valueBranch  = ( TreeMap< String, TreeNode > )valueBranch.get( path[ pathIdx ] ).getData();
            }
            if ( valueBranch.get( path[ pathIdx ] ) == null )     {
                valueBranch.put( path[ pathIdx ], new TreeNode( path[ pathIdx ], new TreeMap< String, TreeNode >() ) );
            }
            valueBranch = ( TreeMap< String, TreeNode > ) valueBranch.get( path[ pathIdx ] ).getData();
        }
    }
    
    /**
     * Helper method ( or alias ) for constructPath( final String[] path ).
     * @param path 
     */
    public final void constructPath( final String path ) throws TreeDescriptorException    {
        constructPath( path.split( ":" ) );
    }
    
    private final TreeMap< String, CONTROL_TYPE > getControlLeaf()    {
        return null;
    }
    
    private final TreeMap< String, DATA_TYPE > getDataLeaf()    {
        return null;
    }
    
    /**
     * This method finds the valueBranch node at the specified pathNames location in the specified tree.
     * It can only be used on a validated pathNames.
     * @param pathNames The path to the valueBranch.  The last name in the array must refer to a valueBranch, not
 a leaf.
     * @return 
     */
    private TreeNode getBranch( final String[] pathNames, TreeNode root )  throws TreeDescriptorException  {
        if ( pathNames == null || pathNames.length < 1 )
            throw new TreeDescriptorException( "Null or empty path passed to branch resolver" );
        if ( pathNames.length == 1 )
            return root;
        TreeMap< String, TreeNode > branch    = ( TreeMap< String, TreeNode > )root.getData();
        for ( int pathIdx = 1; pathIdx < pathNames.length; pathIdx++ )    {
            if ( pathIdx == pathNames.length - 1 )  {
                return branch.get( pathNames[ pathIdx ] );
            }
            branch = ( TreeMap< String, TreeNode > ) branch.get( pathNames[ pathIdx ] ).getData();
        }
        return null; 
    }
    
    private  void pathValidator( final String path, TreeNode tree )  throws TreeDescriptorException  {
        if ( path == null ) {
            throw new TreeDescriptorException( "Path tested for validity is null" );
        }
        pathValidator( path.split( ":" ), tree ); 
    }

    /**
     * This is the general policeman for the tree.  It tests both whether the name argument exists in the
 tree passed in and the integrity tree name specified.  Integrity checking is implemented via type checking 
 to the extent possible both here and in all methods that add anything to the tree.  
 This method exists without throwing an exception if the tree is valid along the 
 valid name specified.
     * @param path
     * @throws TreeDescriptorException 
     */
    private void pathValidator( final String[] path, final TreeNode tree ) throws TreeDescriptorException {
        if ( path == null || path.length == 0 )
            throw new TreeDescriptorException( "Path tested for validity is null or empty" );
        if ( !path[0].equals( rootName ) )
            throw new TreeDescriptorException( "Path tested for validity has wrong root name in tree: " + rootName );
        //Does root have any children?  If name length is 1 and it has no children, all is good.
        if ( path.length == 1 && tree.getData( ) == null  )
            return;
        if ( TreeNode.class.isInstance( tree.getData() ) )      //This is a leaf node: the root cannot be a leaf
            throw new TreeDescriptorException( "Root is a leaf: " + rootName );
        //If the node has children, they are in a TreeMap< String, TreeNode > which is stored in its 
        //TreeNode data property.  The Root is never a leaf, so if its data is not null, it is a TreeMap.
        TreeMap< String, TreeNode >    branch    = ( TreeMap< String,TreeNode > )tree.getData();
        String branchName   = rootName;
        for ( int depth = 1; branch != null && depth < path.length; depth++ )    {
            if ( TreeMap.class.isInstance( branch.get( path[ depth ] ).getData() ) ) {
                if ( !branch.containsKey( path[ depth ] ) )
                    throw new TreeDescriptorException( "Path element, " + path[ depth ] + ", in path tested for validity does not exist in tree: " + rootName );
                branch  = ( TreeMap< String, TreeNode > ) branch.get( path[ depth ] ).getData();
            }
            else    {
                if ( depth != path.length - 1 )
                    throw new TreeDescriptorException( "Found a leaf node before end of path tested in tree: " + rootName );
                //This will throw a class cast exception if the child-leaf is not of the required type.
                DATA_TYPE testValue = (DATA_TYPE) branch.get( path[ depth ] ).getData();
                branch = null;
            }
        }
    }
    
    private void constructViewBranch( TreeNode branch, TreeItem parent )   {
        if ( branch == null )
            return;
        if ( TreeMap.class.isInstance( branch.getData() ) ) {
            TreeMap< String, TreeNode > nodeMap   = ( TreeMap< String, TreeNode > ) branch.getData();
            Iterator< String > iterator   = nodeMap.keySet().iterator();
            while( iterator.hasNext() ) {
                final String key    = iterator.next();

                //Is the child a leaf - a JavaFX Node of CONTROL_TYPE?
                if ( TreeMap.class.isInstance( nodeMap.get( key ).getData() ) ) {
                    TreeMap< String, TreeNode > childBranch = ( TreeMap< String, TreeNode > )nodeMap.get( key ).getData();
                    Iterator< String > branchIterator = childBranch.keySet().iterator();
                    final TreeItem branchItem = new TreeItem( key );
                    parent.getChildren().add( branchItem );
                    while ( branchIterator.hasNext() )  {
                        constructViewBranch( childBranch.get( branchIterator.next() ), branchItem ); 
                    }
                }
                else      {
                    CONTROL_TYPE control =  ( CONTROL_TYPE )nodeMap.get( key ).getData();
                    parent.getChildren().add( new TreeItem( control ) );
                }
            }
       }
        else  {                 //only one Node - could be an empty valueBranch
            if ( TreeNode.class.isInstance( branch.getData() ) )    {
                parent.getChildren().add( new TreeItem( branch.getIdentifier(), new Label( branch.getIdentifier() ) ) );
            }
            else
            {    
                final CONTROL_TYPE control    = ( CONTROL_TYPE )branch.getData();
                parent.getChildren().add( new TreeItem( branch.getIdentifier(), ( Node )control ) );
            }
        }
    }
    
    public final TreeView constructView()   {
        final TreeItem rootItem     = new TreeItem( rootName );
        final TreeView treeView     = new TreeView( rootItem );
        
        if ( controlTree.getData() != null )
            constructViewBranch( controlTree, rootItem );
        return treeView;
    }
    
    
    private final PieChart makePieChart( final Dimension dimensions, ObservableList< PieChart.Data > sliceData )  {
        final PieChart pieChart = new PieChart();
        pieChart.setData( sliceData );
        return pieChart;
    }
    
    private final BarChart makeBarChart( final Dimension dimensions, XYChart.Series barSeries )  {
        final CategoryAxis xAxis    = new CategoryAxis();
        final NumberAxis yAxis      = new NumberAxis();
        final BarChart barChart = new BarChart( xAxis, yAxis );
        barChart.getData().addAll( barSeries );
        return barChart;
    }
    
    
}
