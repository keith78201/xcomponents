/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          RealDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author Geroge Keith Watson
 */
public class RealDescriptor  extends ValueFilter {
    
    public RealDescriptor( final int type ) {
        super( type );
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
    
}
