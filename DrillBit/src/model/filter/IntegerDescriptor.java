/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          IntegerDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class IntegerDescriptor extends ValueFilter {
    
    private Integer low     = null;
    private Integer high    = null;

    public IntegerDescriptor( final int type )  {
        super( type );
    }

    public IntegerDescriptor( final int type,final int low, final int high )  {
        super( type );
        this.low    = low;
        this.high   = high;
    }

    public Integer getLow() {
        return low;
    }

    public void setLow(Integer low) {
        this.low = low;
    }

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }
    
    @Override
    public boolean checkValue( Object value ) {
        if ( ( low != null && ( Integer )value < low ) || ( high != null && ( Integer )value > high ) )
            return false;
        return true;
    }

    @Override
    public boolean checkCharacter( char ch ) {
        return Character.isDigit( ch );
    }
    
}
