/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          IpAddressDescriptor
 * Author           George Keith Watson
 * Date Started:    July 31, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author George Keith Watson
 */
public class IpAddressDescriptor extends ValueFilter  {
    
    private final String validatedIpAddr;

    public IpAddressDescriptor( final int type, final String ipAddress )  {
        super( type );
        this.validatedIpAddr    = validate( ipAddress );
    }
    
    public String getValidatedIpAddr() {
        return validatedIpAddr;
    }
    
    public final String validate( final String candidate )  {
        return candidate;
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
}
