/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          FractionDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

/**
 *
 * @author Geroge Keith Watson
 */
public class FractionDescriptor extends ValueFilter {

    private long numerator;
    private long denominator;
    private Double low     = null;
    private Double high    = null;

    public FractionDescriptor( final int type ) {
        super( type );
    }

    public FractionDescriptor( final int type, final Double low, final Double high ) {
        super( type );
        this.low    = low;
        this.high   = high;
    }

    public long getNumerator() {
        return numerator;
    }

    public void setNumerator(long numerator) {
        this.numerator = numerator;
    }

    public long getDenominator() {
        return denominator;
    }

    public void setDenominator(long denominator) {
        this.denominator = denominator;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh( Double high ) {
        this.high = high;
    }
    
    @Override
    public boolean checkValue( Object value ) {
        if ( ( low != null && ( Double )value < low ) || ( high != null && ( Double )value > high ) )
            return false;
        return true;
    }

    @Override
    public boolean checkCharacter( char ch ) {
        return true;
    }
    
}
