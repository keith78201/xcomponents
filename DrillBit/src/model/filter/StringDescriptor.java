/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          StringDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Filters for various types of strings including phone numbers, email addresses and URLs.
 * @author George Keith Watson
 */
public class StringDescriptor extends ValueFilter {
    
    private final ArrayList< String > autoCompleteList  = new ArrayList< String >();
    
    public StringDescriptor()  {
        super ( ValueFilter.TYPE_STRING );
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return false;
    }
    
    /**
     * Search the auto complete suggestion list for the text util a sub text is fould, starting with the full 
     * text and removing the last character after each search attempt.  e.g. if the text is "clam" then 
     * first "clam" is searched for, then "cla", then "cl", then "c" if it is not found.  As soon as a match is 
     * found, the string containing the sub-text is returned.
     * @param text
     * @return 
     */
    public String checkAutoComplete( final String text )    {
        String searchText   = text;
        while ( text.length() > 0 ) {
            int location = binarySubStringSearch( autoCompleteList, text );
            if ( location >= 0 )
                return autoCompleteList.get( location );
            searchText = searchText.substring( 0, searchText.length() - 1 );
        }
        return text;
    }
    
    public void addAutoCompleteSuggestions( final String ... suggestions )   {
        for ( String suggestion : suggestions ) {
            autoCompleteList.add( suggestion );
        }
        Collections.sort( autoCompleteList );
    }
    
    private int binarySubStringSearch( ArrayList< String > list, String text )  {
        int low = 0;
        int high = list.size()-1;
        while( low <= high ) {
            int probe   = ( high + low ) / 2;
            String subText  = list.get( probe ).substring( 0, text.length()- 1 );
            if ( text.toLowerCase().compareTo( subText.toLowerCase() ) == 0 )
                return probe;
            else if ( text.toLowerCase().compareTo( subText.toLowerCase() ) > 0 )    {
                low = probe + 1;
            }
            else     {
                high = probe - 1;
            }
        }
        return -1;
    }
    
}
