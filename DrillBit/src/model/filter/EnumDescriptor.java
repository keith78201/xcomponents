/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          EnumDescriptor
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model.filter;

import control.ConductorType;
import control.ControlType;
import control.EnumId;
import control.MathOperation;
import control.SimplePropertyType;
import control.StateAbbr;
import control.WireSize;
import control.WiringMethod;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public class EnumDescriptor extends ValueFilter {
    
    private final EnumId enumId;
    private final ControlType controlType;

    public EnumDescriptor( final EnumId enumId, final ControlType controlType )  {
        super ( ValueFilter.TYPE_ENUM );
        this.enumId         = enumId;
        this.controlType    = controlType;
    }

    public EnumId getEnumId() {
        return enumId;
    }
    
    public ControlType getControlType() {
        return controlType;
    }
    
    public final ObservableList< String > getList() {
        switch( enumId )    {
            case TEMPERATURE_RATING:
                return FXCollections.observableArrayList();
            case CONDUIT_TYPE:
                return FXCollections.observableArrayList();
            case CONDUIT_SIZE:
                return FXCollections.observableArrayList();
            case OUTLET_BOX_TYPE:
                return FXCollections.observableArrayList();
            case OUTLET_BOX_SIZE:
                return FXCollections.observableArrayList();
            case JUNCTION_BOX_TYPE:
                return FXCollections.observableArrayList();
            case JUNCTION_BOX_SIZE:
                return FXCollections.observableArrayList();
            case PULL_BOX_TYPE:
                return FXCollections.observableArrayList();
            case PULL_BOX_SIZE:
                return FXCollections.observableArrayList();
            case CONDUIT_BODY_TYPE:
                return FXCollections.observableArrayList();
            case CONDUIT_BODY_SIZE:
                return FXCollections.observableArrayList();
            case CONTROL_TYPE:
                return FXCollections.observableArrayList();
                
            case MATH_OPERATION:
                return MathOperation.list();
            case SIMPLE_PROPERTY_TYPE:
                return SimplePropertyType.list();
            case STATE_ABBRV:
                return StateAbbr.list();
            case WIRE_SIZE:
                return WireSize.list();
            case CONDUCTOR_TYPE:
                return ConductorType.list();
            case WIRING_METHOD:
                return WiringMethod.list();
            default:
                return FXCollections.observableArrayList();
        }
    }

    @Override
    public boolean checkValue(Object value) {
        return true;
    }

    @Override
    public boolean checkCharacter(char ch) {
        return true;
    }
    
    
}
