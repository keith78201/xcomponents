/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          OhmsWheelRecord
 * Author           George Keith Watson
 * Date Started:    July 13, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import control.ControlType;
import control.SimplePropertyType;
import control.XModel;
import java.util.ArrayList;
import java.util.TreeMap;
import model.filter.NumberDescriptor;
import model.filter.ValueFilter;

/**
 *
 * @author George Keith Watson
 */
public class OhmsWheelRecord extends TableRecord implements XModel {
    
    public static final String VIEW_FIELD_NAME_VOLTAGE    = "Voltage";
    public static final String VIEW_FIELD_NAME_CURRENT    = "Current";
    public static final String VIEW_FIELD_NAME_RESISTANCE = "Resistance";
    public static final String VIEW_FIELD_NAME_POWER      = "Power";
    public static final String VIEW_FIELD_NAME_TIME       = "Time";
    public static final String VIEW_FIELD_NAME_ENERGY     = "Energy";
    
    private Double voltageVal      = null;
    private Double currentVal      = null;
    private Double resistanceVal   = null;
    private Double powerVal        = null;
    private Double timeVal         = null;
    private Double energyVal       = null;
    
    private final ArrayList< ObservableProperty >       propertyList    = new ArrayList< >();
    private final TreeMap< String, ObservableProperty > propertyMap     = new TreeMap< >();

    /**
     * Acceptable string double representation format is as specified in the JavaDoc for Double.valueOf( String ).
     * @param voltage
     * @param current
     * @param resistance
     * @param power
     * @param time
     * @param energy 
     */
    public OhmsWheelRecord( final String voltage, final String current, final String resistance,
                            final String power, final String time, final String energy )  {
        try {
            voltageVal  = Double.valueOf( voltage );
            //this.voltage    = voltage;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        try {
            currentVal  = Double.valueOf( current );
            //this.current    = current;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        try {
            resistanceVal  = Double.valueOf( resistance );
            //this.resistance    = resistance;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        try {
            powerVal  = Double.valueOf( power );
            //this.power    = power;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        try {
            timeVal  = Double.valueOf( time );
            //this.time    = time;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        try {
            energyVal  = Double.valueOf( energy );
            //this.energy    = energy;
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
        setProperties(); 
    }
    
    
    @Override
    public String getField( final String fieldName )    {
        switch( fieldName )  {
            case VIEW_FIELD_NAME_VOLTAGE:
                return getVoltage( );
            case VIEW_FIELD_NAME_CURRENT:
                return getCurrent( );
            case VIEW_FIELD_NAME_RESISTANCE:
                return getResistance( );
            case VIEW_FIELD_NAME_POWER:
                return getPower( );
            case VIEW_FIELD_NAME_TIME:
                return getTime( );
            case VIEW_FIELD_NAME_ENERGY:
                return getEnergy( );
            default:
                return null;
        }
    }
    
    public void setField( final String fieldName, final String value )  {
        switch( fieldName )  {
            case VIEW_FIELD_NAME_VOLTAGE:
                setVoltage( ( String ) value );
                break;
            case VIEW_FIELD_NAME_CURRENT:
                setCurrent( ( String ) value );
                break;
            case VIEW_FIELD_NAME_RESISTANCE:
                setResistance( ( String ) value );
                break;
            case VIEW_FIELD_NAME_POWER:
                setPower( ( String ) value );
                break;
            case VIEW_FIELD_NAME_TIME:
                setTime( ( String ) value );
                break;
            case VIEW_FIELD_NAME_ENERGY:
                setEnergy( ( String ) value );
                break;
        }
    }
    

    
    public String getVoltage() { 
        if ( voltageVal == null )
            return "";
        return voltageVal.toString();
    }

    /**
     * Sets a new value for voltage.  Recomputes the other values assuming resistance remains the same, i.e.
     * assuming that this is a common electrical supply circuit.
     * @param voltage 
     */
    public void setVoltage( String voltage ) {
        try {
            final Double voltageVal  = Double.valueOf( voltage );
            if ( this.voltageVal == null || !this.voltageVal.equals( voltageVal ) )   {
                this.voltageVal = voltageVal;
                propertyMap.get( VIEW_FIELD_NAME_VOLTAGE ).getValueFilter().setCurrentValue( voltageVal );
                if ( resistanceVal != null && resistanceVal != 0 )  {
                    currentVal = voltageVal / resistanceVal;
                    propertyMap.get( VIEW_FIELD_NAME_CURRENT ).getValueFilter().setCurrentValue( currentVal );
                    powerVal   = currentVal * voltageVal;
                    propertyMap.get( VIEW_FIELD_NAME_POWER ).getValueFilter().setCurrentValue( powerVal );
                    if ( timeVal != null )
                        energyVal  = powerVal * timeVal;
                        propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
                }
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    public String getCurrent() {
        if ( currentVal == null )
            return "";
        return currentVal.toString();
    }

    /**
     * Sets a new value for current.  Recomputes the other values assuming resistance remains the same, i.e.
     * assuming that this is a common electrical supply circuit.
     * @param currrent 
     */
    public void setCurrent( String current ) {
        try {
            final Double currentVal  = Double.valueOf( current );
            if ( this.currentVal == null || !this.currentVal.equals( currentVal ) )   {
                this.currentVal = currentVal;
                propertyMap.get( VIEW_FIELD_NAME_CURRENT ).getValueFilter().setCurrentValue( currentVal );
                if ( resistanceVal != null )  {
                    voltageVal = currentVal * resistanceVal;
                    propertyMap.get( VIEW_FIELD_NAME_VOLTAGE ).getValueFilter().setCurrentValue( voltageVal );
                    powerVal   = currentVal * voltageVal;
                    propertyMap.get( VIEW_FIELD_NAME_POWER ).getValueFilter().setCurrentValue( powerVal );
                    if ( timeVal != null )
                        energyVal  = powerVal * timeVal;
                        propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
                }
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    public String getResistance() {
        if ( resistanceVal == null )
            return "";
        return resistanceVal.toString();
    }

    /**
     * Sets a new value for resistance.  Recomputes the other values assuming voltage remains the same, i.e.
     * assuming that this is a common electrical supply circuit.
     * @param resistance 
     */
    public void setResistance( String resistance ) {
        try {
            final Double resistanceVal  = Double.valueOf( resistance );
            if ( this.resistanceVal == null || !this.resistanceVal.equals( resistanceVal ) )   {
                this.resistanceVal = resistanceVal;
                propertyMap.get( VIEW_FIELD_NAME_RESISTANCE ).getValueFilter().setCurrentValue( resistanceVal );
                if ( voltageVal != null && resistanceVal != 0 )  {
                    currentVal = voltageVal / resistanceVal;
                    propertyMap.get( VIEW_FIELD_NAME_CURRENT ).getValueFilter().setCurrentValue( currentVal );
                    powerVal   = currentVal * voltageVal;
                    propertyMap.get( VIEW_FIELD_NAME_POWER ).getValueFilter().setCurrentValue( powerVal );
                    if ( timeVal != null )  {
                        energyVal  = powerVal * timeVal;
                        propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
                    }
                }
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    public String getPower() {
        if ( powerVal == null )
            return "";
        return powerVal.toString();
    }

    /**
     * Sets a new value for power.  Recomputes the other values assuming voltage and resistance remain 
     * the same, i.e. assuming that this is a common electrical supply circuit.
     * @param power 
     */
    public void setPower( String power ) {
        try {
            final Double powerVal  = Double.valueOf( power );
            if ( this.powerVal == null || !this.powerVal.equals( powerVal ) )   {
                this.powerVal = powerVal;
                propertyMap.get( VIEW_FIELD_NAME_POWER ).getValueFilter().setCurrentValue( powerVal );
                if ( voltageVal != null && powerVal != 0 )  {
                    currentVal = powerVal / voltageVal;
                    propertyMap.get( VIEW_FIELD_NAME_CURRENT ).getValueFilter().setCurrentValue( currentVal );
                    if ( timeVal != null )
                        energyVal  = powerVal * timeVal;
                        propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
                }
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    public String getTime() {
        if ( timeVal == null )
            return "";
        return timeVal.toString();
    }

    /**
     * Sets a new value for time.  Recomputes energy assuming all other variables remain 
     * the same, i.e. assuming that this is a common electrical supply circuit.
     * @param time 
     */
    public void setTime( String time ) {
        try {
            final Double timeVal  = Double.valueOf( time );
            if ( this.timeVal == null || !this.timeVal.equals( timeVal ) )   {
                this.timeVal = timeVal;
                propertyMap.get( VIEW_FIELD_NAME_TIME ).getValueFilter().setCurrentValue( timeVal );
                if ( powerVal != null )
                    energyVal  = powerVal * timeVal;
                    propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    public String getEnergy() {
        if ( energyVal == null )
            return "";
        return energyVal.toString();
    }

    /**
     * Sets a new value for energy.  Recomputes time assuming all other variables remain 
     * the same, i.e. assuming that this is a common electrical supply circuit.
     * @param energy 
     */
    public void setEnergy( String energy ) {
        try {
            final Double energyVal  = Double.valueOf( energy );
            if ( this.energyVal == null || !this.energyVal.equals( energyVal ) )   {
                this.energyVal = energyVal;
                propertyMap.get( VIEW_FIELD_NAME_ENERGY ).getValueFilter().setCurrentValue( energyVal );
                if ( powerVal != null && powerVal != 0 )
                    timeVal  = energyVal / powerVal;
                    propertyMap.get( VIEW_FIELD_NAME_TIME ).getValueFilter().setCurrentValue( timeVal );
            }
        } catch ( NullPointerException | NumberFormatException ex )    {
        }
    }

    @Override
    public void setField( final String name, final Object value )  {
        switch( name )  {
            case VIEW_FIELD_NAME_VOLTAGE:
                setVoltage( ( String ) value );
                break;
            case VIEW_FIELD_NAME_CURRENT:
                setCurrent( ( String ) value );
                break;
            case VIEW_FIELD_NAME_RESISTANCE:
                setResistance( ( String ) value );
                break;
            case VIEW_FIELD_NAME_POWER:
                setPower( ( String ) value );
                break;
            case VIEW_FIELD_NAME_TIME:
                setTime( ( String ) value );
                break;
            case VIEW_FIELD_NAME_ENERGY:
                setEnergy( ( String ) value );
                break;
        }
    }
    
    @Override
    public final ArrayList< ObservableProperty > getProperties() {
        return propertyList;
    }
    
    private void setProperties() {
        final NumberDescriptor voltageDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        voltageDescriptor.setCurrentValue( getVoltage() );
        propertyMap.put(  VIEW_FIELD_NAME_VOLTAGE, new ObservableProperty< Double >( VIEW_FIELD_NAME_VOLTAGE, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, voltageDescriptor ) );
        propertyList.add( propertyMap.get(  VIEW_FIELD_NAME_VOLTAGE ) );

        final NumberDescriptor currentDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        currentDescriptor.setCurrentValue( getCurrent() );
        propertyMap.put(  VIEW_FIELD_NAME_CURRENT, new ObservableProperty< Double >( VIEW_FIELD_NAME_CURRENT, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, currentDescriptor ) );
        propertyList.add( propertyMap.get(  VIEW_FIELD_NAME_CURRENT ) );
        
        final NumberDescriptor resistanceDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        resistanceDescriptor.setCurrentValue( getResistance() );
        propertyMap.put(  VIEW_FIELD_NAME_RESISTANCE, new ObservableProperty< Double >( VIEW_FIELD_NAME_RESISTANCE, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, resistanceDescriptor ) );
        propertyList.add( propertyMap.get(  VIEW_FIELD_NAME_RESISTANCE ) );
        
        final NumberDescriptor powerDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        powerDescriptor.setCurrentValue( getPower() );
        propertyMap.put(  VIEW_FIELD_NAME_POWER, new ObservableProperty< Double >( VIEW_FIELD_NAME_POWER, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, powerDescriptor ) );
        propertyList.add( propertyMap.get( VIEW_FIELD_NAME_POWER ) );
        
        final NumberDescriptor timeDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        timeDescriptor.setCurrentValue( getTime() );
        propertyMap.put(  VIEW_FIELD_NAME_TIME, new ObservableProperty< Double >( VIEW_FIELD_NAME_TIME, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, timeDescriptor ) );
        propertyList.add( propertyMap.get(  VIEW_FIELD_NAME_TIME ) );
        
        final NumberDescriptor energyDescriptor    = new NumberDescriptor( ValueFilter.TYPE_STRING_DOUBLE );
        energyDescriptor.setCurrentValue( getTime() );
        propertyMap.put( VIEW_FIELD_NAME_ENERGY, new ObservableProperty< Double >( VIEW_FIELD_NAME_ENERGY, ControlType.TEXT_FIELD, SimplePropertyType.DOUBLE, energyDescriptor ) );
        propertyList.add( propertyMap.get( VIEW_FIELD_NAME_ENERGY ) );
    }

    @Override
    public final ArrayList< ObservableProperty > getDependencies( String propertyName ) {
        final ArrayList< ObservableProperty > dependencies  = new ArrayList< >();
        switch ( propertyName ) {
            case VIEW_FIELD_NAME_VOLTAGE:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_CURRENT ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_POWER ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_ENERGY ) );
                break;
            case VIEW_FIELD_NAME_CURRENT:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_POWER ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_ENERGY ) );
                break;
            case VIEW_FIELD_NAME_RESISTANCE:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_CURRENT ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_POWER ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_ENERGY ) );
                break;
            case VIEW_FIELD_NAME_POWER:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_TIME ) );
                break;
            case VIEW_FIELD_NAME_TIME:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_ENERGY ) );
                break;
            case VIEW_FIELD_NAME_ENERGY:
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_POWER ) );
                dependencies.add( propertyMap.get( VIEW_FIELD_NAME_TIME ) );
                break;
        }
        return dependencies;
    }

    @Override
    public ArrayList< ObservableProperty > getDependencies( ObservableProperty property ) {
        return getDependencies( property.getName() );
    }
    
    @Override
    public final ObservableProperty getProperty( final String name ) {
        return propertyMap.get( name );
    }
    
    @Override
    public void updateProperty( final String propertyName,final Object value ) {
        setField( propertyName, value );
    }
    
    
}
