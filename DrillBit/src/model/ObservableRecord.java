/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          ObservableRecord
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;

/**
 *
 * @author George Keith Watson
 */
public class ObservableRecord extends ArrayList < ObservableProperty >  {
    
}
