/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          Reference
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.ArrayList;

/**
 *
 * @author George Keith Watson
 */
public class Reference extends LibraryNode {
    
    private final String location;
    private final ArrayList< String > keyWords  = new ArrayList< >();
    
    public Reference( final String name, final String location ) throws Exception {
        super( name );
        if ( location == null || location.length() == 0 )
            throw new Exception( "Null or empty location passed to Reference constructor." );
        this.location   = location;
    }

    public String getLocation() {
        return location;
    }

    public ArrayList<String> getKeyWords() {
        return keyWords;
    }
    
    public boolean addKeyWord( final String keyWord )  {
        if ( keyWord != null && keyWord.length() > 0 && !keyWords.contains( keyWord ) ) {
            keyWords.add( keyWord );
            return true;
        }
        return false;
    }
    
}
