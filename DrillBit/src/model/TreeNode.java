/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          TreeNode
 * Author           George Keith Watson
 * Date Started:    July 28, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package model;

import java.util.TreeMap;

/**
 *
 * @author George Keith Watson
 * @param <DATA_TYPE>
 */
public final class TreeNode< DATA_TYPE > {
    
    private final String name;
    private DATA_TYPE data  = null;
    private final TreeMap< String, Attribute< String > > attributes   = new TreeMap< >();

    public TreeNode( final String name, final DATA_TYPE data )    {
        this.name   = name;
        this.data   = data;
    }

    public TreeNode( final String name )    {
        this.name   = name;
    }
    
    public final DATA_TYPE getData() {
        return data;
    }

    public final void setData( final DATA_TYPE data ) {
        this.data = data;
    }
    
    public final String getName() {
        return name;
    }

    public final TreeMap< String, Attribute< String > > getAttributes() {
        return attributes;
    }

    public final Attribute addAttribute( final String name, final String value )    {
        if ( name == null || name.length() == 0 || attributes.containsKey( name ) )
            return null;
        attributes.put( name, new Attribute( name, value ) );
        return attributes.get( name );
    }
    
    public final Attribute setAttribute( final String name, final String value )    {
        if ( name == null || name.length() == 0 )
            return null;
        attributes.put( name, new Attribute( name, value ) );
        return attributes.get( name );
    }
    
    public final Attribute removeAttribute( final String name )    {
        if ( name == null || name.length() == 0 || !attributes.containsKey( name ) )
            return null;
        return attributes.remove( name );
    }
    
    public final int removeAllAttributes( final String ... names )    {
        int attributeCount    =  0;
        for ( String name : names ) {
            if ( name != null && attributes.containsKey( name ) ) {
                attributes.remove( name );
                attributeCount++;
            }
        }
        return attributeCount;
    }
    
    public final int clearAttributes( )    {
        final int attributeCount    =  attributes.size();
        attributes.clear();
        return attributeCount;
    }
    
    /**
     * Add all attributes in the variable length argument list which are not already in this TreeNode's
     * attribute map, as identified by name.  To replace existing attribues which have the same
     * name. use setAttribute(( final String name, final String value )) or 
     * setAllAttributes( final Attribute ... attributes ).
     * @param attributes
     * @return 
     */
    public final int addAllAttributes( final Attribute ... attributes )    {
        int attributeCount  = 0;
        for ( Attribute attribute : attributes )    {
            if ( !this.attributes.containsKey( attribute.getName() ) )  {
                this.attributes.put( name, attribute );
                attributeCount++;
            }
        }
        return attributeCount;
    }
    
    /**
     * Set all attributes in the variable length argument list by replacing the Attribute instance with the
     * same name or adding it if the name / key is not already in this TreeNode's attribute map. 
     * Since the properties in an Attribute are immutable, the entire Attribute object mapped from
     * the name key must be replaced.
     * @param attributes
     * @return 
     */
    public final void setAllAttributes( final Attribute ... attributes )    {
        for ( Attribute attribute : attributes )
            this.attributes.put( name, attribute );
    }
    
}
