/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          GridLocation
 * Author           George Keith Watson
 * Date Started:    July 12, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view;

import java.awt.Point;

/**
 *
 * @author George Keith Watson
 */
public class GridLocation {
    
    private final int col;
    private final int row;
    private final int colSpan;
    private final int rowSpan;
    
    public GridLocation( final int col, final int row, final int colSpan, final int rowSpan )   {
        this.col        = col;
        this.row        = row;
        this.colSpan    = colSpan;
        this.rowSpan    = rowSpan;
    }

    public int getColSpan() {
        return colSpan;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
    
}
