/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          SimpleMathActivity
 * Author           George Keith Watson
 * Date Started:    July 18, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import model.FractionRecord;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveListView;
import view.nodes.InteractiveText;
import control.MathOperation;

/**
 *
 * @author George Keith Watson
 */
public class SimpleMathActivity extends AnchorPane {
    
    public static final String EVENT_MOUSE_CLICK        =  "MouseClickedSMA";
    public static final String EVENT_MOUSE_PRESS        =  "MousePressSMA";
    public static final String EVENT_MOUSE_RELEASED     =  "MouseReleasedSMA";
    public static final String EVENT_MOUSE_DRAG         =  "MouseDragSMA";
    public static final String EVENT_BUTTON_INCLUDE     =  "ButtonIncludeSMA";
                
    private final MathOperation mathOp;
    
    private final ArrayList< FractionRecord > argumentList          = new ArrayList< >();
    
    private final InteractiveLabel          labelActivity           = new InteractiveLabel( );
    private final ObservableList< String >  parametersList          = FXCollections.observableArrayList();
    private final InteractiveListView       listViewArguments       = new InteractiveListView( null );
    private final InteractiveLabel          labelResult             = new InteractiveLabel( "Result" );

    private final InteractiveButton         buttonInclude           = new InteractiveButton( "Include" );
    private final InteractiveText           textMathInstructions    = new InteractiveText( "Drag fractions to list to add.\nclick Include button to add to sequence.\n"
            + "Right click to erase this message." );
    
    private Integer operandCount    = 0;

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public SimpleMathActivity( final MathOperation op ) {
        super();
        mathOp  = op;
        final MessageManager eventListener  = new MessageManager();
        listViewArguments.registerEventListenerDepricated( eventListener ); 
        setId( op.toString() );
        switch( op )    {
            case ADDITION:
                labelActivity.setText( "Add:" );
                parametersList.add( "fraction to add" );
                parametersList.add( "fraction to add" );
                parametersList.add( "more" );
                break;
            case SUBTRACTION:
                labelActivity.setText( "Subtract:" );
                parametersList.add( "left side" );
                parametersList.add( "right side" );
                break;
            case MULTIPLICATION:
                labelActivity.setText( "Multiply:" );
                parametersList.add( "fraction to multiply" );
                parametersList.add( "fraction to multiply" );
                parametersList.add( "more" );
                break;
            case DIVISION:
                labelActivity.setText( "Divide:" );
                parametersList.add( "numerator" );
                parametersList.add( "denominator" );
                break;
        }
        listViewArguments.setItems( parametersList );
    }
    
    public void layoutComponents()  {
        getChildren().add( labelActivity );
        getChildren().add( listViewArguments );
        getChildren().add( labelResult );
        getChildren().add( buttonInclude );
        getChildren().add( textMathInstructions );

        labelActivity.setPrefWidth( 150 );
        labelActivity.setPrefHeight( 25 );
        labelActivity.setTranslateX( 10 );
        labelActivity.setTranslateY( 10 );
        
        listViewArguments.setPrefWidth( 150 ); 
        listViewArguments.setPrefHeight( 300 );
        listViewArguments.setTranslateX( 10 );
        listViewArguments.setTranslateY( 35 );
       
        labelResult.setPrefWidth( 150 );
        labelResult.setPrefHeight( 25 );
        labelResult.setTranslateX( 10 );
        labelResult.setTranslateY( 335 );

        buttonInclude.setPrefWidth( 150 );
        buttonInclude.setPrefHeight( 25 );
        buttonInclude.setTranslateX( 10 );
        buttonInclude.setTranslateY( 360 );
        
        buttonInclude.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle( ActionEvent event ) {
                sendMessage( event, EVENT_BUTTON_INCLUDE );
            }
        } ); 
        
        textMathInstructions.setTranslateX( 10 );
        textMathInstructions.setTranslateY( 405 );
    }
    
    public InteractiveListView getListViewArguments() {
        return listViewArguments;
    }
    
    public void addOperand( final Text textOperand )    {
        if ( operandCount == 0 ) {
            listViewArguments.getItems().clear();
            labelResult.setText( textOperand.getText() );
        }
        listViewArguments.getItems().add( textOperand.getText() );
        operandCount++;
        final FractionRecord left   = new FractionRecord( labelResult.getText() );
        final FractionRecord right   = new FractionRecord( listViewArguments.getItems().get( operandCount-1 ).toString() );
        FractionRecord result   = null;
        switch( mathOp )    {
            case NULL:
                break;
            case ADDITION:
                if ( operandCount > 1 ) {
                    result   = left.add( right );
                }
                break;
            case SUBTRACTION:
                if ( operandCount > 1 ) {
                    result   = left.subtract( right );
                }
                break;
            case MULTIPLICATION:
                if ( operandCount > 1 ) {
                    result   = left.multiply( right );
                }
                break;
            case DIVISION:
                if ( operandCount > 1 ) {
                    result   = left.divide( right );
                }
                break;
        }
        if ( result != null )
            labelResult.setText( result.toString() );
    }

    public String getResult() {
        return labelResult.getText();
    }
    
    private class MessageManager implements PropertyChangeListener   {
        @Override
        public void propertyChange( PropertyChangeEvent event ) {
            final Event originalEvent = ( Event )event.getOldValue();
            switch( ( String ) event.getNewValue() )    {
                case EVENT_MOUSE_CLICK:
                    System.out.println( "Mouse Click on listViewArguments");
                    break;
                case EVENT_MOUSE_PRESS:
                    System.out.println( "Mouse Press on listViewArguments");
                    break;
                case EVENT_MOUSE_RELEASED:
                    System.out.println( "Mouse Released on listViewArguments");
                    break;
                case EVENT_MOUSE_DRAG:
                    System.out.println( "Mouse Drag on listViewArguments");
                    break;
            }
        }
    }
    
    public void registerEventListener( final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    private void sendMessage( final Event event, final Object message )    {
        propertyChangeSupport.firePropertyChange( getId(), event, message );
    }

}
