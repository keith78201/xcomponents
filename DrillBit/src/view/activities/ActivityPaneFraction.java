/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ActivityPaneFraction
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.activities;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import model.FractionRecord;
import view.ActivityManager;
import view.GridLocation;
import view.nodes.InteractiveBarChart;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveComboBox;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveListView;
import view.nodes.InteractivePieChart;
import view.nodes.InteractiveTextField;
import view.nodes.InteractiveToggleButton;
import view.panes.StyledGridPane;


/**
 *
 * @author George Keith Watson
 */
public class ActivityPaneFraction extends StyledGridPane {
    
    public final static int INSET_TOP       = 25;
    public final static int INSET_RIGHT     = 25;
    public final static int INSET_BOTTOM    = 25;
    public final static int INSET_LEFT      = 25;
    
    public final static int MENU_BUTTON_MIN_WIDTH   = 125;
    
    public static final String TEXT_HIDE_DECIMAL       = "Decimal";
    public static final String TEXT_HIDE_TERMINOLOGY   = "Terminology";
    public static final String TEXT_HIDE_MIXED         = "Mixed";
    public static final String TEXT_HIDE_REDUCED       = "Reduced";
    
    private final FractionRecord fractionRecord = new FractionRecord( 1l, 1l );
    private final ObservableList< FractionRecord > sequence   = FXCollections.observableArrayList();
    private Integer currentFraction = -1;
    private boolean fractionSelectionChanged    = false;

    private final InteractiveToggleButton toggleButtonHideDecimal   = new InteractiveToggleButton( TEXT_HIDE_DECIMAL );
    private final InteractiveToggleButton toggleButtonHideLanguage  = new InteractiveToggleButton( TEXT_HIDE_TERMINOLOGY );
    private final InteractiveToggleButton toggleButtonHideMixed     = new InteractiveToggleButton( TEXT_HIDE_MIXED );
    private final InteractiveToggleButton toggleButtonHideReduced   = new InteractiveToggleButton( TEXT_HIDE_REDUCED );

    private final ObservableList< String > numberList       = FXCollections.observableArrayList();
    private final InteractiveListView listViewNumerator     = new InteractiveListView( null );
    private final InteractiveComboBox comboBoxNumerator     = new InteractiveComboBox( null );
    private final InteractiveListView listViewDenominator   = new InteractiveListView( null );
    private final InteractiveComboBox comboBoxDenominator   = new InteractiveComboBox( null );
    private final InteractiveButton buttonGenerate          = new InteractiveButton( "Generate" );
    
    private final ObservableList< String > languageList   = FXCollections.observableArrayList( 
            "N over D", 
            "N divided by D", 
            "N items of a set of D items", 
            "N/D * 100 percent" );
    private final InteractiveListView   listViewLanguage        = new InteractiveListView( languageList );
    
    private final Text                  textFractionDrill       = new Text( "Fraction Drill" );
    private final HBox                  boxFractionDrillLabel   = new HBox( 10 );
    
    private final InteractiveLabel      labelNumerator          = new InteractiveLabel( "Numerator:" );
    private final InteractiveLabel      labelDenominator        = new InteractiveLabel( "Denominator:" );
    private final InteractiveTextField  textFieldDecimalEq      = new InteractiveTextField( "decimal equivalent" );
    
    private final HBox                  hBoxMixed              = new HBox( 10 );
    private final InteractiveTextField  textFieldMixed         = new InteractiveTextField( "Mixed" );
    private final InteractiveTextField  textFieldReduced       = new InteractiveTextField( "Reduced" );

    private final ObservableList<PieChart.Data> pieChartData =
                                                    FXCollections.observableArrayList(
                                                    new PieChart.Data( "place holder", 50 ),
                                                    new PieChart.Data( "place holder", 50 ) );
    private final InteractivePieChart pieChart = new InteractivePieChart( "Fraction Slice", pieChartData );

    private final CategoryAxis yAxis    = new CategoryAxis();
    private final NumberAxis xAxis      = new NumberAxis();
    private final InteractiveBarChart< Number, String > barChart = new InteractiveBarChart<  >( xAxis, yAxis );
    private final XYChart.Series barChartData   = new XYChart.Series(); 
    
    private final String cssSimpleBorder = "-fx-border-color: darkcyan;\n"
                                        + "-fx-border-insets: 15;\n"
                                        + "-fx-border-width: 2;\n"
                                        + "-fx-border-style: solid;\n"; 
    
    private final Dimension contextDimensions;
    
    //  this is used in a grid positioning map in StyledGridPane:
    private final String[] componentNames       = { "boxFractionDrillLabel",
                                                    "labelNumerator",
                                                    "listViewNumerator",
                                                    "comboBoxNumerator",
                                                    "labelDenominator",
                                                    "listViewDenominator",
                                                    "comboBoxDenominator",
                                                    "buttonGenerate",
                                                    "toggleButtonHideDecimal",
                                                    "textFieldDecimalEq",
                                                    "toggleButtonHideLanguage",
                                                    "listViewLanguage",
                                                    "toggleButtonHideMixed",
                                                    "hBoxMixed",
                                                    "toggleButtonHideReduced",
                                                    "textFieldReduced",
                                                    "pieChart",
                                                    "barChart" };
    private final Node[] components = {             boxFractionDrillLabel,
                                                    labelNumerator,
                                                    listViewNumerator,
                                                    comboBoxNumerator,
                                                    labelDenominator,
                                                    listViewDenominator,
                                                    comboBoxDenominator,
                                                    buttonGenerate,
                                                    toggleButtonHideDecimal,
                                                    textFieldDecimalEq,
                                                    toggleButtonHideLanguage,
                                                    listViewLanguage,
                                                    toggleButtonHideMixed,
                                                    hBoxMixed,
                                                    toggleButtonHideReduced,
                                                    textFieldReduced,
                                                    pieChart,
                                                    barChart };
    private final GridLocation[] componsntLocations = { 
                                                    new GridLocation( 0, 1, 6, 1 ),     //boxFractionDrillLabel
                                                    new GridLocation( 0, 3, 1, 1 ),     //labelNumerator
                                                    new GridLocation( 1, 3, 1, 2 ),     //listViewNumerator
                                                    new GridLocation( 1, 3, 1, 1 ),     //comboBoxNumerator
                                                    new GridLocation( 0, 5, 1, 1 ),     //labelDenominator
                                                    new GridLocation( 1, 5, 1, 2 ),     //listViewDenominator
                                                    new GridLocation( 1, 5, 1, 1 ),     //comboBoxDenominator
                                                    new GridLocation( 1, 7, 1, 1 ),     //buttonGenerate
                                                    new GridLocation( 3, 3, 1, 1 ),     //toggleButtonHideDecimal
                                                    new GridLocation( 4, 3, 1, 1 ),     //textFieldDecimalEq
                                                    new GridLocation( 3, 4, 1, 1 ),     //toggleButtonHideLanguage
                                                    new GridLocation( 4, 4, 1, 1 ),     //listViewLanguage
                                                    new GridLocation( 3, 5, 1, 1 ),     //toggleButtonHideMixed
                                                    new GridLocation( 4, 5, 1, 1 ),     //hBoxMixed
                                                    new GridLocation( 3, 6, 1, 1 ),     //toggleButtonHideReduced
                                                    new GridLocation( 4, 6, 1, 1 ),     //textFieldReduced
                                                    new GridLocation( 3, 8, 3, 1 ),     //pieChart
                                                    new GridLocation( 3, 8, 3, 1 )      //barChart
                                                    };
    

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public ActivityPaneFraction( final Dimension contextDimensions )   {
        super();
        this.contextDimensions  = contextDimensions;
        
        toggleButtonHideDecimal.enableFadeEffect( 900, 0.4 );
        toggleButtonHideLanguage.enableFadeEffect( 900, 0.4 );
        toggleButtonHideMixed.enableFadeEffect( 900, 0.4 );
        toggleButtonHideReduced.enableFadeEffect( 900, 0.4 );
        
        for ( int number = 1; number < 1000; number++ )  {
            numberList.add( String.valueOf( number ) );
        }
        listViewNumerator.setItems( numberList );
        comboBoxNumerator.setItems( numberList );
        listViewDenominator.setItems( numberList ); 
        comboBoxDenominator.setItems( numberList );
        listViewNumerator.getSelectionModel().setSelectionMode( SelectionMode.SINGLE ); 
        listViewDenominator.getSelectionModel().setSelectionMode( SelectionMode.SINGLE ); 
        
        setAlignment( Pos.CENTER );
        setHgap( 10 );
        setVgap( 10 );
        setPadding( new Insets( INSET_TOP, INSET_RIGHT, INSET_BOTTOM, INSET_LEFT ) );

        toggleButtonHideDecimal.setId( "toggleButtonHideDecimal" );
        toggleButtonHideLanguage.setId( "toggleButtonHideLanguage" );
        toggleButtonHideMixed.setId( "toggleButtonHideMixed" );
        toggleButtonHideReduced.setId( "toggleButtonHideReduced" );
    
        listViewNumerator.setMaxWidth( 100 );
        listViewDenominator.setMaxWidth( 100 );
        comboBoxNumerator.setMaxWidth( 100 );
        comboBoxDenominator.setMaxWidth( 100 );
        listViewNumerator.setId( "listViewNumerator" );
        comboBoxNumerator.setId( "comboBoxNumerator" );
        listViewDenominator.setId( "listViewDenominator" );
        comboBoxDenominator.setId( "comboBoxDenominator" );
        listViewNumerator.getSelectionModel().select( 0 );
        listViewDenominator.getSelectionModel().select( 0 );
        comboBoxNumerator.getSelectionModel().select( 0 );
        comboBoxDenominator.getSelectionModel().select( 0 );
        
        labelNumerator.setFont( Font.font( "Tahoma", FontWeight.BOLD, 14 ) );
        labelDenominator.setFont( Font.font( "Tahoma", FontWeight.BOLD, 14 ) );
        
        textFractionDrill.setId( "textFractionDrill" ); 
        textFractionDrill.setFont( Font.font( "Tahoma", FontWeight.BOLD, 16 ) );
        boxFractionDrillLabel.getChildren().add( textFractionDrill );
        boxFractionDrillLabel.setAlignment( Pos.CENTER ); 
        textFieldDecimalEq.setEditable( false );
        
        pieChart.setLabelsVisible( false ); 
        pieChart.setLegendVisible( true );
        pieChart.setLegendSide( Side.LEFT );
        pieChart.getData().set( 0, new PieChart.Data( "Fraction", 1 ) );
        pieChart.getData().set( 1, new PieChart.Data( "Other", 1 ) );
        
        barChart.setVisible( false );
        barChartData.getData().add( new XYChart.Data< Number, String >( 2.5, "fraction" ) );
        barChartData.getData().add( new XYChart.Data< Number, String >( 4.0, "scale" ) );
        barChart.getData().add( barChartData );
        barChart.setBarGap( 5 ); 
        barChart.setCategoryGap( 20 ); 
        
        textFieldMixed.setEditable( false );
        textFieldReduced.setEditable( false );
        comboBoxNumerator.setVisible( false );
        comboBoxDenominator.setVisible( false );
        hBoxMixed.getChildren().add( textFieldMixed );
        boxFractionDrillLabel.setId( "boxFractionDrillLabel" ); 
        for ( int i = 0; i < componentNames.length; i++ )    {
            add( components[ i ], componsntLocations[ i ] );
        }
    }

    public FractionRecord getFraction() {
        return fractionRecord;
    }
    
    public InteractiveToggleButton getToggleButtonHideDecimal() {
        return toggleButtonHideDecimal;
    }

    public InteractiveLabel getLabelNumerator() {
        return labelNumerator;
    }

    public InteractiveLabel getLabelDenominator() {
        return labelDenominator;
    }

    public InteractiveTextField getTextFieldDecimalEq() {
        return textFieldDecimalEq;
    }

    public InteractiveListView getListViewNumerator() {
        return listViewNumerator;
    }

    public InteractiveListView getListViewDenominator() {
        return listViewDenominator;
    }

    public InteractiveListView getListViewLanguage() {
        return listViewLanguage;
    }

    public InteractiveToggleButton getToggleButtonHideLanguage() {
        return toggleButtonHideLanguage;
    }

    public InteractiveToggleButton getToggleButtonHideMixed() {
        return toggleButtonHideMixed;
    }
    

    public InteractiveButton getButtonGenerate() {
        return buttonGenerate;
    }
    
    public InteractiveToggleButton getToggleButtonHideReduced() {
        return toggleButtonHideReduced;
    }
    
    public InteractiveTextField getTextFieldMixed() {
        return textFieldMixed;
    }

    public InteractiveTextField getTextFieldReduced() {
        return textFieldReduced;
    }

    public InteractivePieChart getPieChart() {
        return pieChart;
    }

    public InteractiveBarChart getBarChart() {
        return barChart;
    }

    public InteractiveComboBox getComboBoxNumerator() {
        return comboBoxNumerator;
    }

    public InteractiveComboBox getComboBoxDenominator() {
        return comboBoxDenominator;
    }
    
    public ObservableList<FractionRecord> getSequence() {
        return sequence;
    }
    
    public Integer getCurrentFraction() {
        return currentFraction;
    }

    public void setCurrentFraction( final Integer currentFraction ) {
        if ( currentFraction >= 0 && currentFraction < sequence.size() && 
                !this.currentFraction.equals( currentFraction ) )  {
            fractionSelectionChanged    = true;
            this.currentFraction    = currentFraction;
        }
        
    }

    public void registerEventListener( final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    private void sendMessage( final Event event, final Object message )    {
        propertyChangeSupport.firePropertyChange( getId(), event, message );
    }

    public void handleListSelectEvent( final Event event ) {
        if ( fractionSelectionChanged )  {
            fractionRecord.setNumerator( Long.parseLong( sequence.get( currentFraction ).getNumerator() ) );
            fractionRecord.setDenominator(Long.parseLong( sequence.get( currentFraction ).getDenominator() ) );

            comboBoxNumerator.getSelectionModel().clearAndSelect( Integer.parseInt(fractionRecord.getNumerator() ) ); 
            listViewNumerator.getSelectionModel().clearAndSelect( Integer.parseInt(fractionRecord.getNumerator() ) ); 
            comboBoxDenominator.getSelectionModel().clearAndSelect( Integer.parseInt(fractionRecord.getDenominator() ) ); 
            listViewDenominator.getSelectionModel().clearAndSelect( Integer.parseInt(fractionRecord.getDenominator() ) ); 
            
            fractionSelectionChanged    = false;
        }
        else {
            if ( listViewNumerator.isVisible() )     {
                fractionRecord.setNumerator( Long.parseLong( listViewNumerator.getSelectionModel().getSelectedItem().toString() ) );
                comboBoxNumerator.getSelectionModel().clearAndSelect( listViewNumerator.getSelectionModel().getSelectedIndex() ); 
            }
            else if ( comboBoxNumerator.isVisible() )    {
                fractionRecord.setNumerator( Long.parseLong( comboBoxNumerator.getSelectionModel().getSelectedItem().toString() ) );
                listViewNumerator.getSelectionModel().clearAndSelect( comboBoxNumerator.getSelectionModel().getSelectedIndex() ); 
            }

            if ( listViewDenominator.isVisible() )   {
                fractionRecord.setDenominator( Long.parseLong( listViewDenominator.getSelectionModel().getSelectedItem().toString() ) );
                comboBoxDenominator.getSelectionModel().clearAndSelect( listViewDenominator.getSelectionModel().getSelectedIndex() ); 
            }
            else if ( comboBoxDenominator.isVisible() )  {
                fractionRecord.setDenominator( Long.parseLong( comboBoxDenominator.getSelectionModel().getSelectedItem().toString() ) );
                listViewDenominator.getSelectionModel().clearAndSelect( comboBoxDenominator.getSelectionModel().getSelectedIndex() ); 
            }
            sequence.add( new FractionRecord( fractionRecord ) );
            
            sendMessage( event, ActivityManager.MESSAGE_LIST_ADD );
        }
        

        textFieldDecimalEq.setText(  fractionRecord.getValue().toString() ); 

        fractionRecord.generateTerminology( listViewLanguage.getItems() );

        if ( fractionRecord.isProper() ) {
            pieChart.getData().set( 0, new PieChart.Data( "Fraction", fractionRecord.getValue() ) );
            pieChart.getData().set( 1, new PieChart.Data( "Other", 1 - fractionRecord.getValue() ) );
        }
        else    {
            pieChart.getData().set( 0, new PieChart.Data( "Fraction", 1 ) );
            pieChart.getData().set( 1, new PieChart.Data( "Other", 0 ) );
        }

        final Double scale = Math.ceil( fractionRecord.getValue() * ( 1 + Math.random() ) );
        while ( barChartData.getData().size() > 0 )
            barChartData.getData().remove( 0 );
        barChartData.getData().add( new XYChart.Data< Number, String >( fractionRecord.getValue(), "fraction" ) );
        barChartData.getData().add( new XYChart.Data< Number, String >( scale, "scale" ) );

        if ( fractionRecord.isProper() ) {
            textFieldMixed.setText( "Not Improper" ); 
        }
        else    {
            textFieldMixed.setText( fractionRecord.getMixed() ); 
        }
        textFieldReduced.setText( fractionRecord.getReduced() ); 
   }
}
