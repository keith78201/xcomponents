/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ActivityManager
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view;

import control.ControlType;
import control.MathOperation;
import control.SimplePropertyType;
import control.StateAbbr;
import control.TreeDescriptorException;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.RadioMenuItemBuilder;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import model.FractionRecord;
import model.ModelingTables;
import model.ObservableProperty;
import model.OhmsWheelRecord;
import model.filter.AllowedValuesList;
import model.filter.ListDescriptor;
import model.filter.TreeDescriptor;
import test.model.TestRecords;
import view.activities.ActivityPaneFraction;
import static view.activities.ActivityPaneFraction.MENU_BUTTON_MIN_WIDTH;
import view.activities.SimpleMathActivity;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveCheckBox;
import view.nodes.InteractiveComboBox;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveListView;
import view.nodes.InteractiveModelingTable;
import view.nodes.InteractiveRadioButton;
import view.nodes.InteractiveToggleButton;
import view.panes.BorderedBrowser;
import view.panes.BorderedHTMLeditor;
import view.panes.Browser;
import view.panes.PropertyList;
import view.panes.TreePane;
import view.panes.TreeViewDesigner;

/**
 * This has five regions, like a Border Layout, the center for content, the edges for controls that manage
 * what content appears and how.
 * 
 * @author George Keith Watson
 */
public class ActivityManager extends BorderPane {
 
    public final static String MESSAGE_ACTION           = "ActionAM";
    public final static String MESSAGE_LIST_VALUE_SEL   = "List Value Selection";
    public final static String MESSAGE_KEY_PRESSED      = "KeyPressedAM";
    public final static String MESSAGE_KEY_TYPED        = "KeyTypedAM";
    public final static String MESSAGE_MOUSE_CLICKED    = "MouseClickedAM";
    public final static String MESSAGE_MOUSE_PRESSED    = "MousePressedAM";
    public final static String MESSAGE_MOUSE_RELEASED   = "MouseReleasedAM";
    public final static String MESSAGE_MOUSE_DRAGGED    = "MouseDraggedAM";
    
    public final static String  LIST_ITEM_HISTORY       = "History";

    public final static String MESSAGE_LIST_ADD         = "Fraction Added to List";
    
    public final static int SCENE_WIDTH     = 950;
    public final static int SCENE_HEIGHT    = 500;
    
    private final ActivityPaneFraction      activityPaneFraction            = new ActivityPaneFraction( new Dimension( SCENE_WIDTH, SCENE_HEIGHT ));
    
    public final static int FRACTION_TABLE_INDEX        = 0;
    public final static int OHMS_WHEEL_TABLE_INDEX      = 1;
    
    private final InteractiveButton buttonFractionDrill     = new InteractiveButton( "Fraction Drill");
    private final InteractiveButton buttonFractionMath      = new InteractiveButton( "Fraction Math");
    private final InteractiveButton buttonOhmsLaw           = new InteractiveButton( "Ohms Law");
    private final InteractiveButton buttonPowerNEnergy      = new InteractiveButton( "Power & Energy");
    private final InteractiveButton buttonAmpacity          = new InteractiveButton( "Ampacity");
    private final InteractiveLabel  labelSpacerA            = new InteractiveLabel(  "");
    private final InteractiveLabel  labelSpacerB            = new InteractiveLabel(  "");
    private final InteractiveButton buttonControls          = new InteractiveButton( "Controls");
    private final InteractiveButton buttonPickChart         = new InteractiveButton( "Pick Chart");
    private final InteractiveButton buttonPickList          = new InteractiveButton( "Pick List");

    private final InteractiveButton     buttonExit              = new InteractiveButton( "Exit" );
    private final HBox                  boxExitButton           = new HBox( 10 );

    private final Node[] tools  = { buttonFractionDrill, buttonFractionMath, buttonOhmsLaw, 
                                    buttonPowerNEnergy, buttonAmpacity, labelSpacerA, 
                                    buttonControls, buttonPickChart, buttonPickList, labelSpacerB, buttonExit };       //new Node[4];
    private final ToolBar mainToolBar  = new ToolBar( tools );
    
    private final ToggleGroup toggleGroupToolsCharts  = new ToggleGroup();
    private final InteractiveRadioButton radioButtonPieChart    = new InteractiveRadioButton( "Pie Chart" );
    private final InteractiveRadioButton radioButtonBarChart    = new InteractiveRadioButton( "Bar Chart" );
    private final Node[] optionToolsCharts  = { radioButtonPieChart, radioButtonBarChart };
    
    private final InteractiveButton buttonNext              = new InteractiveButton( "Next");
    private final InteractiveButton buttonPrev              = new InteractiveButton( "Prev");
    private final InteractiveCheckBox checkBoxShowSequence  = new InteractiveCheckBox( "Show Sequence");
    private final InteractiveCheckBox checkBoxOptionTwo     = new InteractiveCheckBox( "Option Two");
    private final InteractiveCheckBox checkBoxOptionThree   = new InteractiveCheckBox( "Option Three");
    private final InteractiveCheckBox checkBoxOptionFour    = new InteractiveCheckBox( "Option Four");
    private final InteractiveCheckBox checkBoxOptionFive    = new InteractiveCheckBox( "Option Five");
    private final Node[] toolsDrills  = { buttonPrev, buttonNext, checkBoxShowSequence, checkBoxOptionTwo, checkBoxOptionThree, 
                                    checkBoxOptionFour, checkBoxOptionFive };
    
    private final InteractiveRadioButton radioButtonListView    = new InteractiveRadioButton( "List View" );
    private final InteractiveRadioButton radioButtonComboBox    = new InteractiveRadioButton( "Combo Box" );
    private final Node[] toolsLists  = { radioButtonListView, radioButtonComboBox };
    private final ToggleGroup toggleGroupToolsLists  = new ToggleGroup( );
    
    private final ObservableList< String >  mathOperationsList   = FXCollections.observableArrayList();
    private final InteractiveComboBox       comboBoxMathOperations    = new InteractiveComboBox( null );

    private final ObservableList< String >  listSequence                = FXCollections.observableArrayList();
    private final InteractiveListView       listViewSequence            = new InteractiveListView( listSequence );
    private final InteractiveLabel          labelMath                   = new InteractiveLabel( "Select math\noperaitons from\nmenu at top" );
    private final Node[]                    toolsSequence               = { comboBoxMathOperations, listViewSequence, labelMath };
    
    private final ToolBar toolBarDrills     = new ToolBar( toolsDrills );
    private final ToolBar toolBarCharts     = new ToolBar( optionToolsCharts );
    private final ToolBar toolBarLists      = new ToolBar( toolsLists );
    private final ToolBar toolBarSequence   = new ToolBar( toolsSequence );;

    private final SimpleMathActivity additionActivity       = new SimpleMathActivity( MathOperation.ADDITION );
    private final SimpleMathActivity subtractionActivity    = new SimpleMathActivity( MathOperation.SUBTRACTION );
    private final SimpleMathActivity multiplicationActivity = new SimpleMathActivity( MathOperation.MULTIPLICATION );
    private final SimpleMathActivity divisionActivity       = new SimpleMathActivity( MathOperation.DIVISION );

    private MathOperation currentMathActivity       = MathOperation.NULL;
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    private final static ObservableList< FractionRecord >  fractionData = FXCollections.observableArrayList(
            new FractionRecord( 6l, 8l ),
            new FractionRecord( 50l, 10l ),
            new FractionRecord( 27l, 18l ),
            new FractionRecord( null, null )
    );
    
    private final static ObservableList< OhmsWheelRecord >  ohmsWheelData = FXCollections.observableArrayList(
            new OhmsWheelRecord( "1.0", "1.0", "1.0", "1.0", "1.0", "1.0" ),
            new OhmsWheelRecord( "2.0", "1.0", "2.0", "2.0", "1.0", "2.0" ),
            new OhmsWheelRecord( "1.0", "2.0", "0.5", "2.0", "3.0", "6.0" ),
            new OhmsWheelRecord( "5.0", "0.05", "100.0", "0.25", "1.0", "0.25" ),
            new OhmsWheelRecord( "120.0", "15.0", "8.0", "1800.0", "12.0", "18360.0" ),
            new OhmsWheelRecord( "", "", "", "", "", "" )
    );
    
    private final static InteractiveModelingTable< FractionRecord > fractionTable = new InteractiveModelingTable<  >
        ( fractionData,
            FractionRecord.VIEW_FILD_NAME_FRACTION,
            FractionRecord.VIEW_FILD_NAME_DECIMAL, 
            FractionRecord.VIEW_FILD_NAME_PROPER, 
            FractionRecord.VIEW_FILD_NAME_TERMINOLOGY, 
            FractionRecord.VIEW_FILD_NAME_REDUCED );
    
    private final static InteractiveModelingTable< OhmsWheelRecord > ohmsWheelTable = new InteractiveModelingTable<  >
        ( ohmsWheelData,
            OhmsWheelRecord.VIEW_FIELD_NAME_VOLTAGE, 
            OhmsWheelRecord.VIEW_FIELD_NAME_CURRENT,
            OhmsWheelRecord.VIEW_FIELD_NAME_RESISTANCE,
            OhmsWheelRecord.VIEW_FIELD_NAME_POWER,
            OhmsWheelRecord.VIEW_FIELD_NAME_TIME,
            OhmsWheelRecord.VIEW_FIELD_NAME_ENERGY );
    
    private final static ModelingTables modelingTables = new ModelingTables();

    private         DragAction  currentDragAction   = null;
    private         Text textDrag = null;

    private final BorderedHTMLeditor    borderedHTMLeditor = new BorderedHTMLeditor();
    
    private final MainMenuBar mainMenuBar   = new MainMenuBar();
    
    
    public ActivityManager()    {
        initializeModelingTables();

        radioButtonComboBox.setToggleGroup( toggleGroupToolsLists );
        radioButtonListView.setToggleGroup( toggleGroupToolsLists );
        final EventHandler< ActionEvent > listViewSelectorHandler  = new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                activityPaneFraction.getListViewNumerator().setVisible( radioButtonListView.isSelected() );
                activityPaneFraction.getListViewDenominator().setVisible( radioButtonListView.isSelected() );
                activityPaneFraction.getComboBoxNumerator().setVisible( radioButtonComboBox.isSelected() );
                activityPaneFraction.getComboBoxDenominator().setVisible( radioButtonComboBox.isSelected() );
            }
        };
        radioButtonComboBox.setOnAction( listViewSelectorHandler );
        radioButtonListView.setOnAction( listViewSelectorHandler );
                 
        radioButtonPieChart.setToggleGroup( toggleGroupToolsCharts );
        radioButtonBarChart.setToggleGroup( toggleGroupToolsCharts );
        final EventHandler< ActionEvent > chartSelectorHandler  = new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                activityPaneFraction.getPieChart().setVisible( radioButtonPieChart.isSelected() );
                activityPaneFraction.getBarChart().setVisible( radioButtonBarChart.isSelected() );
            }
        };
        radioButtonPieChart.setOnAction( chartSelectorHandler );
        radioButtonBarChart.setOnAction( chartSelectorHandler ); 
        radioButtonPieChart.setSelected( true ); 
        
        setActionListeners();
        final MessageManager eventListener  = new MessageManager();
        activityPaneFraction.getButtonGenerate().registerEventListener( eventListener );
        activityPaneFraction.getComboBoxDenominator().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getComboBoxNumerator().registerEventListenerDepricated( eventListener );
        //activityPaneFraction.getLabelDenominator().registerEventListenerDepricated( eventListener );
        //activityPaneFraction.getLabelNumerator().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getListViewDenominator().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getListViewLanguage().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getListViewNumerator().registerEventListenerDepricated( eventListener );
        //activityPaneFraction.getPieChart().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getTextFieldDecimalEq().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getTextFieldMixed().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getTextFieldReduced().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getToggleButtonHideDecimal().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getToggleButtonHideLanguage().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getToggleButtonHideMixed().registerEventListenerDepricated( eventListener );
        activityPaneFraction.getToggleButtonHideReduced().registerEventListenerDepricated( eventListener );
        
        additionActivity.registerEventListener( eventListener );
        subtractionActivity.registerEventListener( eventListener );
        multiplicationActivity.registerEventListener( eventListener );
        divisionActivity.registerEventListener( eventListener );
        
        activityPaneFraction.registerEventListener( eventListener ); 
        
        buttonFractionDrill.setMinWidth( MENU_BUTTON_MIN_WIDTH );
        buttonFractionMath.setMinWidth( MENU_BUTTON_MIN_WIDTH );
        buttonOhmsLaw.setMinWidth( MENU_BUTTON_MIN_WIDTH );
        buttonPowerNEnergy.setMinWidth( MENU_BUTTON_MIN_WIDTH );
        buttonAmpacity.setMinWidth( MENU_BUTTON_MIN_WIDTH );
        boxExitButton.getChildren().add( buttonExit );
        boxExitButton.setAlignment( Pos.CENTER );
        
        initMathContextMenu();
        //comboBoxMathOperations.setVisible( false );
        toolBarDrills.setOrientation( Orientation.HORIZONTAL );
        toolBarSequence.setOrientation( Orientation.VERTICAL );
        toolBarCharts.setOrientation( Orientation.HORIZONTAL ); 
        toolBarLists.setOrientation( Orientation.HORIZONTAL ); 
        listSequence.add( LIST_ITEM_HISTORY );
        listViewSequence.setPrefWidth( 100 ); 
        listViewSequence.setPrefHeight( 300 );
        
        enableSequenceListDragDrop();
        
        radioButtonListView.setSelected( true ); 
        
        mainToolBar.setOrientation( Orientation.VERTICAL ); 
        additionActivity.layoutComponents();
        subtractionActivity.layoutComponents();
        multiplicationActivity.layoutComponents();
        divisionActivity.layoutComponents();
        
        setCenter( activityPaneFraction );
        setRight( mainToolBar );
        setBottom( toolBarDrills );
        setTop( mainMenuBar );
    }
    
    private void disableSequenceListDragDrop()  {
        listViewSequence.setOnMousePressed( null ); 
        listViewSequence.setOnMouseReleased( null ); 
        listViewSequence.setOnMouseDragged( null );
    }

    
    private void enableSequenceListDragDrop()   {
        listViewSequence.setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( currentDragAction == null )        {
                    if ( listViewSequence.getSelectionModel().getSelectedItem() != null )   {
                        final String textToDrag = listViewSequence.getSelectionModel().getSelectedItem().toString();
                        if ( !textToDrag.equals( LIST_ITEM_HISTORY ) )
                        currentDragAction = new DragAction( new Point2D( 
                                event.getSceneX(), event.getSceneY()), listViewSequence, textToDrag );
                        }
                }
            }
        } ); 
        listViewSequence.setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                if ( event.getButton() == MouseButton.PRIMARY)  {
                    if ( currentDragAction != null )    {
                        final Bounds sequenceToolBarBounds  = toolBarSequence.getBoundsInParent();
                        Bounds listBounds   = null;
                        switch( currentMathActivity )  {
                            case NULL:
                                break;
                            case ADDITION:
                                listBounds  = additionActivity.getListViewArguments().getBoundsInParent();
                                break;
                            case SUBTRACTION:
                                listBounds  = subtractionActivity.getListViewArguments().getBoundsInParent();
                                break;
                            case MULTIPLICATION:
                                listBounds  = multiplicationActivity.getListViewArguments().getBoundsInParent();
                                break;
                            case DIVISION:
                                listBounds  = divisionActivity.getListViewArguments().getBoundsInParent();
                                break;
                        }
                        if ( listBounds != null )   {
                            if ( listBounds.contains( 
                                event.getSceneX() - sequenceToolBarBounds.getMinX() - sequenceToolBarBounds.getWidth(), 
                                event.getSceneY() - mainMenuBar.getHeight() ) )     {
                                if ( !textDrag.getText().equals( LIST_ITEM_HISTORY ) )  {
                                    switch( currentMathActivity )  {
                                        case NULL:
                                            break;
                                        case ADDITION:
                                            additionActivity.addOperand( textDrag );
                                            break;
                                        case SUBTRACTION:
                                            subtractionActivity.addOperand( textDrag );
                                            break;
                                        case MULTIPLICATION:
                                            multiplicationActivity.addOperand( textDrag );
                                            break;
                                        case DIVISION:
                                            divisionActivity.addOperand( textDrag );
                                            break;
                                    }
                                    textDrag.setVisible( false );
                                    textDrag = null;
                                }
                            }
                            else    {
                                textDrag.setVisible( false );
                                textDrag = null;
                            }
                        }
                        currentDragAction = null; 
                    }
                }
            }
        } ); 
        listViewSequence.setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( currentDragAction != null )    {
                    if ( textDrag != null )
                        textDrag.setVisible( false );
                    textDrag = new Text( event.getSceneX(), event.getSceneY(), currentDragAction.getContent().toString() );
                    ActivityManager.this.getChildren().add( textDrag );
                }
            }
        } ); 
        listViewSequence.setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                //if ( event.getButton() == MouseButton.SECONDARY ) {
                //    comboBoxMathOperations.setVisible( !comboBoxMathOperations.isVisible() );
                //}
            }
        } ); 
    }
    
    private void setActionListeners() {
        buttonExit.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                System.exit( 0 ); 
            }
        } );
        buttonPickChart.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setBottom( toolBarCharts );
            }
        } );
        buttonPickList.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setBottom( toolBarLists );
            }
        } );
        buttonControls.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setBottom( toolBarDrills );
            }
        } );
        buttonFractionDrill.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setCenter( activityPaneFraction );
            }
        } ); 
        buttonFractionMath.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setCenter( fractionTable );
            }
        } ); 
        buttonOhmsLaw.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setCenter( ohmsWheelTable );
            }
        } ); 
        buttonPowerNEnergy.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setCenter( activityPaneFraction );
            }
        } ); 
        buttonAmpacity.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                setCenter( activityPaneFraction );
            }
        } ); 

        buttonNext.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle( ActionEvent event ) {
                if ( activityPaneFraction.getSequence().size() > 0 )    {
                    if ( activityPaneFraction.getCurrentFraction() > -1  && 
                            activityPaneFraction.getCurrentFraction() < activityPaneFraction.getSequence().size() - 1 ) {
                        activityPaneFraction.setCurrentFraction( activityPaneFraction.getCurrentFraction() + 1 ); 
                        activityPaneFraction.handleListSelectEvent( event );
                    }
                }
            }
        } ); 
        buttonPrev.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                if ( activityPaneFraction.getSequence().size() > 1 )    {
                    if ( activityPaneFraction.getCurrentFraction() == -1 ) {
                        activityPaneFraction.setCurrentFraction( activityPaneFraction.getSequence().size() - 2 ); 
                        activityPaneFraction.handleListSelectEvent( event );
                    }
                    else if ( activityPaneFraction.getCurrentFraction() > 0 ) {
                        activityPaneFraction.setCurrentFraction( activityPaneFraction.getCurrentFraction() - 1 );
                        activityPaneFraction.handleListSelectEvent( event );
                    }
                }
            }
        } ); 
        checkBoxShowSequence.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                if ( checkBoxShowSequence.isSelected() )
                    setLeft( toolBarSequence );
                else
                    setLeft( null );

            }
        } ); 
        checkBoxOptionTwo.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 
        checkBoxOptionThree.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 
        checkBoxOptionFour.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 
        checkBoxOptionFive.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 
    }
        
    private void initMathContextMenu()  {
        mathOperationsList.add( "Math" );
        mathOperationsList.add( MathOperation.ADDITION.toString() );
        mathOperationsList.add( MathOperation.SUBTRACTION.toString() );
        mathOperationsList.add( MathOperation.MULTIPLICATION.toString() );
        mathOperationsList.add( MathOperation.DIVISION.toString() );
        comboBoxMathOperations.setItems( mathOperationsList );
        comboBoxMathOperations.setId( "Math" );        
        comboBoxMathOperations.getSelectionModel().clearAndSelect( 0 );
        
        comboBoxMathOperations.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                switch( comboBoxMathOperations.getSelectionModel().getSelectedItem().toString() )    {
                    case "Addition":
                        setCenter( additionActivity );
                        currentMathActivity   = MathOperation.ADDITION;
                        break;
                    case "Subtraction":
                        setCenter( subtractionActivity );
                        currentMathActivity   = MathOperation.SUBTRACTION;
                        break;
                    case "Multiplication":
                        setCenter( multiplicationActivity );
                        currentMathActivity   = MathOperation.MULTIPLICATION;
                        break;
                    case "Division":
                        setCenter( divisionActivity );
                        currentMathActivity   = MathOperation.DIVISION;
                        break;
                    default:
                        break;
                }
            }
        } ); 
        comboBoxMathOperations.setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                comboBoxMathOperations.getSelectionModel().getSelectedItem().toString();
            }
        }); 
        comboBoxMathOperations.setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
            }
        }); 
        comboBoxMathOperations.setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
            }
        }); 
    }
    
    
    public ToolBar getOptionToolBarDrills() {
        return toolBarDrills;
    }

    public ToolBar getOptionToolBarCharts() {
        return toolBarCharts;
    }
    
    public ToolBar getOptionToolBarLists() {
        return toolBarLists;
    }
    
    public InteractiveButton getButtonExit() {
        return buttonExit;
    }

    public InteractiveButton getButtonFractionDrill() {
        return buttonFractionDrill;
    }

    public InteractiveButton getButtonFractionMath() {
        return buttonFractionMath;
    }

    public InteractiveButton getButtonOhmsLaw() {
        return buttonOhmsLaw;
    }

    public InteractiveButton getButtonPowerNEnergy() {
        return buttonPowerNEnergy;
    }

    public InteractiveButton getButtonAmpacity() {
        return buttonAmpacity;
    }
    
    public InteractiveButton getButtonControls() {
        return buttonControls;
    }

    public InteractiveButton getButtonPickChart() {
        return buttonPickChart;
    }

    public InteractiveButton getButtonPickList() {
        return buttonPickList;
    }
    
    public InteractiveRadioButton getRadioButtonListView() {
        return radioButtonListView;
    }

    public InteractiveRadioButton getRadioButtonComboBox() {
        return radioButtonComboBox;
    }

    
   private class MainMenuBar extends MenuBar  {
       
       File chosenFile  = null;
       
       public MainMenuBar   ()  {
            final Menu menuFile = new Menu("File");
            final MenuItem menuItemNew   = new MenuItem( "New Macro" );
            final MenuItem menuItemRead  = new MenuItem( "Read Macro" );
            final MenuItem menuItemSave  = new MenuItem( "Save Macro" );
            final MenuItem menuItemExit  = new MenuItem( "Exit" );
            menuItemExit.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                    System.exit( 0 ); 
                }
            } ); 
            menuFile.getItems().addAll( menuItemNew, menuItemRead, menuItemSave, menuItemExit );

            final Menu menuEdit = new Menu( "Edit" );
            final MenuItem menuItemNote   = new MenuItem( "Note" );
            menuItemNote.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                    if ( chosenFile == null )   {
                        borderedHTMLeditor.setHTML( "<html><head><body><p><h4>Hello World!</body></head></html>" );
                    } else  {
                        FileReader reader   = null;
                        try {
                            reader   = new FileReader( chosenFile );
                            char[] fileBuffer   = new char[ ( int )chosenFile.length() ];
                            reader.read( fileBuffer );
                            borderedHTMLeditor.setHTML( String.copyValueOf( fileBuffer ) );
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                        } finally    {
                            if ( reader != null )
                                try {
                                    reader.close();
                                } catch (IOException ex) {
                                    Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                    }
                    setCenter( borderedHTMLeditor );
                }
            } ); 
            menuEdit.getItems().addAll( menuItemNote );
            
            final Menu menuMacros = new Menu( "Macros" );
            final MenuItem menuItemStartMacroRec    = new MenuItem( "Start Recording Macro" );
            final MenuItem menuItemStopMacroRec     = new MenuItem( "Stop Recording Macro" );
            final MenuItem menuItemSelectMacro      = new MenuItem( "Select Macro" );
            final MenuItem menuItemPlayMacro        = new MenuItem( "Play Current Macro" );
            menuMacros.getItems().addAll( menuItemStartMacroRec, menuItemStopMacroRec, menuItemSelectMacro, menuItemPlayMacro );

            final Menu menuStyles = new Menu( "Styles" );
            final CheckMenuItem menuItemStyleFxDefault  = new CheckMenuItem( "Style FX Default" );
            final CheckMenuItem menuItemStyleEarth      = new CheckMenuItem( "Style Earth" );
            final CheckMenuItem menuItemStyleSteel      = new CheckMenuItem( "Style Steel" );
            final CheckMenuItem menuItemStyleWater      = new CheckMenuItem( "Style Water" );
            final CheckMenuItem menuItemStyleWood       = new CheckMenuItem( "Style Wood" );
            menuItemStyleFxDefault.setSelected( true ); 
            menuStyles.getItems().addAll( menuItemStyleFxDefault,menuItemStyleEarth, menuItemStyleSteel, 
                    menuItemStyleWater, menuItemStyleWood );

            final Menu menuSchedlues = new Menu( "Schedule" );

            final Menu menuAlarm = new Menu( "Alarm" );
            final ToggleGroup tGroup = new ToggleGroup();
            final RadioMenuItem soundAlarmItem = RadioMenuItemBuilder.create().toggleGroup(tGroup).text("Sound Alarm").build();
            final RadioMenuItem stopAlarmItem = RadioMenuItemBuilder.create().toggleGroup(tGroup).text("Alarm Off").selected(true).build();
            menuAlarm.getItems().addAll( soundAlarmItem, stopAlarmItem );
            
            final Menu menuContingencyPlans = new Menu( "Contingent Plans" );
            menuContingencyPlans.getItems().add(new CheckMenuItem( "Self Destruct in T minus 50" ) );
            menuContingencyPlans.getItems().add(new CheckMenuItem( "Turn off the coffee machine " ) );
            menuContingencyPlans.getItems().add(new CheckMenuItem( "Run for your lives! " ) );
            
            final Menu menuSchedMacro = new Menu( "SchedulevMacro" );
            final CheckMenuItem checkerRealTime     = new CheckMenuItem( "Real Time" );
            final CheckMenuItem checkerSystemEvent  =  new CheckMenuItem( "System Event" );
            final CheckMenuItem checkerPeriodic     =  new CheckMenuItem( "Periodic" );
            menuSchedMacro.getItems().addAll( checkerRealTime, checkerSystemEvent, checkerPeriodic );
    
            menuSchedlues.getItems().addAll( menuAlarm, menuContingencyPlans, menuSchedMacro );

            final Menu menuTests            = new Menu( "Tests" );
            final MenuItem menuItemTestPropertySheet    = new MenuItem( "Property Sheet" );
            final MenuItem menuItemTestModelingSheet    = new MenuItem( "Modeling Sheet" );
            final MenuItem menuItemTestSpreadSheet      = new MenuItem( "Spreadsheet" );
            final MenuItem menuItemTestHTMLeditor       = new MenuItem( "HTML Editor" );
            final MenuItem menuItemTestModelingTree     = new MenuItem( "ModelingTree" );
            final Menu     menuTestGuiDesiggner     = new Menu( "GUI Designer" );
            final MenuItem     menuItemListGui      = new MenuItem( "List View" );
            final MenuItem     menuItemTreeGui      = new MenuItem( "Tree View" );
            final MenuItem     menuItemTableGui     = new MenuItem( "Table View" );
            final MenuItem     menuItemGroupGui     = new MenuItem( "Group View" );
            menuTestGuiDesiggner.getItems().addAll( menuItemListGui, menuItemTreeGui, menuItemTableGui, menuItemGroupGui );
            
            menuItemTestPropertySheet.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                    TestRecords.initRandomProperties();
                    final ScrollPane propertyScroller   = new ScrollPane(  );
                    propertyScroller.setContent( new PropertyList( TestRecords.randomProperties, null ) );
                    //propertyScroller.setContent( new PropertyList( null, TestRecords.electricalProperties ) );
                    setCenter( propertyScroller );
                }
            } ); 
            menuItemListGui.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
            menuItemTreeGui.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        final TreeDescriptor< Node, Object > treeDescriptor = new TreeDescriptor( "Tree Layout View" );
                        treeDescriptor.constructPath( "Tree Layout View:Tools:Programming:Programming Tools".split( ":" ) );
                        treeDescriptor.constructPath( "Tree Layout View:Tools:DataSources:Selection".split( ":" ) );
                        treeDescriptor.constructPath( "Tree Layout View:Tools:DataSources:Views".split( ":" ) );
                        final TreeViewDesigner treeViewDesigner = new TreeViewDesigner( treeDescriptor );
                        setCenter( treeViewDesigner );
                    } catch (TreeDescriptorException ex) {
                        Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } ); 
            menuItemTableGui.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
            menuItemGroupGui.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
            
            
            menuTests.getItems().addAll( menuItemTestPropertySheet, menuTestGuiDesiggner, menuItemTestModelingSheet,
                    menuItemTestSpreadSheet, menuItemTestHTMLeditor,menuItemTestModelingTree );
            
            final Menu menuModels                   = new Menu( "Models" );
            final Menu menuSelectCurrent            = new Menu( "Select Models" );
            final MenuItem menuItemTreeAnalysis     = new MenuItem( "Tree Analysis using: " );
            final CheckMenuItem checkerModelOne     = new CheckMenuItem( "OhmsWheel" );
            final CheckMenuItem checkerModelTwo     =  new CheckMenuItem( "Ampacities" );
            final CheckMenuItem checkerModelThree   =  new CheckMenuItem( "User Defined" );
            final CheckMenuItem checkerModelFour    =  new CheckMenuItem( "User Defined" );
            final CheckMenuItem checkerModelFive    =  new CheckMenuItem( "User Defined" );
            final CheckMenuItem checkerModelSix     =  new CheckMenuItem( "User Defined" );
            menuSelectCurrent.getItems().addAll( checkerModelOne, checkerModelTwo, checkerModelThree,
                    checkerModelFour, checkerModelFive, checkerModelSix );

            menuItemTreeAnalysis.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                    //JOptionPane.showMessageDialog( null, "Not Implemented Yet" );
                    ArrayList< ObservableProperty > propertyList    = new ArrayList< >();
                    propertyList.add( new ObservableProperty( "String Property", ControlType.TEXT_FIELD, SimplePropertyType.STRING, null ) );
                    
                    final AllowedValuesList< String > stateList   = new AllowedValuesList<  > ();
                    stateList.addAll( StateAbbr.list() );
                    final ListDescriptor stateListDescriptor    = new ListDescriptor< String >( stateList, ControlType.LIST );
                    stateListDescriptor.setCurrentValue( StateAbbr.CT.toString() );
                    propertyList.add( new ObservableProperty( "List Property", ControlType.CHOICE_BOX, SimplePropertyType.STRING, stateListDescriptor  ) );
                    
                    setCenter( new TreePane( "Current Primary Model Selection", propertyList ) );
                }
            } ); 
            
            menuModels.getItems().addAll( menuSelectCurrent, menuItemTreeAnalysis );
            
            final Menu menuLibrary = new Menu( "Library" );
            final MenuItem menuItemOrganize         = new MenuItem( "Organize" );
            final MenuItem menuItemLocalReferences  = new MenuItem( "Local References" );
            final MenuItem menuItemWebReferences    = new MenuItem( "Web References" );
            menuItemLocalReferences.setOnAction( new EventHandler< ActionEvent > () {
                @Override
                public void handle(ActionEvent event) {
                    final FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle( "Reference Selection" );
                    fileChooser.getExtensionFilters().addAll( new ExtensionFilter( "pdf", "*.pdf" ),
                                                        new ExtensionFilter( "html", "*.html" ),
                                                        new ExtensionFilter( "htm", "*.htm" ),
                                                        new ExtensionFilter( "txt", "*.txt" ) );
                    chosenFile   = fileChooser.showOpenDialog( null );
                }
            } ); 
            menuItemWebReferences.setOnAction( new EventHandler< ActionEvent > () {
                @Override
                public void handle(ActionEvent event) {
                    if ( chosenFile != null )   {
                        try {
                            ActivityManager.this.setCenter( new Browser( chosenFile.getCanonicalPath() ) );
                        } catch (Exception ex) {
                            Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else
                        ActivityManager.this.setCenter( new BorderedBrowser(  ) );
                }
            } ); 
            menuLibrary.getItems().addAll( menuItemOrganize, menuItemLocalReferences, menuItemWebReferences );

            getMenus().addAll( menuFile, menuEdit, menuMacros, menuStyles, menuSchedlues, menuTests, menuModels, menuLibrary );   

            prefWidthProperty().bind( this.widthProperty() );
       }
   }
    
    private static void initializeModelingTables()  {
        ohmsWheelTable.setEditable( true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_VOLTAGE, true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_CURRENT, true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_RESISTANCE, true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_POWER, true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_TIME, true );
        ohmsWheelTable.setColumnEditable( OhmsWheelRecord.VIEW_FIELD_NAME_ENERGY, true );

        fractionTable.setColumnEditable( FractionRecord.VIEW_FILD_NAME_FRACTION, true );
        fractionTable.setColumnEditable( FractionRecord.VIEW_FILD_NAME_DECIMAL, true );
        fractionTable.setColumnEditable( FractionRecord.VIEW_FILD_NAME_PROPER, true );
        fractionTable.setColumnEditable( FractionRecord.VIEW_FILD_NAME_REDUCED, true );
        fractionTable.setColumnEditable( FractionRecord.VIEW_FILD_NAME_TERMINOLOGY, true );
        
        modelingTables.add( fractionTable );
        modelingTables.add( ohmsWheelTable );
    }
    
    private class MessageManager implements PropertyChangeListener   {
        @Override
        public void propertyChange(PropertyChangeEvent event) {
            final Event originalEvent = ( Event )event.getOldValue();
            switch( ( String ) event.getNewValue() )    {
                case MESSAGE_ACTION:
                    if ( originalEvent.getSource().equals( activityPaneFraction.getButtonGenerate() ) )    {
                        final int high  = activityPaneFraction.getListViewNumerator().getItems().size() - 1;
                        Double numerator    = Math.random() * high;
                        Double denominator  = Math.random() * high;
                        activityPaneFraction.getListViewNumerator().getSelectionModel().clearAndSelect( numerator.intValue() ); 
                        activityPaneFraction.getListViewDenominator().getSelectionModel().clearAndSelect( denominator.intValue() ); 
                        activityPaneFraction.getComboBoxNumerator().getSelectionModel().clearAndSelect( numerator.intValue() ); 
                        activityPaneFraction.getComboBoxDenominator().getSelectionModel().clearAndSelect( denominator.intValue() ); 
                        activityPaneFraction.handleListSelectEvent( originalEvent );
                    }

                    else if ( InteractiveComboBox.class.isInstance( originalEvent.getSource() ) )  {
                        if ( ( ( InteractiveComboBox ) originalEvent.getSource() ).isVisible() )
                            activityPaneFraction.handleListSelectEvent( originalEvent );
                    }

                    else if ( InteractiveToggleButton.class.isInstance( originalEvent.getSource() ) )
                        if ( originalEvent.getSource().equals( activityPaneFraction.getToggleButtonHideDecimal() ) )    {
                            activityPaneFraction.getTextFieldDecimalEq().setVisible( !activityPaneFraction.getToggleButtonHideDecimal().isSelected() );
                        }
                        else if ( originalEvent.getSource().equals( activityPaneFraction.getToggleButtonHideLanguage() ) ) {
                            activityPaneFraction.getListViewLanguage().setVisible( !activityPaneFraction.getToggleButtonHideLanguage().isSelected() );
                        }
                        else if ( originalEvent.getSource().equals( activityPaneFraction.getToggleButtonHideReduced() ) ) {
                            activityPaneFraction.getTextFieldReduced().setVisible( !activityPaneFraction.getToggleButtonHideReduced().isSelected() );
                        }
                        else if ( originalEvent.getSource().equals( activityPaneFraction.getToggleButtonHideMixed() ) ) {
                            activityPaneFraction.getTextFieldMixed().setVisible( !activityPaneFraction.getToggleButtonHideMixed().isSelected() );
                        }
                    break;
                case MESSAGE_KEY_PRESSED:
                    if ( InteractiveComboBox.class.isInstance( originalEvent.getSource() ) ||
                           InteractiveListView.class.isInstance( originalEvent.getSource() ) )  {
                        activityPaneFraction.handleListSelectEvent( originalEvent );
                    }
                    break;
                case MESSAGE_KEY_TYPED:
                    if ( InteractiveComboBox.class.isInstance( originalEvent.getSource() )  ||
                           InteractiveListView.class.isInstance( originalEvent.getSource() ) )  {
                        activityPaneFraction.handleListSelectEvent( originalEvent );
                    }
                    break;
                case MESSAGE_MOUSE_CLICKED:
                    if ( InteractiveListView.class.isInstance( originalEvent.getSource() ) )  {
                        if ( ( ( InteractiveListView ) originalEvent.getSource() ).isVisible() )
                            activityPaneFraction.handleListSelectEvent( originalEvent );
                    }
                    break;
                    case MESSAGE_MOUSE_PRESSED:
                        break;
                    case MESSAGE_MOUSE_RELEASED:
                        break;
                    case MESSAGE_MOUSE_DRAGGED:
                        break;
                case MESSAGE_LIST_ADD:
                    if ( activityPaneFraction.getListViewNumerator().equals( originalEvent.getSource() ) ||
                           activityPaneFraction.getListViewDenominator().equals( originalEvent.getSource() ) ||
                            activityPaneFraction.getComboBoxNumerator().equals( originalEvent.getSource() ) ||
                            activityPaneFraction.getComboBoxDenominator().equals( originalEvent.getSource() ) ||
                            activityPaneFraction.getButtonGenerate().equals( originalEvent.getSource() ) )   {
                         listSequence.add( activityPaneFraction.getFraction().toString() );
                         listViewSequence.getSelectionModel().clearAndSelect( listSequence.size()-1 );
                    }
                    break;
                case SimpleMathActivity.EVENT_MOUSE_CLICK:
                    break;
                case SimpleMathActivity.EVENT_MOUSE_PRESS:
                    break;
                case SimpleMathActivity.EVENT_MOUSE_RELEASED:
                    break;
                case SimpleMathActivity.EVENT_MOUSE_DRAG:
                    break;
                case SimpleMathActivity.EVENT_BUTTON_INCLUDE:
                    switch( currentMathActivity )   {
                        case NULL:
                            break;
                        case ADDITION:
                            listSequence.add( additionActivity.getResult() );
                            break;
                        case SUBTRACTION:
                            listSequence.add( subtractionActivity.getResult() );
                            break;
                        case MULTIPLICATION:
                            listSequence.add( multiplicationActivity.getResult() );
                            break;
                        case DIVISION:
                            listSequence.add( divisionActivity.getResult() );
                            break;
                    }
                    break;
                    
            }
        }
    }
    
    
    class DragAction    {
        private final Point2D startLoc;
        private final Node source;
        private final Object content;

        public DragAction( final Point2D start, final Node source, final Object content )   {
            this.startLoc      = start;
            this.source     = source;
            this.content    = content;
        }
        
        public Point2D getStart() {
            return startLoc;
        }

        public Node getSource() {
            return source;
        }

        public Object getContent() {
            return content;
        }
        
    }
    
}
