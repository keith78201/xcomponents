/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ProgrammableTreeView
 * Author           George Keith Watson
 * Date Started:    July 28, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.TreeMap;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import model.filter.TreeDescriptor;
import view.nodes.ProgrammableTreeView;

/**
 *
 * @author George Keith Watson
 */
public class BorderedTreeView extends BorderPane {
    
    private final ProgrammableTreeView programmableTreeView = new ProgrammableTreeView( "Control Tree" );
    private final Label labelMessages   = new Label( "Messages" );
    
    public BorderedTreeView( final TreeDescriptor treeDescriptor )   {
        //labelMessages.setMinHeight( 30 );
        //labelMessages.setMaxHeight( 30 );
        if ( treeDescriptor != null )   {
            labelMessages.setText( "messages" );
            setBottom( labelMessages );
            setCenter( treeDescriptor.constructView() );
        }
        else    {
            programmableTreeView.registerListenerDepricated( new PropertyChangeListener() {
                @Override
                public void propertyChange( PropertyChangeEvent evt ) {
                    switch( evt.getPropertyName() ) {
                        case ProgrammableTreeView.MESSAGE_TYPE_MOUSE_ENTER:
                            switch( ( ( TreeMap< String, String > )evt.getOldValue() ).get( ProgrammableTreeView.NODE_NAME_KEY ) ) {
                                case ProgrammableTreeView.NODE_NAME_FIRST_NAME:
                                    labelMessages.setText( "Field = First Name" );
                                    break;
                                case ProgrammableTreeView.NODE_NAME_MIDDLE_NAME:
                                    labelMessages.setText( "Field = Middle Name" );
                                    break;
                                case ProgrammableTreeView.NODE_NAME_LAST_NAME:
                                    labelMessages.setText( "Field = Last Name" );
                                    break;
                            }
                        case ProgrammableTreeView.MESSAGE_TYPE_MOUSE_EXITED:
                            //labelMessages.setText( "" );
                    }

                }
            } ); 
            setCenter( programmableTreeView );
            setBottom( labelMessages );
        }
    }
}
