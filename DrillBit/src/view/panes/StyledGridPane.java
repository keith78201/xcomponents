/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          GridLocation
 * Author           George Keith Watson
 * Date Started:    July 12, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import view.GridLocation;

/**
 *
 * @author George Keith Watson
 */
public class StyledGridPane extends GridPane {
    
    public final HashMap< String, GridLocation > componentLocationMap   = new HashMap<> ();
    
    public void add( final Node node, final GridLocation gridLocation ) {
        add( node, gridLocation.getCol(), gridLocation.getRow(), gridLocation.getColSpan(), gridLocation.getRowSpan() );
        componentLocationMap.put( node.getId(), gridLocation );
    }
    
    public final GridLocation getComponentLocation( final String id ) {
        return componentLocationMap.get( id );
    }
}
