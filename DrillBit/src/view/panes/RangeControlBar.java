/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          RangeControlBar
 * Author           George Keith Watson
 * Date Started:    July 26, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import static javafx.scene.layout.GridPane.setMargin;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import model.Range;

/**
 *
 * @author George Keith Watson
 */
public class RangeControlBar extends GridPane {
    
    public static final String ACTION_NAME_INCREMENT  = "increment";
    public static final String ACTION_NAME_DECREMENT  = "decrement";
    
    private       Range range;
    private final Text textLow      = new Text();
    private final Text textHigh     = new Text();
    private final Text textValue    = new Text();
    private final Button buttonIncrement    = new Button( "Incr" );
    private final Button buttonDecrement    = new Button( "Decr" );
    private       double increment  = 1.0;
    private       double currentValue;
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );

    public RangeControlBar( final double width, final double height, final double low, final double high, final Double increment )    {
        setMinWidth( width );
        setMinHeight( height );
        buttonIncrement.setMaxHeight( height );
        buttonDecrement.setMaxHeight( height );
        try {
            range = new Range( low, high );
        } catch (Exception ex) {
            Logger.getLogger(RangeControlBar.class.getName()).log(Level.SEVERE, null, ex);
        }
        buttonIncrement.setFont(Font.font( Font.getFamilies().get(0), FontWeight.THIN, FontPosture.REGULAR, 10 ) );
        buttonDecrement.setFont(Font.font( Font.getFamilies().get(0), FontWeight.THIN, FontPosture.REGULAR, 10 ) );
        textLow.setFont(Font.font( Font.getFamilies().get(0), FontWeight.THIN, FontPosture.REGULAR, 10 ) );
        textHigh.setFont(Font.font( Font.getFamilies().get(0), FontWeight.THIN, FontPosture.REGULAR, 10 ) );
        textValue.setFont(Font.font( Font.getFamilies().get(0), FontWeight.THIN, FontPosture.REGULAR, 10 ) );
        
        buttonDecrement.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                final double currentValue = Double.parseDouble( textValue.getText() );
                if ( currentValue - increment >= range.getLow() )   {
                    final double newValue = Math.round( ( currentValue - increment ) * 100 ) / 100;
                    textValue.setText( Double.toString( newValue ) );
                    propertyChangeSupport.firePropertyChange( ACTION_NAME_DECREMENT, currentValue, newValue );
                }
            }
        } );
        buttonIncrement.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                final double currentValue = Double.parseDouble( textValue.getText() );
                if ( currentValue + increment <= range.getHigh() )   {
                    final double newValue = Math.round( ( currentValue + increment ) * 100 ) / 100;
                    textValue.setText( Double.toString( newValue ) );
                    propertyChangeSupport.firePropertyChange( ACTION_NAME_INCREMENT, currentValue, newValue );
                }
            }
        } );
        buttonDecrement.setMinWidth(75 );
        buttonIncrement.setMinWidth(75 );
        setMargin( textLow, new Insets( 5, 5, 5, 5 ));
        setMargin( buttonDecrement, new Insets( 5, 5, 5, 5 ));
        setMargin( textValue, new Insets( 5, 5, 5, 5 ));
        setMargin( buttonIncrement, new Insets( 5, 5, 5, 5 ));
        setMargin( textHigh, new Insets( 5, 5, 5, 5 ));
        add( textLow, 0, 0, 1, 1 );
        add( buttonDecrement, 1, 0, 1, 1 );
        add( textValue, 2, 0, 1, 1 );
        add( buttonIncrement, 3, 0, 1, 1 );
        add( textHigh, 4, 0, 1, 1 );
        textLow.setText( Double.toString( range.getLow() ) );
        textHigh.setText( Double.toString( range.getHigh() ) ); 
        textValue.setText( Double.toString( range.getLow() + ( currentValue * range.getRange() ) ) );
   }
    
    
    public Range getRange() {
        return range;
    }

    public void setRange(Range newRange) {
        this.range = newRange;
        buttonIncrement.setVisible( true );
        buttonDecrement.setVisible( true );
        textLow.setVisible( true );
        textHigh.setVisible( true );
        textValue.setVisible( true );

        newRange.setLow( newRange.getLow() );
        newRange.setHigh( newRange.getHigh() );
        textLow.setText( newRange.getLow().toString() );
        textHigh.setText( newRange.getHigh().toString() );
        Double currentValue = Double.valueOf( textValue.getText() );
    }
    
    
    public double getIncrement() {
        return increment;
    }

    public void setIncrement(double increment) {
        this.increment = increment;
    }

    public double getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue( double currentValue ) {
        this.currentValue = currentValue;
        textValue.setText( Double.toString( currentValue ) );
    }
    
    
    public void addActionListener( PropertyChangeListener listener )   {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
}
