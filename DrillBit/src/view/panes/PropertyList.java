/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          PropertyList
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import control.XModel;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import model.ObservableProperty;
import model.Range;
import model.filter.EnumDescriptor;
import model.filter.ListDescriptor;
import model.filter.MapDescriptor;
import model.filter.StringDescriptor;
import model.filter.TreeDescriptor;
import model.filter.ValueFilter;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveCheckBox;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveProgressBar;
import view.nodes.InteractiveRadioButton;
import view.nodes.InteractiveScrollBar;
import view.nodes.InteractiveSlider;
import view.nodes.InteractiveToggleButton;

/**
 *
 * @author George Keith Watson
 */
public class PropertyList extends StyledGridPane {
    
    private final static int CONTROL_WIDTH  = 200;
    private final static int CONTROL_HEIGHT = 25;

    
    private final InteractiveLabel labelPropertyHeader  = new InteractiveLabel( "Property" );
    private final InteractiveLabel labelPropertyValue   = new InteractiveLabel( "Value" );
    
    private final XModel xModel;
    private final ArrayList < ObservableProperty > propertyList;
    private final TreeMap< String, Node > controlMap    = new TreeMap<  >();
    
    public PropertyList( final ArrayList < ObservableProperty > propertyListArg, final XModel xModel ) {
        
        if ( ( this.xModel = xModel ) != null )
            this.propertyList   = xModel.getProperties();
        else
            this.propertyList   = propertyListArg;
        Integer row = 0;
        //setBlendMode( BlendMode.ADD );
        setHgap( 20 );
        setVgap( 5 );
        //setLayoutX( 50 );
        setPadding( new Insets( 50, 10, 50, 10 ));

        add( labelPropertyHeader, 0, row );
        add( labelPropertyValue, 1, row );
        //setGridLinesVisible( true );
        
        row++;
        for ( ObservableProperty observableProperty : propertyList )    {
            final Label textPropertyName = new Label( observableProperty.getName() );
            textPropertyName.setMinHeight( CONTROL_HEIGHT );
            textPropertyName.setMinWidth( CONTROL_WIDTH );
            setAlignment(Pos.CENTER);
            switch( observableProperty.getControlType() )   {
                case ACCORDION:
                    final Accordion accordian   = new Accordion();
                    final TreeMap< String, Node > accordianControlMap  = 
                            observableProperty.getValueFilter().getFilterType() == ValueFilter.TYPE_MAP ?
                           ( ( MapDescriptor )observableProperty.getValueFilter() ).getMap() : null; 
                    if ( accordianControlMap != null )   {
                        final TreeMap< String, Object > valueMap  = ( ( MapDescriptor )observableProperty.getValueFilter() ).getValueMap( );
                        Iterator< String > iterator = accordianControlMap.keySet().iterator();
                        while ( iterator.hasNext() )    {
                            final String propertyName   = iterator.next();
                            final Node control  = accordianControlMap.get( propertyName );
                            if ( valueMap.get( propertyName ) != null ) {
                                if ( Button.class.isInstance( control ) )   {
                                    ( ( Button ) control ).setText( valueMap.get( propertyName ).toString() );
                                    ( ( Button ) control ).setOnAction( new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            //Nothing to set dirrectly - user can attach their own action listeners at
                                            //programming end.
                                        }
                                    } ); 
                                }
                                else if ( TextField.class.isInstance( control ) )   {
                                    ( ( TextField ) control ).setText( valueMap.get( propertyName ).toString() );
                                    ( ( TextField ) control ).setOnAction(  new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            valueMap.put( propertyName, ( ( TextField ) control ).getText() );
                                        }
                                    } ); 
                                }
                                else if ( CheckBox.class.isInstance( control ) )   {
                                    ( ( CheckBox ) control ).setSelected( ( Boolean ) valueMap.get( propertyName ) );
                                    ( ( CheckBox ) control ).setOnAction(  new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            valueMap.put( propertyName, ( ( CheckBox ) control ).isSelected() );
                                        }
                                    } ); 
                                }
                                else if ( RadioButton.class.isInstance( control ) )   {
                                    ( ( RadioButton ) control ).setSelected( ( Boolean ) valueMap.get( propertyName ) );
                                    ( ( RadioButton ) control ).setOnAction(  new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            valueMap.put( propertyName, ( ( RadioButton ) control ).isSelected() );
                                        }
                                    } ); 
                                }
                                else if ( ToggleButton.class.isInstance( control ) )   {
                                    ( ( ToggleButton ) control ).setSelected( ( Boolean ) valueMap.get( propertyName ) );
                                    ( ( ToggleButton ) control ).setOnAction(  new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            valueMap.put( propertyName, ( ( ToggleButton ) control ).isSelected() );
                                        }
                                    } ); 
                                }
                                else if ( ChoiceBox.class.isInstance( control ) )   {
                                    ( ( ChoiceBox ) control ).getSelectionModel().select( valueMap.get( propertyName ).toString() );
                                    ( ( ChoiceBox ) control ).valueProperty().addListener( new ChangeListener() {
                                        @Override
                                        public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                                            valueMap.put( propertyName, ( ( ChoiceBox ) control ).valueProperty().getValue() );
                                        }
                                    } ); 
                                }
                                else if ( ListView.class.isInstance( control ) )   {
                                    ( ( ListView ) control ).getSelectionModel().select( valueMap.get( propertyName ).toString() );
                                    ( ( ListView ) control ).getSelectionModel().selectionModeProperty().addListener( new ChangeListener() {
                                        @Override
                                        public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                                            valueMap.put( propertyName, ( ( ListView ) control ).getSelectionModel().getSelectedItem() );
                                        }
                                    } );
                                    ( ( ListView ) control ).getSelectionModel().selectionModeProperty().addListener( new ChangeListener() {
                                        @Override
                                        public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                                            valueMap.put( propertyName, ( ( ListView ) control ).getSelectionModel().getSelectedItem() );
                                        }
                                    } );
                                }
                                else if ( ComboBox.class.isInstance( control ) )   {
                                    ( ( ComboBox ) control ).getSelectionModel().select( valueMap.get( propertyName ).toString() );
                                    ( ( ComboBox ) control ).valueProperty().addListener( new ChangeListener() {
                                        @Override
                                        public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                                            valueMap.put( propertyName, ( ( ComboBox ) control ).valueProperty().getValue() );
                                        }
                                    } ); 
                                }
                                else if ( PasswordField.class.isInstance( control ) )   {
                                    ( ( PasswordField ) control ).setText( valueMap.get( propertyName ).toString() );
                                    ( ( PasswordField ) control ).setOnAction( new EventHandler< ActionEvent >() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            valueMap.put( propertyName, ( ( TextField ) control ).getText() );
                                        }
                                    } ); 
                                }
                                else if ( ColorPicker.class.isInstance( control ) )   {
                                    ( ( ColorPicker ) control ).setValue( ( Color ) valueMap.get( propertyName ) );
                                    ( ( ColorPicker ) control ).valueProperty().addListener( new ChangeListener< Color >() {
                                        @Override
                                        public void changed(ObservableValue<? extends Color> arg0, Color arg1, Color arg2) {
                                            valueMap.put( propertyName, ( ( ColorPicker ) control ).valueProperty().get() );
                                        }
                                    } ); 
                                }
                                else if ( TreeView.class.isInstance( control ) )   {
                                    //No setting of default node selection coded yet.
                                    ( ( TreeView ) control ).setOnMouseClicked( new EventHandler< MouseEvent >() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            System.out.println("Accordian Tree Selection: " + ( ( TreeView ) control ).getSelectionModel().getSelectedItem().toString() );
                                            valueMap.put( propertyName, ( ( TreeView ) control ).getSelectionModel().getSelectedItem() );
                                        }
                                    }); 
                                }
                                else if ( Slider.class.isInstance( control ) )   {
                                    ( ( Slider ) control ).setValue( ( Double )valueMap.get( propertyName ) );
                                    ( ( Slider ) control ).valueProperty().addListener( new ChangeListener< Number >() {
                                        @Override
                                        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                                            valueMap.put( propertyName, ( ( Slider ) control ).valueProperty().get() );
                                        }
                                    } );

                                }
                                
                            }
                            accordian.getPanes().add( new TitledPane( propertyName, control ) ); 
                        }
                        accordian.setPrefWidth( CONTROL_WIDTH );
                        accordian.setPrefHeight( CONTROL_HEIGHT * ( accordianControlMap.size() + 0.2 ) );
                        add( accordian, 1, row, 1, 1 );
                        controlMap.put( observableProperty.getName(), accordian );
                    }  

                    break;
                case BUTTON:
                    final InteractiveButton propertyButton  = new InteractiveButton( observableProperty.getPropertyView().toString() );
                    final int thisRow = row;
                    propertyButton.setPrefWidth( CONTROL_WIDTH );
                    propertyButton.setPrefHeight( CONTROL_HEIGHT );
                    
                    ObservableList< String > list = null;
                    switch ( observableProperty.getValueFilter().getFilterType() )    {
                        case ValueFilter.TYPE_ENUM:
                            list   = ( ( EnumDescriptor )observableProperty.getValueFilter() ).getList();
                            break;
                        case ValueFilter.TYPE_LIST:
                            list   = ( ( ListDescriptor )observableProperty.getValueFilter() ).getValueList().getObservableList();
                            break;
                        default:
                            list   = null;
                    }
                    if ( list != null && list.size() > 0 )
                        propertyButton.setText( list.get( 0 ).toString() );

                    final ObservableProperty observablePropertyButton = observableProperty;

                    switch( ( ( EnumDescriptor )observablePropertyButton.getValueFilter() ).getControlType() )  {
                        case CHOICE_BOX:
                            final ChoiceBox choiceBox;
                            switch ( observablePropertyButton.getValueFilter().getFilterType() )    {
                                case ValueFilter.TYPE_ENUM:
                                    choiceBox   = new ChoiceBox( ( ( EnumDescriptor )observablePropertyButton.
                                            getValueFilter() ).getList() );
                                    break;
                                case ValueFilter.TYPE_LIST:
                                    choiceBox   = new ChoiceBox( ( ( ListDescriptor )observablePropertyButton.
                                            getValueFilter() ).getValueList().getObservableList() );
                                    break;
                                default:
                                    choiceBox   = null;
                            }
                            if ( choiceBox.getItems().size() > 0 )  {
                                choiceBox.getSelectionModel().select( 0 );
                            }

                            propertyButton.setOnAction( new EventHandler< ActionEvent >() {
                                @Override
                                public void handle(ActionEvent event) {
                                    if ( choiceBox != null ) {
                                        choiceBox.setPrefWidth( CONTROL_WIDTH );
                                        choiceBox.setPrefHeight( CONTROL_HEIGHT );
                                        choiceBox.valueProperty().addListener( new ChangeListener() {
                                            @Override
                                            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                                                propertyButton.setText( choiceBox.getSelectionModel().getSelectedItem().toString() );
                                                observablePropertyButton.getValueFilter().setCurrentValue( propertyButton.getText() ); 
                                                getChildren().remove( choiceBox );
                                                add( propertyButton, 1, thisRow );
                                            }
                                        } ); 
                                        getChildren().remove( propertyButton );
                                        add( choiceBox, 1, thisRow );
                                    }
                                }
                            } );
                            break;
                        case LIST:
                            final ListView listView;
                            switch ( observablePropertyButton.getValueFilter().getFilterType() )    {
                                case ValueFilter.TYPE_ENUM:
                                    listView   = new ListView( ( ( EnumDescriptor )observablePropertyButton.
                                            getValueFilter() ).getList() );
                                    break;
                                case ValueFilter.TYPE_LIST:
                                    listView   = new ListView( ( ( ListDescriptor )observablePropertyButton.
                                            getValueFilter() ).getValueList().getObservableList() );
                                    break;
                                default:
                                    listView   = null;
                            }
                            if ( listView != null && listView.getItems().size() > 0 )  {
                                listView.getSelectionModel().select( 0 );
                            }
                            propertyButton.setOnAction( new EventHandler< ActionEvent >() {
                                @Override
                                public void handle(ActionEvent event) {
                                    if ( listView != null ) {
                                        listView.setPrefWidth( CONTROL_WIDTH );
                                        listView.setPrefHeight( CONTROL_HEIGHT * 2 );
                                        textPropertyName.setMinHeight( CONTROL_HEIGHT * 2 );
                                        listView.setOnMouseClicked( new EventHandler< MouseEvent > () {
                                            @Override
                                            public void handle(MouseEvent event) {
                                                if ( event.getClickCount() == 2 )   {
                                                    propertyButton.setText( listView.getSelectionModel().getSelectedItem().toString() );
                                                    observablePropertyButton.getValueFilter().setCurrentValue( propertyButton.getText() ); 
                                                    getChildren().remove( listView );
                                                    add( propertyButton, 1, thisRow );
                                                    textPropertyName.setMinHeight( CONTROL_HEIGHT );
                                                }
                                            }
                                        } );
                                        getChildren().remove( propertyButton );
                                        add( listView, 1, thisRow );
                                    }
                                }
                            } );
                            break;
                    }
                    add( propertyButton, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyButton );
                    break;
                case CHECK_BOX:
                    final InteractiveCheckBox propertyCheckBox  = new InteractiveCheckBox( observableProperty.getPropertyView().toString() );
                    propertyCheckBox.setPrefWidth( CONTROL_WIDTH );
                    propertyCheckBox.setPrefHeight( CONTROL_HEIGHT );
                    add( propertyCheckBox, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyCheckBox );
                    break;
                case CHOICE_BOX:
                    ChoiceBox propertyChoiceBox = null;
                    switch ( observableProperty.getValueFilter().getFilterType() )    {
                        case ValueFilter.TYPE_ENUM:
                            propertyChoiceBox   = new ChoiceBox( ( ( EnumDescriptor )observableProperty.
                                    getValueFilter() ).getList() );
                            break;
                        case ValueFilter.TYPE_LIST:
                            propertyChoiceBox   = new ChoiceBox( ( ( ListDescriptor )observableProperty.
                                    getValueFilter()).getValueList().getObservableList() );
                            break;
                        default:
                            propertyChoiceBox   = null;
                    }
                    if ( propertyChoiceBox != null )    {
                        if ( propertyChoiceBox.getItems().size() > 0 )
                            propertyChoiceBox.getSelectionModel().select( 0 );
                        propertyChoiceBox.setPrefWidth( CONTROL_WIDTH );
                        propertyChoiceBox.setPrefHeight( CONTROL_HEIGHT );
                        add( propertyChoiceBox, 1, row, 1, 1 );
                    }
                    controlMap.put( observableProperty.getName(), propertyChoiceBox );
                    break;
                case COLOR_PICKER:
                    final ColorPicker propertyColorPicker   = new ColorPicker();
                    propertyColorPicker.setPrefWidth( CONTROL_WIDTH );
                    propertyColorPicker.setPrefHeight( CONTROL_HEIGHT );
                    add( propertyColorPicker, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyColorPicker );
                    break;
                case COMBO_BOX:
                    ComboBox propertyComboBox = null;
                    switch ( observableProperty.getValueFilter().getFilterType() )    {
                        case ValueFilter.TYPE_ENUM:
                            propertyComboBox   = new ComboBox( ( ( EnumDescriptor )observableProperty.
                                    getValueFilter() ).getList() );
                            break;
                        case ValueFilter.TYPE_LIST:
                            propertyComboBox   = new ComboBox( ( ( ListDescriptor )observableProperty.
                                    getValueFilter()).getValueList().getObservableList() );
                            break;
                        default:
                            propertyComboBox   = null;
                    }
                    if ( propertyComboBox != null )    {
                        if ( propertyComboBox.getItems().size() > 0 )
                            propertyComboBox.getSelectionModel().select( 0 );
                        propertyComboBox.setPrefWidth( CONTROL_WIDTH );
                        propertyComboBox.setPrefHeight( CONTROL_HEIGHT );
                        add( propertyComboBox, 1, row, 1, 1 );
                    }
                    controlMap.put( observableProperty.getName(), propertyComboBox );
                    break;
                case FILE_CHOOSER:
                    final Button buttonSelectFile = new Button( "Select File" );
                    buttonSelectFile.setPrefWidth( CONTROL_WIDTH );
                    buttonSelectFile.setPrefHeight( CONTROL_HEIGHT );
                    buttonSelectFile.setOnAction( new EventHandler< ActionEvent >() {
                        @Override
                        public void handle(ActionEvent event) {
                            final FileChooser fileChooser = new FileChooser();
                            final File chosenFile   = fileChooser.showOpenDialog( null );
                            if ( chosenFile != null )       {
                                buttonSelectFile.setText( chosenFile.getName() ); 
                            }
                        }
                    } );
                    add( buttonSelectFile, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), buttonSelectFile );
                    break;
                //This type of data entry control will pop up the JavaFX browser (WebView) with a single
                //user defined HTML form in it.  The form is filled out and submitted and the fields are
                //collected into a Property choiceBox of type Map.  The HTML form can be anything that WebView
                //can manage, which includes JavaScript, which is useful for real-time live inveractive
                //validation without the redisplay blink that submitting the entire form for validation causes.
                //JavaScript will also handle the real-time updating of dependent fields in the form as
                //the independent fields are entered.
                case HYPER_LINK:
                    Hyperlink hyperlink = new Hyperlink( observableProperty.getPropertyView().toString() );
                    hyperlink.setPrefWidth( CONTROL_WIDTH );
                    hyperlink.setPrefHeight( CONTROL_HEIGHT );
                    hyperlink.setOnAction( new EventHandler< ActionEvent >() {
                        @Override
                        public void handle(ActionEvent event) {
                            JOptionPane.showMessageDialog(null, 
                        "This type of data entry control will pop up the JavaFX browser (WebView) with a single\n" +
                        "user defined HTML form in it.  The form is filled out and submitted and the fields are\n" +
                        "collected into a Property list of type Map.  The HTML form can be anything that WebView\n" +
                        "can manage, which includes JavaScript, which is useful for real-time live inveractive\n" +
                        "validation without the redisplay blink that submitting the entire form for validation causes.\n" +
                        "JavaScript will also handle the real-time updating of dependent fields in the form as\n" +
                        "the independent fields are entered.\n", 
                            "Not Implemented Yet", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } );
                    add( hyperlink, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), hyperlink );
                    break;
                case LABEL:
                    final String valueString;
                    if ( observableProperty.getValueFilter().getCurrentValue() != null )
                           valueString  =  observableProperty.getValueFilter().getCurrentValue().toString();
                    else 
                        valueString = "";
                    final InteractiveLabel propertyLabel    = new InteractiveLabel( valueString );
                    propertyLabel.setPrefWidth( CONTROL_WIDTH );
                    propertyLabel.setPrefHeight( CONTROL_HEIGHT );
                    add( propertyLabel, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyLabel );
                    break;
                case LIST:
                    ListView listView = null;
                    switch ( observableProperty.getValueFilter().getFilterType() )    {
                        case ValueFilter.TYPE_ENUM:
                            listView   = new ListView( ( ( EnumDescriptor )observableProperty.
                                    getValueFilter() ).getList() );
                            break;
                        case ValueFilter.TYPE_LIST:
                            listView   = new ListView( ( ( ListDescriptor )observableProperty.
                                    getValueFilter()).getValueList().getObservableList() );
                            break;
                        default:
                            propertyChoiceBox   = null;
                    }
                    if ( listView != null )    {
                        if ( listView.getItems().size() > 0 )
                            listView.getSelectionModel().select( 0 );
                        listView.setPrefWidth( CONTROL_WIDTH );
                        listView.setPrefHeight( CONTROL_HEIGHT * 3 );
                        add( listView, 1, row, 1, 1 );
                    }
                    controlMap.put( observableProperty.getName(), listView );
                    break;
                case PASSWORD_FIELD:
                    final PasswordField passwordField = new PasswordField(  );
                    passwordField.setText( "password" );
                    passwordField.setPrefWidth( CONTROL_WIDTH );
                    passwordField.setPrefHeight( CONTROL_HEIGHT );
                    add( passwordField, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), passwordField );
                    break;
                case PROGRESS_BAR:
                    textPropertyName.setMinHeight( CONTROL_HEIGHT * 2 );
                    final InteractiveProgressBar progressBar = new InteractiveProgressBar( 0.50, CONTROL_WIDTH, CONTROL_HEIGHT * 2 );
                    try {
                        progressBar.setRange( new Range( 10.0, 150.0 ) );
                        progressBar.setIncrement( 10.0 ); 
                    } catch (Exception ex) {
                        Logger.getLogger(PropertyList.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    add( progressBar, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), progressBar );
                    break;
                case RADIO_BUTTON:
                    final InteractiveRadioButton propertyRadioButton  = new InteractiveRadioButton( observableProperty.getPropertyView().toString() );
                    propertyRadioButton.setPrefWidth( CONTROL_WIDTH );
                    propertyRadioButton.setPrefHeight( CONTROL_HEIGHT );
                    add( propertyRadioButton, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyRadioButton );
                    break;
                case SCROLL_BAR:
                    textPropertyName.setMinHeight( CONTROL_HEIGHT * 2 );
                    final InteractiveScrollBar scrollBar = new InteractiveScrollBar( 0.50, CONTROL_WIDTH, CONTROL_HEIGHT * 2 );
                    try {
                        scrollBar.setRange( new Range( 10.0, 150.0 ) );
                        scrollBar.setIncrement( 10.0 ); 
                    } catch (Exception ex) {
                        Logger.getLogger(PropertyList.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    add( scrollBar, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), scrollBar );
                    break;
                case SLIDER:
                    textPropertyName.setMinHeight( CONTROL_HEIGHT * 2 );
                    final InteractiveSlider slider = new InteractiveSlider( 0.50, CONTROL_WIDTH, CONTROL_HEIGHT * 2 );
                    try {
                        slider.setRange( new Range( 10.0, 150.0 ) );
                        slider.setIncrement( 10.0 ); 
                    } catch (Exception ex) {
                        Logger.getLogger(PropertyList.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    add( slider, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), slider );
                    break;
                case TEXT_AREA:
                    final TextArea textArea   = new TextArea();
                    if ( observableProperty.getValueFilter().getCurrentValue() != null )
                        textArea.setText( observableProperty.getValueFilter().getCurrentValue().toString() );
                    else
                        textArea.setText( "" );
                    textArea.setPrefWidth( CONTROL_WIDTH );
                    textArea.setPrefHeight( CONTROL_HEIGHT * 3 );
                    add( textArea, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), textArea );
                    break;
                case TEXT_FIELD:
                    final String stringValue;
                    if ( observableProperty.getValueFilter().getCurrentValue() != null )    
                        stringValue = observableProperty.getValueFilter().getCurrentValue().toString();
                    else
                        stringValue = "";                        
                    final TextField textField       = new TextField( stringValue );
                    final ValueFilter valueFilter   = observableProperty.getValueFilter();
                    final String propertyName       = observableProperty.getName();
                    textField.setOnAction( new EventHandler< ActionEvent > () {
                        @Override
                        public void handle( ActionEvent event ) {
                            if ( valueFilter.checkValue( textField.getText() ) )   {
                                xModel.updateProperty( propertyName, textField.getText() );
                                final ArrayList< ObservableProperty > changedProperties = xModel.getDependencies( propertyName );
                                for ( int pIdx = 0; pIdx < changedProperties.size(); pIdx++ )   {
                                    //TYPE OF CONTROL IS HARD CODED FOR NOW - TESTING ONLY
                                    final String updatedPropertyName = changedProperties.get( pIdx ).getName();
                                    final String newValue   = changedProperties.get( pIdx ).getValueFilter().getCurrentValue().toString();
                                    ( ( TextField )controlMap.get( updatedPropertyName ) ).setText( newValue );
                                }
                            }
                            else    {
                                System.err.println( "Invalid value entered for " + propertyName + ":\t" + textField.getText() );
                            }
                        }
                    } ); 
                    textField.setOnMousePressed( new EventHandler< MouseEvent >() {
                        @Override
                        public void handle(MouseEvent event) {
                            //if ( !textField.isFocused() )   {
                            //    textField.selectAll();
                            //}
                        }
                    } ); 
                    final ObservableProperty op = observableProperty;
                    textField.setOnKeyTyped( new EventHandler< KeyEvent >() {
                        @Override
                        public void handle(KeyEvent event) {
                        //    if ( !op.getValueFilter().checkCharacter( event.getCharacter().charAt( 0 ) ) )  {
                        //        event.consume();
                        //        textField.setText( ( String ) textField.getUserData() );
                        //    }
                        //    else
                        //        textField.setUserData( textField.getText() );
                            String autoString   = ( ( StringDescriptor ) op.getValueFilter() ).checkAutoComplete( textField.getText() );
                            
                        }
                    } );  
                    textField.setPrefWidth( CONTROL_WIDTH );
                    textField.setPrefHeight( CONTROL_HEIGHT );
                    textField.setUserData( textField.getText() );
                    add( textField, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), textField );
                    break;
                case TOGGLE_BUTTON:
                    final String stringTBValue;
                    if ( observableProperty.getValueFilter().getCurrentValue() != null )
                        stringTBValue = observableProperty.getValueFilter().getCurrentValue().toString();    
                    else
                        stringTBValue = "";
                    final InteractiveToggleButton propertyToggleButton   = new InteractiveToggleButton( stringTBValue );
                    
                    propertyToggleButton.setPrefWidth( CONTROL_WIDTH );
                    propertyToggleButton.setPrefHeight( CONTROL_HEIGHT );
                    add( propertyToggleButton, 1, row, 1, 1 );
                    controlMap.put( observableProperty.getName(), propertyToggleButton );
                    break;
                case TREE_VIEW:
                    final TreeDescriptor treeDescriptor = ( TreeDescriptor ) observableProperty.getValueFilter();
                    TreeView treeView = treeDescriptor.constructView();
                    treeView.setPrefWidth( CONTROL_WIDTH * 2 );
                    treeView.setPrefHeight( CONTROL_HEIGHT * 10 );
                    add( treeView, 1, row, 3, 1 );
                    controlMap.put( observableProperty.getName(), treeView );
                    break;
                default:
                    break;
            }
            add( textPropertyName, 0, row, 1, 1 );
            row++;
        }
    }
}
