/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          TreePane
 * Author           George Keith Watson
 * Date Started:    July 20, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Slider;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import model.ObservableProperty;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveCheckBox;
import view.nodes.InteractiveComboBox;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveRadioButton;
import view.nodes.InteractiveTextField;
import view.nodes.InteractiveToggleButton;

/**
 *
 * @author George Keith Watson
 */
public class TreePane extends BorderPane {
    
    private final InteractiveToggleButton toggleButtonPropertyEditor    = new InteractiveToggleButton( "Property editor" );
    
    private final TreeItem< String > treeItemModel  = new TreeItem(  );
    private final TreeItem< String > treeItemEdit   = new TreeItem( toggleButtonPropertyEditor );
    private final TreeItem< String > treeItemKnown  = new TreeItem( "Known values" );
    private final TreeView< String > treeViewModel  = new TreeView( treeItemModel );
    
    private final GridPane paneBottom   = new GridPane();
    private final InteractiveToggleButton buttonShowValues            = new InteractiveToggleButton( "Show Values" );
    private final InteractiveToggleButton toggleButtonEditValues    
                                        = new InteractiveToggleButton( "Edit Values" );

    private final GridPane paneTop   = new GridPane();
    private final InteractiveLabel labelDragDropInstructions    = new InteractiveLabel( "Drag values from the history sequence pane to the property to assign value to it." );
    
    private final ArrayList< TreeItem > editPropertyTreeNodes    = new ArrayList<>();
    private final ArrayList< TreeItem > knownPropertyTreeNodes   = new ArrayList<>();
    
    
    public TreePane( final String modelName, final List< ObservableProperty > propertyList )   {
        treeItemModel.setValue( modelName );
        treeItemModel.setExpanded( true );
        treeItemModel.getChildren().addAll( treeItemEdit, treeItemKnown );
        Node newNode    = null;
        for ( int propertyIndex = 0; propertyIndex < propertyList.size(); propertyIndex++ )  {
            switch( propertyList.get( propertyIndex ).getControlType() )    {
                case LABEL:
                    newNode = new InteractiveLabel();
                    break;
                case BUTTON:
                    newNode = new InteractiveButton();
                    break;
                case RADIO_BUTTON:
                    newNode = new InteractiveRadioButton();
                    break;
                case TOGGLE_BUTTON:
                    newNode = new InteractiveToggleButton();
                    break;
                case CHECK_BOX:
                    newNode = new InteractiveCheckBox();
                    break;
                case CHOICE_BOX:
                     //if ( propertyList.get( propertyIndex ).getSelections() != null )
                     //    newNode = new ChoiceBox( propertyList.get( propertyIndex ).getSelections() );
                     //else
                         newNode = new ChoiceBox( );
                    break;
                case TEXT_FIELD:
                    newNode = new InteractiveTextField();
                    break;
                case TREE_VIEW:
                    newNode = new TreeView();
                    break;
                case COMBO_BOX:
                    newNode = new InteractiveComboBox( null );
                    break;
                case SLIDER:
                    newNode = new Slider( );
                    break;
                case HYPER_LINK:
                    newNode = new Hyperlink( "" );
                    break;
                case PASSWORD_FIELD:
                    newNode = new PasswordField(  );
                    break;
                case COLOR_PICKER:
                    newNode = new ColorPicker(  );
                    break;
                case FILE_CHOOSER:
                    newNode = null;
                    break;
                
            }
            //final TextField leaf    = new TextField( "Property " + propertyIndex );
            //leaf.setPrefWidth( 200 );
            editPropertyTreeNodes.add( new TreeItem<  >( newNode ) );
            treeItemEdit.getChildren().add( editPropertyTreeNodes.get( propertyIndex ) );
            knownPropertyTreeNodes.add( new TreeItem< >( "Property " + propertyIndex ) );
            treeItemKnown.getChildren().add( knownPropertyTreeNodes.get( propertyIndex ) ); 
        }
        treeItemEdit.setExpanded( true );
        treeItemKnown.setExpanded( true );
        toggleButtonPropertyEditor.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                treeItemEdit.getChildren().get( 0 ).setGraphic( new Label("Graphic set") );
                //for ( int propertyIndex = 0; propertyIndex < 10; propertyIndex++ )  {
                //    treeItemEdit.getChildren().get( propertyIndex ).setGraphic( 
                //            toggleButtonPropertyEditor.isSelected() ? 
                //                new InteractiveTextField( "Property " + propertyIndex )  :
                //                new InteractiveLabel( "Property " + propertyIndex ) ); 
                //    }
            }
        } ); 
        
        labelDragDropInstructions.setVisible( false );
        toggleButtonEditValues.setVisible( false );
        toggleButtonEditValues.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                if ( buttonShowValues.isSelected() )
                    labelDragDropInstructions.setVisible( toggleButtonEditValues.isSelected() );
            }
        } ); 
        buttonShowValues.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                buttonShowValues.setText( buttonShowValues.isSelected() ? "Hide Values" : "Show Values" );
                toggleButtonEditValues.setVisible( buttonShowValues.isSelected() ); 
                if ( buttonShowValues.isSelected() )
                    labelDragDropInstructions.setVisible( toggleButtonEditValues.isSelected() );
                else    
                    labelDragDropInstructions.setVisible( false );
            }
        } ); 
        
        paneBottom.add( buttonShowValues, 0, 0 );
        paneBottom.add( toggleButtonEditValues, 1, 0 );
        
        paneTop.add( labelDragDropInstructions, 0, 0 );
        
        setCenter( treeViewModel );
        setBottom( paneBottom );
        setTop( paneTop );
    }
}
