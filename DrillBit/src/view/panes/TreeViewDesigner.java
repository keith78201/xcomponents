/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          TreeViewDesigner
 * Author           George Keith Watson
 * Date Started:    August 2, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      2014-08-02:
 *          With this view, the user will be able to drag controls from the left pane into a tree in the 
 *          center pane to add to branches in the tree.  The tree structure can be predefined and lacking
 *          Controls ( leaves ), or the user can add all of the branches and paths required for the layout
 *          here.  The toolbar for adding branches and paths will be in the left pane.  
 */

package view.panes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import model.filter.TreeDescriptor;
import model.filter.TreeNode;
import view.nodes.InteractiveButton;
import view.nodes.InteractiveCheckBox;
import view.nodes.InteractiveChoiceBox;
import view.nodes.InteractiveComboBox;
import view.nodes.InteractiveLabel;
import view.nodes.InteractiveListView;
import view.nodes.InteractiveProgressBar;
import view.nodes.InteractiveRadioButton;
import view.nodes.InteractiveScrollBar;
import view.nodes.InteractiveSlider;
import view.nodes.InteractiveTextField;
import view.nodes.InteractiveTitledPane;
import view.nodes.InteractiveToggleButton;
import view.nodes.NodeUtils;
import view.nodes.ProXyNode;

/**
 *
 * @author George Keith Watson
 */
public class TreeViewDesigner extends BorderPane {
    
    private final TreeItem< Node > root = new TreeItem( new Label( "Tree Layout View" ) );
    private final TreeView < Node > controlTree = new TreeView( root );
    private final TreeDescriptor< Node, Object > treeDescriptor;
    
    private final ToolBar   toolBarControls = new ToolBar(  );
    private final ScrollPane scrollPaneToolBar  = new ScrollPane( );

    private final TreeNode rootNode;
    private final TreeView treeView;
        
    private final Accordion         accordion       = new Accordion( );
    private final InteractiveTitledPane      titledPaneAccordion    
            = new InteractiveTitledPane( "Accordion", new Label( "your control here" ) );
    private final InteractiveButton          button          = new InteractiveButton( "Button" );
    private final InteractiveCheckBox        checkBox        = new InteractiveCheckBox( "CheckBox" );
    private final ObservableList    choiceBoxSelections 
            = FXCollections.observableArrayList( "ChoiceBox", "Drag n Drop then Add" );
    private final InteractiveChoiceBox       choiceBox       = new InteractiveChoiceBox( choiceBoxSelections );
    private final ColorPicker       colorPicker                = new ColorPicker();
    private final ObservableList    comboBoxSelections 
            = FXCollections.observableArrayList( "ComboBox", "Drag n Drop then Add" );
    private final InteractiveComboBox        comboBox        = new InteractiveComboBox( comboBoxSelections );
    private final Hyperlink         hyperlink                  = new Hyperlink( "Hyperlink" );
    private final InteractiveLabel           label           = new InteractiveLabel( "Label" );
    private final ObservableList    listViewSelections 
            = FXCollections.observableArrayList( "ListView", "Drag n Drop then Add" );
    private final InteractiveListView        listView        = new InteractiveListView( listViewSelections );
    private final PasswordField     passwordField               = new PasswordField();
    private final InteractiveProgressBar     progressBar     = new InteractiveProgressBar( 0, 100, 50 );
    private final ProgressIndicator progressIndicator 
            = new ProgressIndicator();
    private final InteractiveRadioButton     radioButton     = new InteractiveRadioButton( "Radio Button" );
    private final InteractiveScrollBar       scrollBar       = new InteractiveScrollBar( 0, 100, 50 );
    private final Separator         separator                = new Separator();
    private final InteractiveSlider          slider          = new InteractiveSlider( 0, 100, 50 );
    private final TabPane         tabPane                    = new TabPane();
    private final TableView       tableView                  = new TableView();
    private final TextArea        textArea                   = new TextArea( "Text Area" );
    private final InteractiveTextField       textField       = new InteractiveTextField( "Text Field" );
    private final TitledPane      titledPane                 = new TitledPane( "TitledPane", new Button( "Your Control" ) );
    private final InteractiveToggleButton    toggleButton    = new InteractiveToggleButton( "ToggleButton" );
    private final ToolBar         toolBar                    = new ToolBar();
    
    private         DragAction  currentDragAction   = null;

    public TreeViewDesigner( final TreeDescriptor< Node, Object > treeDescriptor )  {
        this.treeDescriptor = treeDescriptor;
        rootNode   = treeDescriptor.getControlTree();
        treeView = treeDescriptor.constructView();
        final TreeNode rootNode   = treeDescriptor.getControlTree();
        final TreeView treeView = treeDescriptor.constructView();
        
        accordion.getPanes().add( titledPaneAccordion );
        accordion.setExpandedPane(  titledPaneAccordion );
        choiceBox.getSelectionModel().clearAndSelect( 0 ); 
        comboBox.getSelectionModel().clearAndSelect( 0 );
        listView.getSelectionModel().clearAndSelect( 0 ); 
        passwordField.setText( "password" );
        tableView.getColumns().add( new TableColumn( "Table" ) );
        tableView.getColumns().add( new TableColumn( "Column" ) );
        toolBarControls.setOrientation( Orientation.VERTICAL );
        toolBar.getItems().addAll( new Button( "Tool" ), new Button( "Tool" ), new Button( "Tool" ) );

        accordion.setPrefWidth( 150 );
        button.setPrefWidth( 150 );
        checkBox.setPrefWidth( 150 );
        choiceBox.setPrefWidth( 150 );
        colorPicker.setPrefWidth( 150 );
        comboBox.setPrefWidth( 150 );
        hyperlink.setPrefWidth( 150 );
        label.setPrefWidth( 150 );
        listView.setPrefWidth( 150 );
        passwordField.setPrefWidth( 150 );
        progressBar.setPrefWidth( 150 );
        progressIndicator.setPrefWidth( 150 );
        progressIndicator.setPrefHeight( 25 );
        radioButton.setPrefWidth( 150 );
        scrollBar.setPrefWidth( 150 );
        separator.setPrefWidth( 150 );
        slider.setPrefWidth( 150 );
        tabPane.setPrefWidth( 150 );
        tableView.setPrefWidth( 150 );
        textArea.setPrefWidth( 150 );
        textField.setPrefWidth( 150 );
        titledPane.setPrefWidth( 150 );
        toggleButton.setPrefWidth( 150 );
        toolBar.setPrefWidth( 150 );
                
        
        listView.setPrefHeight( 50 );
        tableView.setPrefHeight( 50 );
        textArea.setPrefHeight( 50 );
        
        toolBarControls.getItems().addAll( accordion,   button,     checkBox,   choiceBox,
                                        colorPicker,    comboBox,   hyperlink,  label,
                                        listView,       passwordField,  progressBar,    progressIndicator,
                                        radioButton,    scrollBar,  separator,  slider,
                                        tabPane,        tableView,  textArea,   textField,
                                        titledPane,     toggleButton, toolBar, initializeDragHandlers( button ) );
        
        toolBarControls.setPrefWidth( 175 );
        scrollPaneToolBar.setContent( toolBarControls );
        scrollPaneToolBar.setPrefViewportWidth( 200 );
        
        //initializeDragHandlers( accordion );
        //initializeDragHandlers( button );
        //initializeDragHandlers( checkBox );
        //initializeDragHandlers( choiceBox );
        //initializeDragHandlers( colorPicker );
        //initializeDragHandlers( comboBox );
        //initializeDragHandlers( hyperlink );
        //initializeDragHandlers( label );
        //initializeDragHandlers( listView );
        //initializeDragHandlers( passwordField );
        //initializeDragHandlers( progressBar );
        //initializeDragHandlers( progressIndicator );
        //initializeDragHandlers( progressIndicator );
        //initializeDragHandlers( radioButton );
        //initializeDragHandlers( scrollBar );
        //initializeDragHandlers( separator );
        //initializeDragHandlers( slider );
        //initializeDragHandlers( tabPane );
        //initializeDragHandlers( tableView );
        //initializeDragHandlers( textArea );
        //initializeDragHandlers( textField );
        //initializeDragHandlers( titledPane );
        //initializeDragHandlers( toggleButton );
        //initializeDragHandlers( toolBar );
        
        setCenter( treeView );
        setLeft( scrollPaneToolBar );
    }
    
    /**
     * initialize and plug in the drag and drop handlers for all controls in the pallet.
     * The original control must retain as much of its designed functionality as possible, as it 
     * is possible for it to have been designed by the user rather than being a default control. 
     * The currently implemented workflow requires that it be designed only once it is in the layout. 
     * This design layout will also allow user to use controls that are already partially or 
     * completely designed, including their assigned event handlers. <br>
     * The drag action by user requires a right click to select a control. This can either select the 
     * control in place with the drag starting from the position in the toolbar, or it can place the 
     * control in some corner of the the selected layout, which in this case is a tree.<br>
     * After the right click, it makes
     * sense to use the right mouse down state to drag the control to its desired location, however it is
     * easier to hover the mouse over the movable proxy control and move the mouse to move the 
     * proxy and then drop it on the next right click.  It is also easier to program.  The hover can wait
     * until the mouse position is near the center, in a "header" area like a window, or pick it up anywhere
     * within its bounds.  This can be a configurable behavior also.  For now, in-bounds is the rule, 
     * since it is the simplest to program for a prototype or demo version.
     * @param node
     * @return 
     */
    private Node initializeDragHandlers( final Node node )   {
        //final Node dragNode = accordion.equals( node ) ? titledPaneAccordion : node;
        final Node dragNode = accordion.equals( node ) ? titledPaneAccordion.makeProxy( 0.5 ) : ( ( ProXyNode )node ).makeProxy( 0.5 );
        dragNode.setVisible( false ); 
        
        node.setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event ) { 
                if ( event.isSecondaryButtonDown() && currentDragAction == null )        {
                    NodeUtils.copyProperties( node, dragNode );
                    dragNode.setOpacity( 0.5 );
                    dragNode.setVisible( true ); 
                    node.setVisible( false );
                    currentDragAction = new DragAction( new Point2D( 
                            ( ( Double )dragNode.getLayoutX() ).intValue() + event.getX() + 5, 
                            ( ( Double )dragNode.getLayoutY() ).intValue() + event.getY() + 40 ), 
                            dragNode, dragNode.getId() );
                }
            } 
        } );
        
        dragNode.setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                if ( currentDragAction != null )        {
                    double baseX    = currentDragAction.getStart().getX();
                    double baseY    = currentDragAction.getStart().getY();
                    dragNode.setTranslateX( event.getSceneX() - baseX );
                    dragNode.setTranslateY( event.getSceneY() - baseY );
                }
            }
         } );
        dragNode.setOnMouseMoved(  new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println( "Mouse moved in or on Drag Node" );
           }
        } );
        
        dragNode.setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                currentDragAction       = null;
           }
        } );
        return dragNode;
    }
    
    class DragAction    {
        private final Point2D startLoc;
        private final Node source;
        private final Object content;

        public DragAction( final Point2D start, final Node source, final Object content )   {
            this.startLoc      = start;
            this.source     = source;
            this.content    = content;
        }
        
        public Point2D getStart() {
            return startLoc;
        }

        public Node getSource() {
            return source;
        }

        public Object getContent() {
            return content;
        }
        
    }
}
