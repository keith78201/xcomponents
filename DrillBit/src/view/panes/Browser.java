/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          Browser
 * Author           George Keith Watson
 * Date Started:    July 22, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author George Keith Watson
 */
public class Browser extends javafx.scene.layout.BorderPane {
    
    private final     WebView   browser     = new WebView();
    private final     WebEngine webEngine   = browser.getEngine();
        
    public Browser( final String url ) {
        if ( url != null )
            webEngine.load( "file://" + url );
        setCenter( browser );
    }
}
