/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.panes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import view.ActivityManager;

/**
 *
 * @author George Keith Watson
 */
public class BorderedHTMLeditor extends BorderPane {
    
    private final HTMLEditor noteEditor = new HTMLEditor();
    private final Label labelMessages   = new Label( "Messages" );
    
    public BorderedHTMLeditor() {
        labelMessages.setMinHeight( 30 );
        initializeContextMenu();
        setCenter( noteEditor );
        setBottom( labelMessages );
    }
    
    public final void setHTML( final String html ) {
        noteEditor.setHtmlText( html );
    }
    
    private void initializeContextMenu()    {
        final ObservableList< MenuItem > menuItems   = FXCollections.observableList( new ArrayList< MenuItem >() );
        final MenuItem menuItemSaveToFile     = new MenuItem( "Save to File" );
        final MenuItem menuItemFetchFile      = new MenuItem( "Fetch File" );
        final MenuItem menuItemSelectAll      = new MenuItem( "Select All" );
        final MenuItem menuItemSaveSelected   = new MenuItem( "Save Selected" );
        final MenuItem menuItemCreateForm     = new MenuItem( "Create Form" );
        menuItemSaveToFile.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle( ActionEvent event ) {
                final FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().addAll( new FileChooser.ExtensionFilter( "html", "*.html" ),
                                                    new FileChooser.ExtensionFilter( "htm", "*.htm" ) );
                final File selection  = fileChooser.showSaveDialog( null );
                    if ( selection == null )   {
                        labelMessages.setText( "Save File: Cancelled" );
                    } else  {
                        PrintWriter writer   = null;
                        try {
                            selection.delete();
                            selection.createNewFile();
                            writer   = new PrintWriter( new FileWriter( selection ) );
                            String[] lines  = noteEditor.getHtmlText().split( "\n" );
                            for ( int lineIdx = 0; lineIdx < lines.length; lineIdx++ )
                                writer.write( lines[ lineIdx ] ); 
                            writer.close();
                            labelMessages.setText( "Saved to File: " + selection.getCanonicalPath() );
                        } catch ( FileNotFoundException ex ) {
                            Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                        } catch ( IOException ex ) {
                            Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                        } finally    {
                            if ( writer != null )
                                writer.close();
                        }
                    }
            }
        });
        menuItemFetchFile.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                final FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().addAll( new FileChooser.ExtensionFilter( "html", "*.html" ),
                                                    new FileChooser.ExtensionFilter( "htm", "*.htm" ) );
                final File chosenFile  = fileChooser.showOpenDialog( null );
                if ( chosenFile == null )   {
                        labelMessages.setText( "Fetch File: Cancelled" );                
                } else  {
                    FileReader reader   = null;
                    try {
                        reader   = new FileReader( chosenFile );
                        char[] fileBuffer   = new char[ ( int )chosenFile.length() ];
                        reader.read( fileBuffer );
                        noteEditor.setHtmlText( String.copyValueOf( fileBuffer ) );
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                    } finally    {
                        if ( reader != null )
                            try {
                                reader.close();
                            } catch (IOException ex) {
                                Logger.getLogger(ActivityManager.class.getName()).log(Level.SEVERE, null, ex);
                            }
                    }
                }
            }
        } ); 
        menuItemSelectAll.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                ObservableMap< ? extends Object, ? extends Object > properties = noteEditor.getProperties();
                if ( properties != null )   {
                    Iterator< ? extends Object > iterator   = properties.keySet().iterator();
                    while( iterator.hasNext() ) {
                        Object key = iterator.next();
                        System.out.println( key.toString() + " = \t\t" + properties.get( key ).toString()  );
                    }
                }
            }
        } ); 
        menuItemSaveSelected.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                labelMessages.setText( "Save Selected: not implemented yet" );
            }
        } ); 

        menuItemCreateForm.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                labelMessages.setText( "Create Form: not implemented yet" );
            }
        } ); 
        menuItems.addAll( menuItemSaveToFile, menuItemFetchFile, menuItemSelectAll, 
                            menuItemSaveSelected, menuItemCreateForm );
        final ContextMenu contextMenu    = new ContextMenu(  );
        contextMenu.getItems().addAll( menuItems );
        noteEditor.setContextMenu( contextMenu ); 
    }
    
}
