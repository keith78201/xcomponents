/*
 * Project:         ControlsFXlab
 * Date Started:    July 23, 2014
 * 
 * Module:          BorderedBrowser
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.panes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import model.Library;
import model.Reference;
import model.Subject;
import view.ActivityManager;


/**
 *
 * @author George Keith Watson
 */
public class BorderedBrowser extends BorderPane {

    private final WebView   webView             = new WebView( );
    private final WebEngine webEngine           = webView.getEngine();
    private       Subject   javaSubject         = null;
    private       Subject   necSubject          = null;
    private       Library   referenceLibrary    = null;
    
    private final SplitPane         bowserSplitPane     = new SplitPane();
    private final GridPane          adminPane           = new GridPane();
    private final ScrollPane        adminscroller       = new ScrollPane( );
    private final AddReferenceForm  addReferenceForm    = new AddReferenceForm();
    private final AddSubjectForm    addSubjectForm      = new AddSubjectForm();
    private final DeleteItemForm    deleteItemForm      = new DeleteItemForm();
    
    public BorderedBrowser  ()  {
        try {
            //Properties systemProperties = System.getProperties();
            //Iterator iterator   = systemProperties.keySet().iterator();
            //while ( iterator.hasNext() )    {
            //    Object key = iterator.next();
            //    System.out.println( key.toString() + "\t" + systemProperties.getProperty( key.toString() ) );
            //}
            javaSubject         = new Subject( "Java" );
            javaSubject.addReference( new Reference( "Sun\'s Tutorials", "file:///root/Java/tutorial/reallybigindex.html" )  );
            javaSubject.addReference( new Reference( "JavaFX Area Chart", "file:///root/Java/JavaFX/Charts/Using JavaFX Charts  Area Chart   JavaFX 2 Tutorials and Documentation.htm" )  );
            javaSubject.addReference( new Reference( "JavaFX Bar Chart", "file:///root/Java/JavaFX/Charts/Using JavaFX Charts  Bar Chart   JavaFX 2 Tutorials and Documentation.htm" )  );
            necSubject          = new Subject( "NEC" );
            necSubject.addReference( new Reference( "The NEC itself","http://www.nfpa.org/NEC.html" ) );
            necSubject.addReference( new Reference( "YouTube Video: Intro to NEC", "http://youtube.com/vidId=INTRO_NEC" ) );
            referenceLibrary    = new Library( webEngine, javaSubject, necSubject );
            referenceLibrary.setPrefWidth( 150 );
            initializeContextMenu();

            webEngine.load( "file:///root/Java/tutorial/reallybigindex.html" );

            final ScrollPane referenceTreeScroller    = new ScrollPane(  );
            referenceTreeScroller.setContent( referenceLibrary );
            
            adminscroller.setContent( adminPane );
            //bowserSplitPane.setDividerPositions( this.getHeight() / 2 );
            bowserSplitPane.setOrientation( Orientation.VERTICAL );
            bowserSplitPane.getItems().addAll( webView, adminscroller );
            setCenter( bowserSplitPane );
            setRight( referenceLibrary );
        } catch (Exception ex) {
            Logger.getLogger(BorderedBrowser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }
    
    private void addTreeItemActionListener( final Library referenceLibrary )    {
        final TreeMap< String, Object > treeItemsMap = referenceLibrary.getTreeItems();
        Iterator< String > iterator = treeItemsMap.keySet().iterator();
        while( iterator.hasNext() ) {
            final String key    = iterator.next();
            if ( TreeItem.class.isInstance( treeItemsMap.get( key ) ) ) {

            }
        }
    }
    
    private void initializeContextMenu()    {
        final ObservableList< MenuItem > menuItems   = FXCollections.observableList( new ArrayList< MenuItem >() );
        final MenuItem menuItemAddSubject       = new MenuItem( "Add Subject" );
        final MenuItem menuItemInsertSubject    = new MenuItem( "Insert Subject" );
        final MenuItem menuItemAddReference     = new MenuItem( "Add Reference" );
        final MenuItem menuItemDelete           = new MenuItem( "Delete" );
        final MenuItem menuItemEdit             = new MenuItem( "Edit" );
        final MenuItem menuItemRelocate         = new MenuItem( "Relocate" );
        menuItemAddSubject.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle( ActionEvent event ) {
                adminscroller.setContent( addSubjectForm );
            }
        });
        menuItemInsertSubject.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle( ActionEvent event ) {
                adminscroller.setContent( addSubjectForm );
            }
        });
        menuItemAddReference.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                adminscroller.setContent( addReferenceForm );
            }
        } ); 
        menuItemDelete.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
                if (referenceLibrary.getSelectionModel().getSelectedItem() != null )        {
                    deleteItemForm.setItemName( referenceLibrary.getSelectionModel().getSelectedItem().getValue() );
                    adminscroller.setContent( deleteItemForm );
                }
            }
        } ); 
        menuItemEdit.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 
        menuItemRelocate.setOnAction( new EventHandler< ActionEvent >() {
            @Override
            public void handle(ActionEvent event) {
            }
        } ); 

        menuItems.addAll( menuItemAddSubject, menuItemInsertSubject, menuItemAddReference, menuItemDelete, menuItemEdit, 
                            menuItemRelocate );
        final ContextMenu contextMenu    = new ContextMenu(  );
        contextMenu.getItems().addAll( menuItems );
        referenceLibrary.setContextMenu( contextMenu ); 
    }
    
    class AddReferenceForm extends GridPane {
        
        private final Label     labelSearchInternet = new Label( "SearchInternet" );
        private final TextField textFieldKeyWords   = new TextField( "Key Words" );
        private final TextField textFieldSiteURL    = new TextField( "Site URL" );
        private final Button    buttonSearch        = new Button( "Search" );
        
        public AddReferenceForm()   {
            add( labelSearchInternet, 0, 0 );
            add( textFieldKeyWords, 0, 2 );
            add( textFieldSiteURL, 0, 3 );
            add( buttonSearch, 0, 5 );
            
            buttonSearch.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
        }
    }
    
    class AddSubjectForm  extends GridPane  {
        
        private Label labelsubject  = new Label( "Subject: " );
        private TextField textFieldSubject  = new TextField( );
        
        public AddSubjectForm() {
            textFieldSubject.setMinWidth( 200 );
            add( labelsubject, 0, 0 );
            add( textFieldSubject, 1, 0 );
        }
    }

    class DeleteItemForm  extends GridPane  {
        
        private final Label         labelDelete     = new Label( "Delete?" );
        private final Label         labelItemName   = new Label( );
        private final ObservableList< String > choices = FXCollections.observableArrayList( "Yes", "No" );
        private final ComboBox      comboBoxYesNo   = new ComboBox( choices );
        private final Button        buttonCommit    = new Button( "Commit" );
        
        public DeleteItemForm( ) {
            
            labelItemName.setMinWidth( 300 );
            comboBoxYesNo.setMinWidth( 100 );
            buttonCommit.setMinWidth( 100 );
            add( labelDelete, 0, 0 );
            add( labelItemName, 0, 1 );
            add( comboBoxYesNo, 1, 1 );
            add( buttonCommit, 1, 2 );
            comboBoxYesNo.getSelectionModel().select( 1 );
            comboBoxYesNo.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
            buttonCommit.setOnAction( new EventHandler< ActionEvent >() {
                @Override
                public void handle(ActionEvent event) {
                }
            } ); 
        }
        
        public final void setItemName( final String itemName ) {
            labelItemName.setText( itemName );
        } 
    }
}
