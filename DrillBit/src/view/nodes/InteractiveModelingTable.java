/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          InteractiveModelingTable
 * Author           George Keith Watson
 * Date Started:    July 13, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
*/

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.util.Callback;
import model.TableRecord;
/**
 *
 * @author George Keith Watson
 * @param <RECORD_TYPE>
 */
public class InteractiveModelingTable< RECORD_TYPE > extends TableView {
    
    private final ArrayList< TableColumn >          columns                 = new ArrayList<  >();
    private final ArrayList< String >               columnNames             = new ArrayList<  >();
    private final TreeMap< String, TableColumn >    colNameMap              = new TreeMap<  >();
    private final HashMap< String, EditingCell >    colEditCellMap          = new HashMap<  >();
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    
    private final ObservableList< TableRecord > data = FXCollections.observableArrayList();

    public InteractiveModelingTable( final ObservableList data, final String... fieldNames )   {
        setPlaceholder( new Label( "Data Table" ) );
        for ( String fieldName : fieldNames )   {
            colNameMap.put( fieldName, new TableColumn(  fieldName ) );
            columns.add( colNameMap.get( fieldName ) );
            colNameMap.get( fieldName ).setMinWidth( 100 );
            columnNames.add( fieldName );
        }
        getColumns().addAll( columns );
        
        if ( data != null ) {
            setData( data );
        }
        
        for ( int index = 0; index < columns.size(); index++ )  {
            columns.get( index ).setEditable( true ); 
            columns.get( index ).setId( columnNames.get( index ) );  
            columns.get( index ).setCellValueFactory( new PropertyValueFactory<  >( columnNames.get( index ) ) );
        }
        setEditable( true );
        pluginEventHandlers();
    }
    
    public ObservableList<TableRecord> getData() {
        return data;
    }
    
    public final void setData( final ObservableList data  )   {
        if ( data != null ) {
            this.data.clear();
            this.data.addAll( data );
            setItems( data );
        }
    }
    
    public void setColumnEditable( final String colName, final boolean editable )   {
        if ( editable ) {
            final Callback< TableColumn, TableCell > cellFactory = new Callback< TableColumn, TableCell >()  {
                @Override
                public TableCell call( TableColumn p )  {
                    colEditCellMap.put( colName , new EditingCell( InteractiveModelingTable.this.data, InteractiveModelingTable.this ) );
                    return colEditCellMap.get( colName );
                }
            };
            colNameMap.get( colName ).setCellFactory( cellFactory );
            colNameMap.get( colName ).setOnEditCommit( new EventHandler< CellEditEvent< RECORD_TYPE, String > >() {
                @Override
                public void handle(CellEditEvent<RECORD_TYPE, String> event) {
                    ( ( TableRecord ) event.getTableView().getItems().get( event.getTablePosition().getRow() ) )
                            .setField( colName, event.getNewValue() ); 
                }
            }  );
            
        }
        else    {
            //disable the edit component :this doesnt work
            if ( colEditCellMap.get( colName ) != null )
                colEditCellMap.get( colName ).setEditable( false ); 
            
        }
    }
    
    public void setColumnEditable( final int colIndex, final boolean editable )   {
        if ( colIndex >= 0 && colIndex < columns.size() )
            columns.get( colIndex ).setEditable( editable ); 
    }
    
    
    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    
    
    
    public class EditingCell extends TableCell< TableRecord, String >{

        private TextField textField  = null;
        private final InteractiveModelingTable table;
        private final ObservableList data;

        public EditingCell( final ObservableList data, final InteractiveModelingTable table )    {
            this.table  = table;
            this.data   = data;
        }

        @Override
        public void startEdit() {
            if ( !isEmpty())    {
                super.startEdit();
                createTextField();
                setText( null );
                setGraphic( textField );
                textField.selectAll();

            }
        }

        @Override
        public void cancelEdit()    {
            super.cancelEdit();
            setText( (String) getItem() );
            setGraphic( null );
        }

        @Override
        public void updateItem( String item, boolean empty )    {
            super.updateItem( item, empty );
            if ( empty )    {
                setText( null );
                setGraphic( null );
            }
            else {
                if ( isEditing() )  {
                    if ( textField != null )    {
                        textField.setText( getString() ); 
                    }
                    setText( null );
                    setGraphic( textField );
                }
                else {
                    if ( table != null && data != null ) {
                        //table.getData().clear();
                        //table.getData().addAll( data );
                        table.setItems( data );

                        //table.setData( data );
                    }
                    setText( getString() );
                    setGraphic( null );
                }
            }
        }

        private void createTextField()  {
            textField   = new TextField( getString() );
            textField.setMinWidth( this.getWidth() - this.getGraphicTextGap() * 2 ); 
            textField.focusedProperty().addListener( new ChangeListener< Boolean >() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observableValue, 
                        Boolean oldFocused, Boolean focused ) {
                    if ( !focused ) {
                        commitEdit( textField.getText() );
                    }
                }
            }  ); 
        }

        private String getString()  {
            return getItem() == null ? "": getItem().toString();
        }
    }    
    
}
