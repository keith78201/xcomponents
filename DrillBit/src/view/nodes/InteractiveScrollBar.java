/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveScrollBar
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.GridPane;
import model.Range;
import view.panes.RangeControlBar;

/**
 *
 * @author George Keith Watson
 */
public class InteractiveScrollBar extends GridPane {
    
    private final ScrollBar   scrollBar;
    private Boolean interacive      = true;
    private final RangeControlBar rangeControlBar;
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public InteractiveScrollBar( final double value, final double width, final double totalHeight )  {
        scrollBar = new ScrollBar( );
        //default min anx max
        
        setTranslateY( -totalHeight/2 );
        scrollBar.setScaleY( 0.8);
        setScaleY( 0.75 );
        
        scrollBar.setMin( 0 );
        scrollBar.setMax( 100 );
        scrollBar.setValue( value ); 
        setMinHeight( totalHeight );
        setMaxHeight( totalHeight );
        setMouseTransparent( false );
        setPickOnBounds( false );
        pluginEventHandlers();
        pluginEventHandlersDepricated();
        scrollBar.setMaxHeight( totalHeight / 2 );
        scrollBar.setMinHeight( totalHeight / 2 );
        scrollBar.setMinWidth( width );
        rangeControlBar = new RangeControlBar( width, totalHeight / 2, 0, 100, 10.0 );
        rangeControlBar.setVisible( false );
        rangeControlBar.addActionListener( new PropertyChangeListener() {
            @Override
            public void propertyChange( PropertyChangeEvent event ) {
                scrollBar.setValue( ( Double )event.getNewValue() );
            }
        } );  
        add( rangeControlBar, 0, 0, 1, 1 );
        add( scrollBar, 0, 1, 1, 1 );
    }
    
    public void setIncrement( final double increment )   {
        rangeControlBar.setIncrement( increment );
    }
    
    public void setWidth( final double width )    {
        setMaxWidth( width );
        setMinWidth( width );
        scrollBar.setMaxWidth( width );
        scrollBar.setMinWidth( width );
    }

    public void setHeight( final double height )    {
        setMaxHeight( height );
        setMinWidth( height );
        scrollBar.setMaxHeight( height );
        scrollBar.setMinHeight( height );
    }

    public Boolean isInteracive() {
        return interacive;
    }
    
    public void setInteractive( final boolean interactive ) {
        this.interacive = interactive;
    }
    
    public void setRange( final Range newRange ) {
        rangeControlBar.setRange( newRange );
        rangeControlBar.setVisible( true );
        scrollBar.setMin( newRange.getLow() );
        scrollBar.setMax( newRange.getHigh() );
        scrollBar.setValue( ( rangeControlBar.getCurrentValue() - rangeControlBar.getRange().getLow() ) / ( rangeControlBar.getRange().getRange() ) );
    }
    
    private void pluginEventHandlersDepricated() {
        scrollBar.valueProperty().addListener( new ChangeListener< Number > () {
            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                final double newValue = Math.round( scrollBar.getValue() * 100 ) / 100.0;
                scrollBar.setValue( newValue );
                rangeControlBar.setCurrentValue( newValue );
            }
        }); 
        
    }

    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    
}
