/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveButton
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.Proctor;
import control.UserEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.util.Duration;
import view.ActivityManager;

/**
 * This class adds mouse-over and other interactivity to the FX Button. Instances can only be placed into
 ActivityPaneFraction instances.
 * @author George Keith Watson
 */
public class InteractiveButton extends Button implements ProXyNode {
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public InteractiveButton(  final String text ) {
        super( text );
        pluginEventHandlers();
       
    }

    public InteractiveButton( )    {
        super();
        pluginEventHandlers();
    }
  
    

    @Override
    public final Button makeProxy( final double opacity ) {
        Button proxy = new Button( getText() );
        
        //Copy all needed properties, no event handlers
        NodeUtils.copyProperties( this, proxy ); 

        proxy.opacityProperty().set( opacity );
        proxy.setMouseTransparent( false );
        proxy.setMaxSize( getMaxWidth(), getMaxHeight() );
        proxy.setMinSize( getMinWidth(), getMinHeight() );
        proxy.setPrefSize( getPrefWidth(), getPrefHeight() );
                
        return proxy;
    }
            
    private void pluginEventHandlers() {
        
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Proctor.addEvent( new UserEvent( event.toString(), event ) ); 
                sendMessage( event, ActivityManager.MESSAGE_ACTION );
            }
        });
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) ); 
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent t) {
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent t) {
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent t) {
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent t) {
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) ); 
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) ); 
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent t) {
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent t) {
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent t) {
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent t) {
            }
        } );
                
    }

    public void registerEventListener( final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    private void sendMessage( final Event event, final Object message )    {
        propertyChangeSupport.firePropertyChange( getId(), event, message );
    }

    public void enableFadeEffect( final int duration, final double opacity )  {
        //setEffect( new Reflection() );
        final Timeline timeline = new Timeline();
        timeline.setCycleCount( Timeline.INDEFINITE );
        timeline.setAutoReverse( true );
        final KeyValue kv   = new KeyValue( this.opacityProperty(), opacity );
        final KeyFrame kf   = new KeyFrame( Duration.millis( duration ), kv );
        timeline.getKeyFrames().add( kf );
        timeline.play();
    }

}
