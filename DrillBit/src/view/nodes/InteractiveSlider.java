/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveSlider
 * Author           George Keith Watson
 * Date Started:    July 26, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.GridPane;
import model.Range;
import view.panes.RangeControlBar;

/**
 *
 * @author George Keith Watson
 */
public class InteractiveSlider extends GridPane {
    
    private final Slider   slider;
    private Boolean interacive      = true;
    private boolean dragging        = false;
    private final RangeControlBar rangeControlBar;
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public InteractiveSlider( final double value, final double width, final double totalHeight )  {
        slider = new Slider( );
        
        setTranslateY( -totalHeight/2 );
        slider.setScaleY( 0.8);
        setScaleY( 0.75 );
        
        slider.setValue( value ); 
        setMinHeight( totalHeight );
        setMaxHeight( totalHeight );
        setMouseTransparent( false );
        setPickOnBounds( false );
        slider.setMaxHeight( totalHeight / 2 );
        slider.setMinHeight( totalHeight / 2 );
        pluginEventHandlers();
        pluginEventHandlersDepricated();
        rangeControlBar = new RangeControlBar( width, totalHeight / 2, 0, 100, 10.0 );
        rangeControlBar.setVisible( false );
        rangeControlBar.addActionListener( new PropertyChangeListener() {
            @Override
            public void propertyChange( PropertyChangeEvent event ) {
                slider.setValue( ( Double )event.getNewValue() );
            }
        } );  
        add( rangeControlBar, 0, 0, 1, 1 );
        add( slider, 0, 1, 1, 1 );
    }

    
    public void setIncrement( final double increment )   {
        rangeControlBar.setIncrement( increment );
    }
    
    public void setWidth( final double width )    {
        setMaxWidth( width );
        setMinWidth( width );
        slider.setMaxWidth( width );
        slider.setMinWidth( width );
    }

    public void setHeight( final double height )    {
        setMaxHeight( height );
        setMinWidth( height );
        slider.setMaxHeight( height );
        slider.setMinHeight( height );
    }

    public Boolean isInteracive() {
        return interacive;
    }
    
    public void setInteractive( final boolean interactive ) {
        this.interacive = interactive;
    }
    
    public void setRange( final Range newRange ) {
        slider.setMax( newRange.getHigh() );
        slider.setMin( newRange.getLow() );
        rangeControlBar.setRange( newRange );
        rangeControlBar.setVisible( true );
        slider.setValue( ( rangeControlBar.getCurrentValue() ) );
    }
    
    private void pluginEventHandlersDepricated() {
        slider.valueProperty().addListener( new ChangeListener< Number >() {
            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                final double newValue = Math.round( slider.getValue() * 100 ) / 100.0;
                slider.setValue( newValue );
                rangeControlBar.setCurrentValue( newValue );
            }
        }); 
        slider.setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
            }
        } );
        slider.setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive )   {
                    final Range range = rangeControlBar.getRange();
                    double newValue = range.getLow() + event.getX() / slider.getWidth() * range.getRange();
                    newValue = Math.round( newValue * 100 ) / 100.0;
                    rangeControlBar.setCurrentValue( newValue );
                    slider.setValue( newValue );
                    dragging    = true; 
                }
            }
        } );
        slider.setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive && dragging && event.getX() >= 0 && event.getX() <= slider.getWidth() )   {
                    final Range range = rangeControlBar.getRange();
                    double newValue = range.getLow() + event.getX() / slider.getWidth() * range.getRange();
                    newValue = Math.round( newValue * 100 ) / 100;
                    rangeControlBar.setCurrentValue( newValue );
                    slider.setValue( newValue );
                }
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive )
                    dragging    = false;
            }
        } );
    }

    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    

}
