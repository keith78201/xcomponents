/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;

/**
 *
 * @author George Keith Watson
 */
public class InteractiveTitledPane extends TitledPane implements ProXyNode {

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    private final String title;
    
    public InteractiveTitledPane( final String title, final Node content )  {
        super( title, content );
        this.title  = title;
    }
    
    public InteractiveTitledPane( )  {
        super( );
        this.title  = "";
    }
    
    
    @Override
    public Node makeProxy( double opacity ) {
        TitledPane proxy = new TitledPane( title, getContent() );
        
        //Copy all needed properties, no event handlers
        NodeUtils.copyProperties( this, proxy ); 

        proxy.opacityProperty().set( opacity );
        proxy.setMouseTransparent( false );
        
        //copy properties particular to a TitledPane
        proxy.setAnimated( animatedProperty().getValue() );
        proxy.setCollapsible( this.collapsibleProperty().getValue() ); 
        proxy.setContent( this.contentProperty().getValue() ); 
        proxy.setExpanded( this.expandedProperty().getValue() );
        proxy.setMaxSize( getMaxWidth(), getMaxHeight() );
        proxy.setMinSize( getMinWidth(), getMinHeight() );
        proxy.setPrefSize( getPrefWidth(), getPrefHeight() );
        
        return proxy;
    }
    
    
    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    
}
