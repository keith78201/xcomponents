/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveButton
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *
 */

package view.nodes;

import control.InputEvent;
import control.Proctor;
import control.UserEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.util.Duration;
import view.ActivityManager;

/**
 * This class adds mouse-over and other interactivity to the FX Button.
 * @author George Keith Watson
 */
public class InteractiveToggleButton extends ToggleButton {
    
    private final Timeline timeline = new Timeline();
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public InteractiveToggleButton( final String text ) {
        super( text );
        pluginEventHandlers();
        pluginEventHandlersDepricated();
    }

    public InteractiveToggleButton( )    {
        super();
        pluginEventHandlers();
        pluginEventHandlersDepricated();
    }
            
    private void pluginEventHandlersDepricated() {
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Proctor.addEvent( new UserEvent( event.toString(), event ) ); 
                sendMessageDepricated( event, ActivityManager.MESSAGE_ACTION );
            }
        });
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) );
                startFadeEffect();
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) ); 
                stopFadeEffect();
            }
        } );
    }
    
    public void enableFadeEffect( final int duration, final double opacity )  {
        //setEffect( new Reflection() );
        timeline.setCycleCount( Timeline.INDEFINITE );
        timeline.setAutoReverse( true );
        final KeyValue kv   = new KeyValue( this.opacityProperty(), opacity );
        final KeyFrame kf   = new KeyFrame( Duration.millis( duration ), kv );
        timeline.getKeyFrames().add( kf );
    }
    
    public void startFadeEffect() {
        timeline.play();
    }
    
    public void stopFadeEffect() {
        timeline.jumpTo( Duration.ZERO ); 
        timeline.stop();
    }

    public void registerEventListenerDepricated( final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    private void sendMessageDepricated( final Event event, final Object message )    {
        propertyChangeSupport.firePropertyChange( getId(), event, message );
    }

    private void pluginEventHandlers() {
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString() , message, eventId.toString() );
    }
}
