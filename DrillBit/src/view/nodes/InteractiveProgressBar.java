/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveProgressBar
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.GridPane;
import model.Range;
import view.panes.RangeControlBar;

/**
 *
 * @author George Keith Watson
 */
public class InteractiveProgressBar extends GridPane {
    
    private final ProgressBar   progressBar;
    private Boolean interacive      = true;
    private boolean dragging        = false;
    private final RangeControlBar rangeControlBar;
    
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );

    public InteractiveProgressBar( final double value, final double width, final double totalHeight ) {
        progressBar = new ProgressBar( value );
        
        setTranslateY( -totalHeight/2 );
        progressBar.setScaleY( 0.8);
        setScaleY( 0.75 );
        
        setMinHeight( totalHeight );
        setMaxHeight( totalHeight );
        setMouseTransparent( false );
        setPickOnBounds( false );
        pluginEventHandlers();
        pluginExtensionHandlers();
        progressBar.setMinHeight( totalHeight / 2 );
        progressBar.setMaxHeight( totalHeight / 2 );
        progressBar.setMinWidth( width );
        rangeControlBar = new RangeControlBar( width, totalHeight / 2, 0, 100, 10.0 );
        rangeControlBar.setVisible( false );
        rangeControlBar.addActionListener( new PropertyChangeListener() {
            @Override
            public void propertyChange( PropertyChangeEvent event ) {
                progressBar.setProgress( ( ( Double )event.getNewValue() - rangeControlBar.getRange().getLow() ) / ( rangeControlBar.getRange().getRange() ) );
            }
        } );  
        add( rangeControlBar, 0, 0, 1, 1 );
        add( progressBar, 0, 1, 1, 1 );
        setValignment( rangeControlBar, VPos.CENTER );
        setValignment( progressBar, VPos.CENTER );
    }
    
    public void setIncrement( final double increment )   {
        rangeControlBar.setIncrement( increment );
    }
    
    public void setWidth( final double width )    {
        setMaxWidth( width );
        setMinWidth( width );
        progressBar.setMaxWidth( width );
        progressBar.setMinWidth( width );
    }

    public void setHeight( final double height )    {
        setMaxHeight( height );
        setMinWidth( height );
        progressBar.setMaxHeight( height );
        progressBar.setMinHeight( height );
    }

    public Boolean isInteracive() {
        return interacive;
    }
    
    public void setInteractive( final boolean interactive ) {
        this.interacive = interactive;
    }
    
    public void setRange( final Range newRange ) {
        rangeControlBar.setRange( newRange );
        rangeControlBar.setVisible( true );
        progressBar.setProgress( ( rangeControlBar.getCurrentValue() - rangeControlBar.getRange().getLow() ) / ( rangeControlBar.getRange().getRange() ) );
    }
    
    private void pluginExtensionHandlers() {
        progressBar.setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
            }
        } );
        progressBar.setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive )   {
                    final Range range = rangeControlBar.getRange();
                    double newValue = range.getLow() + event.getX() / progressBar.getWidth() * range.getRange();
                    newValue = Math.round( newValue * 100 ) / 100.0;
                    rangeControlBar.setCurrentValue( newValue );
                    progressBar.setProgress( ( newValue - range.getLow() ) / range.getRange() );
                    dragging    = true; 
                }
            }
        } );
        progressBar.setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive && dragging && event.getX() >= 0 && event.getX() <= progressBar.getWidth() )   {
                    final Range range = rangeControlBar.getRange();
                    double newValue = range.getLow() + event.getX() / progressBar.getWidth() * range.getRange();
                    newValue = Math.round( newValue * 100 ) / 100;
                    rangeControlBar.setCurrentValue( newValue );
                    progressBar.setProgress( ( newValue - range.getLow() ) / range.getRange() );
                }
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent event) {
                if ( interacive )
                    dragging    = false;
            }
        } );
        
    }
    
    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    
}
