/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          InteractiveComboBox
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.InputEvent;
import control.Proctor;
import control.UserEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import view.ActivityManager;


/**
 *
 * @author George Keith Watson
 */
public class InteractiveComboBox extends ComboBox {

    private final StringBuffer keysTyped    = new StringBuffer();
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    
    public InteractiveComboBox( final ObservableList< String > list ) {
        super( list );
        pluginEventHandlers();
        pluginEventHandlersDepricated();
    }
    
   private void pluginEventHandlersDepricated() {
        setOnAction( new EventHandler() {
            @Override
            public void handle(Event event) {
                Proctor.addEvent( new UserEvent( event.toString(), event ) ); 
                sendMessageDepricated( event, ActivityManager.MESSAGE_ACTION );
            }
        } );

        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle(MouseEvent t) {
                Proctor.addEvent( new UserEvent( t.toString(), t ) ); 
                keysTyped.delete( 0, keysTyped.length() );
            }
        } );
                
        setOnKeyPressed( new EventHandler< KeyEvent >() {
            @Override
            public void handle(KeyEvent event) {
                switch( event.getCode() )   {
                    case UP:
                    case DOWN:
                        keysTyped.delete( 0, keysTyped.length() );
                        sendMessageDepricated( event, ActivityManager.MESSAGE_KEY_PRESSED );
                        break;
                }
            }
        } );
        setOnKeyTyped( new EventHandler< KeyEvent >() {
            @Override
            public void handle(KeyEvent event) {
                Proctor.addEvent( new UserEvent( event.toString(), event ) ); 
                final String character  = event.getCharacter();
                keysTyped.append( character );
                try {
                    boolean found = false;
                    int selection = 0;
                    while( !found && selection < InteractiveComboBox.this.getItems().size() ) {
                        if ( ( ( String )InteractiveComboBox.this.getItems().get( selection ) ).startsWith( keysTyped.toString() ) )
                            found = true;
                        else selection++;
                    }
                    if ( found )    {
                        InteractiveComboBox.this.getSelectionModel().clearAndSelect( selection ); 
                        sendMessageDepricated( event, ActivityManager.MESSAGE_KEY_TYPED );
                    }
                }
                catch ( NumberFormatException ex )   {
                }
            }
        } );
        
   }
    
    public void registerEventListenerDepricated( final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    private void sendMessageDepricated( final Event event, final Object message )    {
        propertyChangeSupport.firePropertyChange( getId(), event, message );
    }

    private void pluginEventHandlers() {
        
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
    
}
