/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          NodeUtils
 * Author           George Keith Watson
 * Date Started:    Aufust 3, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *
 */

package view.nodes;

import javafx.scene.Node;

/**
 * A collection of static utilities, like Collections, String, and Array, for JavaFX Nodes.
 * @author George Keith Watson
 */
public final class NodeUtils {
    
    public static final void copyProperties( final Node sourceNode, final Node destiNode )  {
        destiNode.setBlendMode( sourceNode.getBlendMode() );
        destiNode.setCache( sourceNode.cacheProperty().getValue() );
        destiNode.setCacheHint( sourceNode.getCacheHint() );
        destiNode.setClip( sourceNode.getClip() );
        destiNode.setCursor( sourceNode.getCursor() );
        destiNode.setDepthTest( sourceNode.getDepthTest() );
        destiNode.setDisable( sourceNode.disabledProperty().getValue() );
        destiNode.setEffect( sourceNode.getEffect() );
        destiNode.setId( sourceNode.getId() );
        destiNode.setLayoutX(sourceNode.getLayoutX() );
        destiNode.setLayoutY(sourceNode.getLayoutY() );
        destiNode.setManaged(sourceNode.managedProperty().getValue() );
        destiNode.setMouseTransparent( sourceNode.mouseTransparentProperty().getValue() );
        destiNode.setOpacity( sourceNode.getOpacity() );
        destiNode.setPickOnBounds( sourceNode.pickOnBoundsProperty().getValue() );
        destiNode.setRotate( sourceNode.getRotate() );
        destiNode.setRotationAxis( sourceNode.getRotationAxis() );
        destiNode.setScaleX( sourceNode.getScaleX() );
        destiNode.setScaleY( sourceNode.getScaleY() );
        destiNode.setScaleZ( sourceNode.getScaleZ() );
        destiNode.setStyle( sourceNode.getStyle() );
        destiNode.setTranslateX( sourceNode.getTranslateX() );
        destiNode.setTranslateY( sourceNode.getTranslateY() );
        destiNode.setTranslateZ( sourceNode.getTranslateZ() );
        destiNode.setUserData( sourceNode.getUserData() );
        destiNode.setVisible( sourceNode.visibleProperty().getValue() ); 
    }
}
