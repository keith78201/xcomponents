/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ProgrammableTreeView
 * Author           George Keith Watson
 * Date Started:    July 28, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package view.nodes;

import control.InputEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.TreeMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import model.TreeNode;

/**
 *
 * @author George Keith Watson
 */
public class ProgrammableTreeView extends TreeView {
    
    public final static String MESSAGE_TYPE_MOUSE_ENTER    = "mouse entered";
    public final static String MESSAGE_TYPE_MOUSE_EXITED   = "mouse exited";
    
    public final static String NODE_NAME_KEY           = "node name";
    public final static String NODE_NAME_LAST_NAME     = "last name";
    public final static String NODE_NAME_FIRST_NAME    = "first name";
    public final static String NODE_NAME_MIDDLE_NAME   = "midle name";
    
    private final String rootName;
    
    private final TreeNode< Node > controlTreeLeaf    = new TreeNode( "FX Control Node - Leaf" );
    private final TreeNode< TreeMap< String, TreeNode > > controlTreeBranch  = new TreeNode( "FX Control Node - Leaf" );

    private final TreeNode< Object > dataTreeLeaf     = new TreeNode( "FX Control Node - Leaf" );
    private final TreeNode< TreeMap< String, TreeNode > > dataTreeBranch  = new TreeNode( "FX Control Node - Leaf" );

    private final Button btn                    = new Button( "Press Me" );
    private final TreeItem< Button > buttonItem       = new TreeItem( btn );
    private final CheckBox checkBox             = new CheckBox( "Checl Box" ); 
    private final TreeItem< CheckBox > checkBoxItem   = new TreeItem( checkBox );
    private final ChoiceBox choiceBox           = new ChoiceBox( FXCollections.observableArrayList( "One", "Two", "Three" ) );  
    private final TreeItem< ChoiceBox > choiceBoxItem  = new TreeItem( choiceBox );
    private final TreeItem  nameBranch  = new TreeItem( "Name" );
    private final TextField textFieldLastName = new TextField( "Last" );
    private final TreeItem< TextField >   treeItemLastName    = new TreeItem( textFieldLastName );
    private final TextField textFieldFirstName = new TextField( "First" );
    private final TreeItem< TextField >   treeItemFirstName    = new TreeItem( textFieldFirstName );
    private final TextField textFieldMiddleName = new TextField( "Middle" );
    private final TreeItem< TextField >   treeItemMiddleName    = new TreeItem( textFieldMiddleName );

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport( this );
    
    public ProgrammableTreeView( final String rootName )   {
        this.rootName   = rootName;
        choiceBox.getSelectionModel().selectFirst();
        choiceBox.valueProperty().addListener( new ChangeListener() {
            @Override
            public void changed( ObservableValue arg0, Object arg1, Object arg2 ) {
                System.out.println("ChoiceBox TreeItem value changed to: " + choiceBox.valueProperty().getValue() );
            }
        }  ); 
        nameBranch.getChildren().add( treeItemLastName );
        nameBranch.getChildren().add( treeItemFirstName );
        nameBranch.getChildren().add( treeItemMiddleName );
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        pluginHoverListers();
        TreeItem rootItem   = new TreeItem( rootName );
        rootItem.getChildren().addAll( buttonItem, checkBoxItem, choiceBoxItem, nameBranch );
        rootItem.setExpanded( true );
        
        setRoot( rootItem );
    }
    
    private void pluginHoverListers()   {
        textFieldLastName.setOnMouseEntered( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_LAST_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_ENTER, message );                
            }
        } );
        textFieldFirstName.setOnMouseEntered( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_FIRST_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_ENTER, message );                
            }
        } );
        textFieldMiddleName.setOnMouseEntered( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_MIDDLE_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_ENTER, message );                
            }
        } );
        
        textFieldLastName.setOnMouseExited( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_LAST_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_EXITED, message );                
            }
        } );
        textFieldFirstName.setOnMouseExited( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_FIRST_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_EXITED, message );                
            }
        } );
        textFieldMiddleName.setOnMouseExited( new EventHandler < MouseEvent > () {
            @Override
            public void handle(MouseEvent event) {
                final TreeMap< String, String > message = new TreeMap< >();
                message.put( NODE_NAME_KEY, NODE_NAME_MIDDLE_NAME );
                sendMessageDepricated( MESSAGE_TYPE_MOUSE_EXITED, message );                
            }
        } );
    }
    
    public void registerListenerDepricated( final PropertyChangeListener listener ) {
        propertyChangeSupport.addPropertyChangeListener( listener ); 
    }
    
    public void sendMessageDepricated( final String identifier, final TreeMap< String, String > message )  {
        propertyChangeSupport.firePropertyChange( identifier, message, null );
    }
    
    
    private void pluginEventHandlers() {
        setOnMouseClicked( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_CLICKED, event );
            }
        } );
        setOnMouseDragEntered( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle(MouseDragEvent event) {
                sendMessage( InputEvent.MOUSE_DRAG_ENTERED, event );
            }
        }  );
        setOnMouseDragExited( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_EXITED, event );
            }
        }  );
        setOnMouseDragOver( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_OVER, event );
            }
        }  );
        setOnMouseDragReleased( new EventHandler< MouseDragEvent >() {
            @Override
            public void handle( MouseDragEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAG_RELEASED, event );
            }
        }  );
        setOnMouseDragged( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_DRAGED, event );
            }
        } );
        setOnMouseEntered( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_ENTER, event );
            }
        } );
        setOnMouseExited( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_EXIT, event );
            }
        } );
        setOnMouseMoved( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_MOVED, event );
            }
        } );
        setOnMousePressed( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_PRESSED, event );
            }
        } );
        setOnMouseReleased( new EventHandler< MouseEvent >() {
            @Override
            public void handle( MouseEvent event ) {
                sendMessage( InputEvent.MOUSE_RELEASED, event );
            }
        } );
       
        setOnSwipeDown( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_DOWN, event );
            }
        } );
        setOnSwipeLeft( new EventHandler< SwipeEvent >() {
            @Override
            public void handle(SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_LEFT, event );
            }
        } );
        setOnSwipeRight( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_RIGHT, event );
            }
        } );
        setOnSwipeUp( new EventHandler< SwipeEvent >() {
            @Override
            public void handle( SwipeEvent event ) {
                sendMessage( InputEvent.SWIPE_UP, event );
            }
        } );
                
    }
    
    public void registerListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.addPropertyChangeListener( name, listener );
    }
    
    public void removeListener( final String name, final PropertyChangeListener listener )    {
        propertyChangeSupport.removePropertyChangeListener( name, listener );
    }
    
    private void sendMessage( final InputEvent eventId, final Event message ) {
        propertyChangeSupport.firePropertyChange( eventId.toString(), message, this );
    }
}
