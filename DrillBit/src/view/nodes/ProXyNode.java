/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ProXyNode
 * Author           George Keith Watson
 * Date Started:    Aufust 3, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *
 */

package view.nodes;

import javafx.scene.Node;

/**
 *
 * @author George Keith Watson
 */
public interface ProXyNode {
    
    /**
     *
     * @param opacity
     * @return
     */
    public Node makeProxy( final double opacity );

}
