/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          DrillBit
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      2014-07-10:
 *      This is a fraction drill application modeled on flash cards.  I am using PennFoster's "Math for the
 *      Trades" study unit from their Residential Electrician Diploma program for definition of the study 
 *      material to include.  This product idea can be generalized for learning many different skills for 
 *      various trades.  I will be developing a release for tne Android operating system when I finish
 *      this JavaFX version, and then publish it for free on google Play and the Amazon app store.  Numerous
 *      such apps are possible, replacing expensive tutors and brick and mortar classrooms, which for even
 *      the simplest community college courses cost $400 per selester unit or more.
 *      
 */

package drillbit;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.ActivityManager;

/**
 *
 * @author Geoge Keith Watson
 */
public class DrillBit extends Application {
    
    private ActivityManager         rootPane;

    @Override
    public void start( final Stage primaryStage ) {
        //MathLib.checkPrimes();
        rootPane                    = new ActivityManager();
        Scene scene = new Scene( rootPane, ActivityManager.SCENE_WIDTH, ActivityManager.SCENE_HEIGHT );
        scene.getStylesheets().add( DrillBit.class.getResource( "DrillActivity.css" ).toExternalForm() ); 
        primaryStage.setTitle( "Drill Bit" );
        primaryStage.setScene( scene );
        primaryStage.show();
    }


    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {
        launch( args );
    }
    
   
}
