/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum StateAbbr {
    AL  ("AL"),
    AR  ("AR"),
    AZ  ("AZ"),
    CA  ("CA"),
    CO  ("CO"),
    CT  ("CT"),
    DE  ("DE"),
    FL  ("FL"),
    GA  ("GA"),
    HI  ("HI"),
    ID  ("ID"),
    IW  ("IW"),
    IA  ("IA"),
    KN  ("KN"),
    KT  ("KT"),
    LA  ("LA"),
    ME  ("ME"),
    MI  ("MI"),
    MN  ("MN"),
    MS  ("MS"),
    NC  ("NC"),
    ND  ("ND"),
    NM  ("NM"),
    NV  ("NV"),
    NY  ("NY"),
    OH  ("OH"),
    OR  ("OR"),
    PN  ("PN"),
    RI  ("RI"),
    SC  ("SC"),
    SD  ("SD"),
    TX  ("TX"),
    VA  ("VA"),
    WA  ("WA"),
    WY  ("WY");
    
    private final String abbr;
    
    private StateAbbr( final String text )  {
        abbr  = text;  
    }
    
    public static final ObservableList< String > list() {
        final List< StateAbbr > enumList  = Arrays.asList(  AL, AR, AZ, CA, CO, CT, DE, FL, GA, HI, ID, IW, IA, KN,
                                                    KT, LA, ME, MI, MN, MS, NC, ND, NM, NV, NY, OH, OR, PN,
                                                    RI, SC, SD, TX, VA, WA, WY );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( StateAbbr element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
    
    
    @Override
    public String toString()    {
        return abbr;
    }
}
