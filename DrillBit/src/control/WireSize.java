/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          WireSize
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import static control.MathOperation.ADDITION;
import static control.MathOperation.DIVISION;
import static control.MathOperation.MULTIPLICATION;
import static control.MathOperation.SUBTRACTION;
import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum WireSize {
    
    AWG_0  ( "awg 0" ),
    AWG_1  ( "awg 1" ),
    AWG_2  ( "awg 2" ),
    AWG_3  ( "awg 3" ),
    AWG_4  ( "awg 4" ),
    AWG_6  ( "awg 6" ),
    AWG_8  ( "awg 8" ),
    AWG_10  ( "awg 10" ),
    AWG_12  ( "awg 12" ),
    AWG_14  ( "awg 14" ),
    AWG_16  ( "awg 16" ),
    AWG_18  ( "awg 18" );


    private final String text;

    private WireSize( final String text )   {
        this.text   = text;
    }
    
    public static final ObservableList< String > list() {
        final List< WireSize > enumList  = Arrays.asList( AWG_0, AWG_1, AWG_2, AWG_3, AWG_4, AWG_6, AWG_8,
                                                        AWG_10, AWG_12, AWG_14, AWG_16, AWG_18         );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( WireSize element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
            
    @Override
    public String toString()    {
        return text;
    }
    
    
}
