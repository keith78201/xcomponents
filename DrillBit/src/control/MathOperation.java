/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          MathOperation
 * Author           George Keith Watson
 * Date Started:    July 18, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum MathOperation {
    NULL                ( "Null" ),
    ADDITION            ( "Addition" ),
    SUBTRACTION         ( "Subtraction" ),
    MULTIPLICATION      ( "Multiplication" ),
    DIVISION            ( "Division" );
    
    private final String name;
    
    private MathOperation( final String name ) {
        this.name   = name;
    }
    
    public static MathOperation getOp( final String text )  {
        switch( text )  {
            case "Addition": return ADDITION;
            case "Subtraction": return SUBTRACTION;
            case "Multiplication": return MULTIPLICATION;
            case "Division": return DIVISION;
            default:
                return NULL;
        }
    }
    
    public static final ObservableList< String > list() {
        final List< MathOperation > enumList  = Arrays.asList( ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( MathOperation element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
            
    @Override
    public String toString()    {
        return name;
    }
    
}
