/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          XModel
 * Author           George Keith Watson
 * Date Started:    July 26, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.ArrayList;
import model.ObservableProperty;

/**
 *
 * @author George Keith Watson
 */
public interface XModel {
    /**
     * Construct and return the list off properties that the model implementing this interface wants a user 
     * interface to know about, either for read only display or for data entry.
     * @return 
     */
    public abstract ArrayList< ObservableProperty > getProperties();
    
    /** Construct and return the list of properties that dependent for their calculation on the named property.
     * A user interface can then update a view control to show the updated value after the named property
     * has been updated by user or other input.
     * @param propertyName
     * @return 
     */
    public abstract ArrayList< ObservableProperty > getDependencies( final String propertyName );

    /** 
     * Convenience method which does the same thing as getDependencies( final String propertyName ).
     * @param propertyName
     * @return 
     */
    public abstract ArrayList< ObservableProperty > getDependencies( final ObservableProperty property );
    
    public abstract ObservableProperty getProperty( final String name );
    
    public abstract void updateProperty( final String propertyName, final Object value );
}
