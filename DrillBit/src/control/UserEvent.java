/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          UserEvent
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

/**
 *
 * @author George Keith Watson
 */
public class UserEvent {
    
    private final long timeStamp;
    private final String description;
    private final Object descriptor;
    
    public UserEvent( final String description, final Object descriptor )  {
        timeStamp  = System.currentTimeMillis();
        this.description       = description;
        this.descriptor = descriptor;
    }
}
