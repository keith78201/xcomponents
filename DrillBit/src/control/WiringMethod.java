/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          WiringMethod
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum WiringMethod {
    
    ARMORED_CABLE                           ( "Armored Cable" ),
    CATV_CABLES                             ( "CATV Cables" ),
    CLASS_2_AND_CLASS_3_CABLES              ( "Class 2 and Class 3 Cables" ),
    COMMUNICATIONS_CABLES                   ( "Communications Cables" ),
    COMMUNICATIONS_RACEWAYS                 ( "Communications Raceways" ),
    ELECTRICAL_METALIC_TUBING               ( "Electrical Metalic Tubing" ),
    ELECTRICAL_NONMETALIC_TUBING            ( "Electrical Nonmetalic Tubing" ),
    FIRE_ALARM__CABLES                      ( "Fire Alarm Cables" ),
    FLEXIBLE_METAL_CONDUIT                  ( "Flexible Metal Conduit" ),
    FLEXIBLE_METALIC_TUBING                 ( "Flexible Metallic Tubing " ),
    INSTRUMENTATION_TRAY_CABLE              ( "Instrumentation Tray Cable" ),
    INTERMEDIATE_METAL_CONDUIT              ( "Intermediate Metal Conduit" ),
    LIQUIDTIGHT_FLEXIBLE_METAL_CONDUIT      ( "Liquidtight Flexible Metal Conduit" ),
    LIQUIDTIGHT_FLEXIBLE_NONMETAL_CONDUIT   ( "Liquidtight Flexible Nonmetal Conduit" ),
    METAL_CLAD_CABLE                        ( "Metal-Clad Cable" ),
    MINERAL_INSULATED_METAL_SHEATHED_CABLE  ( "Mineral-Insulated, Metal-Seathed Cable" ),
    MULTICONDUCTOR_SERVICE_ENTRANCE_CABLE   ( "Multiconductor Service-Entrance Cable" ),
    MULTICOND_UNDGRND_FDR_BRNCH_CIRCUIT     ( "Multiconductor Underground Feeder and Branch-Circuit Cable" ),
    NTWK_PWRD_BROADBAND_COMM_CABLES         ( "Network-powered broadband communications cables" ),
    NONMETALIC_SHEATHED_CABLE               ( "Nonmetalic-Sheathed Cable" ),
    NON_POWER_LIMITED_FIRE_ALARM_CABLE      ( "Non-Power-Limited Fire Alarm Cable" ),
    OPTICAL_FIBER_CABLES                    ( "Optical Fiber Cables" ),
    OPTICAL_FIBER_RACEWAYS                  ( "Optical Fiber Raceways" ),
    OTH_FACTORY_ASSEMBLED_CABLE_TRAY_CBLS   ( "Other Factory Assembled Cable Tray Cables" ),
    POLYVINAL_CHLORIDE_CONDUIT              ( "Polyvinyl Chloride Conduit" ),
    POWER_AND_CONTROL_TRAY_CABLE            ( "Power and Control Tray Cable" ),
    POWER_LIMITED_FIRE_ALARM_CABLE          ( "Power-Limited Fire Alarm Cable" ),
    POWER_LIMITED_TRAY_CABLE                ( "Power-Limited Tray Cable" ),
    RIGID_METAL_CONDUIT                     ( "Rigid Metal Conduit" ),
    RIGID_NONMETALLIC_CONDUIT               ( "Rigid Nonmetallic Conduit" ),
    REINFORCED_THERMOSETTING_RESIN_CONDUIT  ( "Reinforced Thermosetting Resin Conduit" ),
    SIGNALING_RACEWAY                       ( "Signaling Raceway" );
    
    private final String text;
    
    private WiringMethod( final String text )   {
        this.text   = text;
    }
    
    public static final ObservableList< String > list() {
        final List< WiringMethod > enumList  = Arrays.asList( ARMORED_CABLE, CATV_CABLES,
                CLASS_2_AND_CLASS_3_CABLES,         COMMUNICATIONS_CABLES,
                COMMUNICATIONS_RACEWAYS,            ELECTRICAL_METALIC_TUBING,
                ELECTRICAL_NONMETALIC_TUBING,       FIRE_ALARM__CABLES,
                FLEXIBLE_METAL_CONDUIT,             FLEXIBLE_METALIC_TUBING,
                INSTRUMENTATION_TRAY_CABLE,         INTERMEDIATE_METAL_CONDUIT,
                LIQUIDTIGHT_FLEXIBLE_METAL_CONDUIT, LIQUIDTIGHT_FLEXIBLE_NONMETAL_CONDUIT,
                METAL_CLAD_CABLE,                   MINERAL_INSULATED_METAL_SHEATHED_CABLE,
                MULTICONDUCTOR_SERVICE_ENTRANCE_CABLE,  MULTICOND_UNDGRND_FDR_BRNCH_CIRCUIT,
                NTWK_PWRD_BROADBAND_COMM_CABLES,    NONMETALIC_SHEATHED_CABLE,
                NON_POWER_LIMITED_FIRE_ALARM_CABLE, OPTICAL_FIBER_CABLES,
                OPTICAL_FIBER_RACEWAYS,             OTH_FACTORY_ASSEMBLED_CABLE_TRAY_CBLS,
                POLYVINAL_CHLORIDE_CONDUIT,         POWER_AND_CONTROL_TRAY_CABLE,
                POWER_LIMITED_FIRE_ALARM_CABLE,     POWER_LIMITED_TRAY_CABLE,
                RIGID_METAL_CONDUIT,                RIGID_NONMETALLIC_CONDUIT,
                REINFORCED_THERMOSETTING_RESIN_CONDUIT, SIGNALING_RACEWAY        );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( WiringMethod element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
            
    @Override
    public String toString()    {
        return text;
    }
    
}
