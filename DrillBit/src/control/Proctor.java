/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          Proctor
 * Author           George Keith Watson
 * Date Started:    July 10, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *          This class facilitates automatic proctoring or supervision of user learning activities, such
 *          as flash card style drilling, quizzes and exams.  If this is dsployed as a trusted, signed
 *          applet, it should be possible, using embedded native code, to monitor the user's use of other 
 *          programs on their computer, such as running or starting other applications than their browser,
 *          having more than one tab active, etc.
 *
 */

package control;

import java.util.ArrayList;

/**
 *
 * @author George Keith Watson
 */
public class Proctor {
    
    private static final ArrayList< UserEvent > userEventLog    = new ArrayList<  >();
    
    public static void addEvent( final UserEvent userEvent )    {
        userEventLog.add( userEvent );
    }
    
    
}
