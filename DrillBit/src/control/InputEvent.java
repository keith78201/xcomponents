/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          UserEvent
 * Author           George Keith Watson
 * Date Started:    August 3, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;

/**
 *
 * @author George Keith Watson
 */
public enum InputEvent {
    MOUSE_CLICKED           ( "mouse clicked" ),
    MOUSE_ENTER             ( "mouse enter" ),
    MOUSE_EXIT              ( "mouse exit" ),
    MOUSE_MOVED             ( "mouse moved" ),
    MOUSE_PRESSED           ( "mouse pressed" ),
    MOUSE_RELEASED          ( "mouse released" ),
    MOUSE_DRAG              ( "mouse drag" ),
    MOUSE_HOVER             ( "mouse hover" ),
    
    KEY_DOWN                ( "key down" ),
    KEY_UP                  ( "key up" ),
    KEY_PRESSED             ( "key pressed" ),
    
    SELECTION_CHANGE        ( "selection change" ),
    ACTION                  ( "action" ),
    CONTEXT_MENU_REQUEST    ( "context menu request" ),
    
    DRAG_DETECTED           ( "drag detected" ),
    DRAG_DONE               ( "drag done" ),
    DRAG_DROP               ( "drag drop" ),
    DRAG_ENTERED            ( "drag entered" ),
    DRAG_EXITED             ( "drag exited" ),
    DRAG_OVER               ( "drag over" ),
    
    MOUSE_DRAG_ENTERED      ( "mouse drag" ),
    MOUSE_DRAG_EXITED       ( "mouse enter" ),
    MOUSE_DRAG_OVER         ( "mouse drag" ),
    MOUSE_DRAG_RELEASED     ( "mouse enter" ),
    MOUSE_DRAGED            ( "mouse drag" ),
    
    ROTATE            ( "rotate" ),
    ROTATE_START      ( "rotate start" ),
    ROTATE_FINISH     ( "rotate finish" ),

    SCROLL            ( "scroll" ),
    SCROLL_START      ( "scroll start" ),
    SCROLL_FINISH     ( "scroll finish" ),
    
    SWIPE_DOWN        ( "swipe down" ),
    SWIPE_LEFT        ( "swipe left" ),
    SWIPE_RIGHT       ( "swipe right" ),
    SWIPE_UP          ( "swipe up" ),
    
    TOUCH_MOVED       ( "touch moved" ),
    TOUCH_PRESSED     ( "touch pressed" ),
    TOUCH_RELEASED    ( "touch released" ),
    TOUCH_STATIONARY  ( "touch stationary" ),

    ZOOM              ( "zoom" ),
    ZOOM_FINISHED     ( "zoom finished" ),
    ZOOM_STARTED      ( "zoom started" );

    private final String identifier;
    
    private InputEvent( final String identifier )   {
        this.identifier = identifier;
        
        Button button   = new Button();
    }
    
    public static final ObservableList< String > list() {
        final List< InputEvent > enumList  = Arrays.asList(  
                MOUSE_CLICKED,      MOUSE_ENTER,        MOUSE_EXIT,         MOUSE_MOVED,
                MOUSE_PRESSED,      MOUSE_RELEASED,     MOUSE_DRAG,         MOUSE_HOVER,
                KEY_DOWN,           KEY_UP,             KEY_PRESSED,        SELECTION_CHANGE,
                ACTION,             CONTEXT_MENU_REQUEST,   DRAG_DETECTED,  DRAG_DONE,
                DRAG_DROP,          DRAG_ENTERED,       DRAG_EXITED,        DRAG_OVER,
                MOUSE_DRAG_ENTERED, MOUSE_DRAG_EXITED,  MOUSE_DRAG_OVER,    MOUSE_DRAG_RELEASED,
                MOUSE_DRAGED,       ROTATE,             ROTATE_START,       ROTATE_FINISH,
                SCROLL,             SCROLL_START,       SCROLL_FINISH,      SWIPE_DOWN,
                SWIPE_LEFT,         SWIPE_RIGHT,        SWIPE_UP,           TOUCH_MOVED,
                TOUCH_PRESSED,      TOUCH_RELEASED,     TOUCH_STATIONARY,   ZOOM,
                ZOOM_FINISHED,      ZOOM_STARTED  );
        final ObservableList< String > list = FXCollections.observableArrayList(
               
        );
        for ( InputEvent element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
    
    public final String toString()  {
        return identifier;
    }
}
