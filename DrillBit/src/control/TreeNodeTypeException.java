/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          TreeNodeTypeException
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

/**
 *
 * @author George Keith Watson
 */
public class TreeNodeTypeException extends Exception {
    
    public TreeNodeTypeException( final String message )    {
        super( message );
    }
}
