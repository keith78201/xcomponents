/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ConductorType
 * Author           George Keith Watson
 * Date Started:    July 27, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum ConductorType {
    FEP         ( "FEP" ),
    FEPB        ( "FEPB" ),
    FFH_2       ( "FFH-2" ),
    
    KF_1        ( "KF-1" ),
    KF_2        ( "KF-2" ),
    KFF_1        ( "KFF-1" ),
    KFF_2        ( "KFF-2" ),
    
    PAF         ( "PAF" ),
    PAFF        ( "PAFF" ),
    PF          ( "PF" ),
    PFA         ( "PFA" ),
    PFAH        ( "PFAH" ),
    PFF         ( "PFF" ),
    PGF         ( "PGF" ),
    PGFF        ( "PGFF" ),
    PTF         ( "PTF" ),
    PTFF        ( "PTFF" ),
    RFH_1       ( "RFH-1" ),
    RFH_2       ( "RFH-2" ),
    RHH         ( "RHH" ),
    RHW         ( "RHW" ),
    RHW_2       ( "RHW-2" ),
    SF_1        ( "SF-1" ),
    SF_2        ( "SF-2" ),
    SFF_1       ( "SF-1" ),
    SFF_2       ( "SFF-2" ),
    TF          ( "TF" ),
    TFE         ( "TFE" ),
    TFF         ( "TFF" ),
    TFN         ( "TFN" ),
    TFFN        ( "TFFN" ),
    THHN        ( "THHN" ),
    THHW        ( "THHW" ),
    THW         ( "THW" ),
    THWN        ( "THWN" ),
    THW_2       ( "THW-2" ),
    THWN_2      ( "THWN-2" ),
    TW          ( "TW" ),    
    XF          ( "XF" ),
    XFF         ( "XFF" ),
    XHH         ( "XHH" ),
    XHHW        ( "XHHW" ),
    XHHW_2      ( "XHHW-2" ),
    Z           ( "Z" ),
    ZF          ( "ZF" ),
    ZFF         ( "ZFF" ),
    ZW          ( "ZW" );
    
    private final String text;
    
    private ConductorType( final String text ) {
        this.text   = text;
    }
    
    public static final ObservableList< String > list() {
        final List< ConductorType > enumList  = Arrays.asList(
        
        );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( ConductorType element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
            
    @Override
    public String toString()    {
        return text;
    }
    
}
