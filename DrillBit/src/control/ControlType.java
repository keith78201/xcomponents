/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          ControlType
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;


/**
 *
 * @author George Keith Watson
 */
public enum ControlType {
    ACCORDION           ( "Accordion" ),
    BUTTON              ( "Button" ),
    CHART_BAR           ( "Bar Chart" ),
    CHART_AREA          ( "Area Chart" ),
    CHART_BUBBLE        ( "Bubble Chart" ),
    CHART_LINE          ( "Line Chart" ),
    CHART_PIE           ( "Pie Chart" ),
    CHART_SCATTER       ( "Scatter Chart" ),
    CHART_STACKED_AREA  ( "Stacked Area Chart" ),
    CHART_STACKED_BAR   ( "Stacked Bar Chart" ),
    CHECK_BOX           ( "Check Box" ),
    CHOICE_BOX          ( "Choice Box" ),
    COLOR_PICKER        ( "Color Picker" ),
    COMBO_BOX           ( "Combo Box" ),
    FILE_CHOOSER        ( "File Chooser" ),
    HYPER_LINK          ( "Hyperlink" ),
    LABEL               ( "Label" ),
    LIST                ( "List" ),
    PASSWORD_FIELD      ( "Password Field" ),
    PROGRESS_BAR        ( "Progress Bar" ),
    RADIO_BUTTON        ( "Radio Button" ),
    SCROLL_BAR          ( "Scroll Bar" ),
    SLIDER              ( "Slider" ),
    TEXT_AREA           ( "Text Area" ),
    TEXT_FIELD          ( "Text Field" ),
    TOGGLE_BUTTON       ( "Toggle Button" ),
    TREE_VIEW           ( "Tree View" );
    
    private final String name;

    private ControlType( final String name )   {
        this.name   = name;
    }

    public static final List< ControlType > list(  ) {
        List< ControlType > enumList  = Arrays.asList( LABEL, BUTTON, RADIO_BUTTON, TOGGLE_BUTTON, CHECK_BOX,
                    CHOICE_BOX, TEXT_FIELD, TREE_VIEW, COMBO_BOX, SLIDER, HYPER_LINK, PASSWORD_FIELD,
                    COLOR_PICKER, FILE_CHOOSER );
        return enumList;
    }

    @Override
    public String toString()    {
        return name;
    }
    
}
