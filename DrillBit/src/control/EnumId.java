/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          EnumId
 * Author           George Keith Watson
 * Date Started:    July 23, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

/**
 *
 * @author George Keith Watson
 */
public enum EnumId {
    TEMPERATURE_RATING,
    CONDUIT_TYPE,
    CONDUIT_SIZE,
    OUTLET_BOX_TYPE,
    OUTLET_BOX_SIZE,
    JUNCTION_BOX_TYPE,
    JUNCTION_BOX_SIZE,
    PULL_BOX_TYPE,
    PULL_BOX_SIZE,
    CONDUIT_BODY_TYPE,
    CONDUIT_BODY_SIZE,

    WIRING_METHOD,
    WIRE_SIZE,
    CONDUCTOR_TYPE,
    
    CONTROL_TYPE,
    MATH_OPERATION,
    SIMPLE_PROPERTY_TYPE,
    STATE_ABBRV;
}
