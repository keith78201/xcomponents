/*
 * Project:         DrillBit
 * Date Started:    July 10, 2014
 * 
 * Module:          StringCodec
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 *      2014-07-29L
 *      This module will code and decode Strings for security.  It does relatively simple obfuscation for now.
*       Since this will be an open source, Apache licensed project, the user programmer is highly encouraged
 *      to write their own implementing its interface where it is applied in my DrillBit source code,
 *      or anywhere they find security requires it.
 *
 */

package control;

import java.util.TreeMap;

/**
 *
 * @author George Keith Watson
 */
public class StringCodec {
    
    private final static int RAND_LENGTH    = 10;
    private final static TreeMap< String, String > coder   = new TreeMap< String, String >();
            
    public final static String encode( final String text )  {
        final StringBuffer buffer = new StringBuffer( text );
        coder.put( text, buffer.reverse().insert( 0, random( RAND_LENGTH ) ).toString() ); 
        return coder.get( text );
    }
    
    public final static String decode( final String text )  {
        return coder.get( text );
    }
    
    public final static String encode( final char[] text )  {
        final StringBuffer buffer = new StringBuffer( );
        buffer.append( text );
        coder.put( buffer.toString(), buffer.reverse().insert( 0, random( RAND_LENGTH ) ).toString()  );
        return coder.get( buffer.toString() );
    }
    
    public final static String decode( final char[] text )  {
        final StringBuffer buffer = new StringBuffer( );
        buffer.append( text );
        return coder.get( buffer.toString() );
    }
    
    public final static String encode( final Character[] text )  {
        final StringBuffer buffer = new StringBuffer( );
        buffer.append( text );
        coder.put( buffer.toString(), buffer.reverse().insert( 0, random( RAND_LENGTH ) ).toString() );
        return coder.get( buffer.toString() );
    }
    
    public final static String decode( final Character[] text )  {
        final StringBuffer buffer = new StringBuffer( );
        buffer.append( text );
        return coder.get( buffer.toString() );
    }
    
    
    private static String random( final int length )   {
        final StringBuffer buffer = new StringBuffer();
        for ( int i = 0; i < length * 5; i++ )   {
            buffer.append( ( char ) Math.round (Math.random() * Character.MAX_VALUE ) );
        }
        return buffer.toString();
    }
    
    
}
