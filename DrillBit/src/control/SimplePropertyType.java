/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          SimplePropertyType
 * Author           George Keith Watson
 * Date Started:    July 21, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package control;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author George Keith Watson
 */
public enum SimplePropertyType {
    BOOLEAN             ( "Boolean" ),
    CHARACTER           ( "Character" ),
    BYTE                ( "Byte" ),
    SHORT               ( "Short" ),
    INTEGER             ( "Integer" ),
    FRACTION            ( "Fraction" ),
    LONG                ( "Long" ),
    FLOAT               ( "Float" ),
    DOUBLE              ( "Double" ),
    STRING              ( "String" ),
    TIME                ( "Time" ),
    DATE                ( "Date" ),
    PHONE               ( "Phone" ),
    EMAIL               ( "eMail" ),
    URL                 ( "URL" ),
    POSTAL_CODE         ( "Postal Code" ),
    IP_ADDR             ( "IP Address" ),
    PROPERTY_NAME       ( "Property  Name" ),
    ENUM                ( "Enum" );
    
    private final String text;
    
    private SimplePropertyType( final String text ) {
        this.text   = text;
    }
    
    public static final ObservableList< String > list() {
        final List< SimplePropertyType > enumList  = Arrays.asList( BOOLEAN, CHARACTER, BYTE, SHORT, INTEGER,
                                                    LONG, FLOAT, DOUBLE, STRING, TIME, DATE, PHONE, EMAIL,
                                                    URL, POSTAL_CODE, IP_ADDR, PROPERTY_NAME, ENUM );
        final ObservableList< String > list = FXCollections.observableArrayList();
        for ( SimplePropertyType element : enumList )    {
            list.add( element.toString() );
        }
        return list;
    }
    @Override
    public String toString()    {
        return text;
    }
            
}
