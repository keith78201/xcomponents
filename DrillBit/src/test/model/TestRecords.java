/*
 * Project:         DrillBit
 * Date Started:    July 13, 2014
 * 
 * Module:          TestRecords
 * Author           George Keith Watson
 * Date Started:    July 19, 2014
 * Copyright:       (c) Copyright 2014, irrevocable inter-vivos trust of Author, d.b.a. modelSoftTech
 * Development:
 */

package test.model;

import control.ControlType;
import control.EnumId;
import control.SimplePropertyType;
import control.TreeDescriptorException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.paint.Color;
import model.ObservableProperty;
import model.ObservableRecord;
import model.OhmsWheelRecord;
import model.filter.AllowedValuesList;
import model.filter.EnumDescriptor;
import model.filter.ListDescriptor;
import model.filter.MapDescriptor;
import model.filter.StringDescriptor;
import model.filter.TreeDescriptor;
import model.filter.TreeNode;

/**
 *
 * @author George Keith Watson
 */
public class TestRecords {
    
    public static final ObservableRecord randomProperties = new ObservableRecord();
    public static final OhmsWheelRecord electricalProperties = new OhmsWheelRecord(
                                                            "120.0", "20.0", "6.0", "2400.0", "10.0", "24.0" );
    
    
    private static final ObservableList< PieChart.Data > sliceData = FXCollections.observableArrayList( 
                                                    new PieChart.Data( "Slice One", 10 ),
                                                    new PieChart.Data( "Slice Two", 20 ),
                                                    new PieChart.Data( "Slice Three", 30 ));
    private static final XYChart.Series barSeries  = new XYChart.Series( FXCollections.observableArrayList( 
                                                    new XYChart.Data( "Slice One", 10 ),
                                                    new XYChart.Data( "Slice Two", 20 ),
                                                    new XYChart.Data( "Slice Three", 30 ) ) );
    private static final ObservableList< String > listData = FXCollections.observableArrayList( 
                                                    "First",
                                                    "Second",
                                                    "Third");
    public static void initRandomProperties()   {
        
        final StringDescriptor stringPropertyDescriptor   = new StringDescriptor( );
        stringPropertyDescriptor.setCurrentValue( "default value" );

        TreeItem root       = new TreeItem( "root" );
        root.getChildren().addAll( new TreeItem( new Button("Button") ), 
                new TreeItem( new CheckBox( "CheckBox" ) ), new TreeItem( new ChoiceBox( FXCollections.observableArrayList(  "One", "Two", "Three", "Four", "Five", "..." ) ) ) );
        TreeView treeViewAccordian   = new TreeView( root );
        MapDescriptor< Node, Object > mapPropertyDescriptor = new MapDescriptor();
        mapPropertyDescriptor.addItem( "Button", new Button( "Press Me" ) );
        mapPropertyDescriptor.addItem( "TextField", new TextField( "Text" ) );
        mapPropertyDescriptor.addItem( "CheckBox", new CheckBox( "True or False" ) );
        mapPropertyDescriptor.addItem( "RadioButton", new RadioButton( "On or Off" ) );
        mapPropertyDescriptor.addItem( "ToggleButton", new ToggleButton( "Toggle it" ) );
        mapPropertyDescriptor.addItem( "ChoiceBox", new ChoiceBox( FXCollections.observableArrayList(  "One", "Two", "Three", "Four", "Five", "..." ) ) );
        mapPropertyDescriptor.addItem( "ListView", new ListView( FXCollections.observableArrayList(  "One", "Two", "Three", "Four", "Five", "..." ) ) );
        mapPropertyDescriptor.addItem( "ComboBox", new ComboBox( FXCollections.observableArrayList(  "One", "Two", "Three", "Four", "Five", "..." ) ) );
        mapPropertyDescriptor.addItem( "PasswordField", new PasswordField( ) );
        mapPropertyDescriptor.addItem( "ColorPicker", new ColorPicker( ) );
        mapPropertyDescriptor.addItem( "TreeView", treeViewAccordian );
        mapPropertyDescriptor.addItem( "Slider", new Slider() );

        mapPropertyDescriptor.setValue( "Button", "Press Me" );
        mapPropertyDescriptor.setValue( "TextField", "Text" );
        mapPropertyDescriptor.setValue( "CheckBox", true );
        mapPropertyDescriptor.setValue( "RadioButton", true );
        mapPropertyDescriptor.setValue( "ToggleButton", true );
        mapPropertyDescriptor.setValue( "ChoiceBox", "One" );
        mapPropertyDescriptor.setValue( "ListView", "Two" );
        mapPropertyDescriptor.setValue( "ComboBox", "Three" );
        mapPropertyDescriptor.setValue( "PasswordField", "password" );
        mapPropertyDescriptor.setValue( "ColorPicker", Color.AQUA );
        mapPropertyDescriptor.setValue( "TreeView", "two" );
        mapPropertyDescriptor.setValue( "Slider", 35.0 );

        randomProperties.add( new ObservableProperty(  ControlType.ACCORDION.toString(), 
                ControlType.ACCORDION,  SimplePropertyType.BOOLEAN, mapPropertyDescriptor ) );
        
        AllowedValuesList allowableButtonValues   = new AllowedValuesList();
        allowableButtonValues.add( "Selection One" );
        allowableButtonValues.add( "Selection Two" );
        allowableButtonValues.add( "Selection Three" );
        allowableButtonValues.add( "Selection Four" );
        allowableButtonValues.add( "Selection Five" );
        final ListDescriptor< String > buttonListDescriptor = new ListDescriptor( allowableButtonValues, ControlType.CHOICE_BOX );
        final EnumDescriptor butonEnumDescriptor = new EnumDescriptor( EnumId.MATH_OPERATION, ControlType.LIST );
        randomProperties.add( new ObservableProperty( ControlType.BUTTON.toString(),   
                ControlType.BUTTON, SimplePropertyType.STRING, butonEnumDescriptor ) );
        
        randomProperties.add( new ObservableProperty(  ControlType.CHECK_BOX.toString(),   
                ControlType.CHECK_BOX,  SimplePropertyType.STRING, stringPropertyDescriptor ) );
        
        AllowedValuesList allowableValues   = new AllowedValuesList();
        /*
        allowableValues.add( "First Choice" );
        allowableValues.add( "Second Choice" );
        allowableValues.add( "Third Choice" );
        allowableValues.add( "Fourth Choice" );
        final ListDescriptor< String > listDescriptor = new ListDescriptor( allowableValues );
        */
        final EnumDescriptor enumDescriptor = new EnumDescriptor( EnumId.MATH_OPERATION, ControlType.CHOICE_BOX );
        randomProperties.add( new ObservableProperty( ControlType.CHOICE_BOX.toString(), 
                ControlType.CHOICE_BOX, SimplePropertyType.STRING, enumDescriptor ) );
        
        randomProperties.add( new ObservableProperty(  ControlType.COLOR_PICKER.toString(),  
                ControlType.COLOR_PICKER, SimplePropertyType.STRING, stringPropertyDescriptor ) );

        final EnumDescriptor enumDescriptorStates   = new EnumDescriptor( EnumId.STATE_ABBRV, ControlType.LIST );
        randomProperties.add( new ObservableProperty(  ControlType.COMBO_BOX.toString(),  
                ControlType.COMBO_BOX,  SimplePropertyType.STRING, enumDescriptorStates ) );
        
        randomProperties.add( new ObservableProperty(  ControlType.FILE_CHOOSER.toString(),   
                ControlType.FILE_CHOOSER, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.HYPER_LINK.toString(), 
                ControlType.HYPER_LINK, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.LABEL.toString(), 
                ControlType.LABEL,      SimplePropertyType.STRING, stringPropertyDescriptor ) );
        
        
        allowableValues   = new AllowedValuesList();
        allowableValues.add( "List Item One" );
        allowableValues.add( "List Item Two" );
        allowableValues.add( "List Item Three" );
        allowableValues.add( "List Item Four" );
        allowableValues.add( "List Item Five" );
        allowableValues.add( "List Item Six" );
        allowableValues.add( "List Item Seven" );
        allowableValues.add( "List Item Eight" );
        final ListDescriptor< String > listItemsDescriptor = new ListDescriptor( allowableValues, ControlType.CHOICE_BOX );
        randomProperties.add( new ObservableProperty(  ControlType.LIST.toString(), 
                ControlType.LIST, SimplePropertyType.STRING, listItemsDescriptor ) );
        
        randomProperties.add( new ObservableProperty(  ControlType.PASSWORD_FIELD.toString(),  
                ControlType.PASSWORD_FIELD, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.PROGRESS_BAR.toString(),  
                ControlType.PROGRESS_BAR, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.RADIO_BUTTON.toString(),   
                ControlType.RADIO_BUTTON, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.SCROLL_BAR.toString(), 
                ControlType.SCROLL_BAR, SimplePropertyType.DOUBLE, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.SLIDER.toString(), 
                ControlType.SLIDER,     SimplePropertyType.STRING, stringPropertyDescriptor ) );
        randomProperties.add( new ObservableProperty(  ControlType.TEXT_AREA.toString(), 
                ControlType.TEXT_AREA, SimplePropertyType.STRING, stringPropertyDescriptor ) );
        
        
        final StringDescriptor stringTextFieldPropertyDescriptor   = new StringDescriptor( );
        stringTextFieldPropertyDescriptor.addAutoCompleteSuggestions( "Sallly", "Sells", "Sea","Shells", "by","the", "Shore" );
        randomProperties.add( new ObservableProperty(  ControlType.TEXT_FIELD.toString(), 
                ControlType.TEXT_FIELD, SimplePropertyType.STRING, stringTextFieldPropertyDescriptor ) );
        
        randomProperties.add( new ObservableProperty(  ControlType.TOGGLE_BUTTON.toString(), 
                ControlType.TOGGLE_BUTTON, SimplePropertyType.STRING, stringPropertyDescriptor ) );

        TreeDescriptor demoTreeDescriptor   = null;
        try {
            demoTreeDescriptor   = new TreeDescriptor( "Demo Control Tree" );
            demoTreeDescriptor.constructPath( "Demo Control Tree:Other:Tools:Programming:Programming Tools".split( ":" ) );
            demoTreeDescriptor.addBranch( "Demo Control Tree", "True or False" );
            demoTreeDescriptor.addBranch( "Demo Control Tree", "Text" );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Text" , "TextField", ControlType.TEXT_FIELD, "Text");
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Text" , "TextArea", ControlType.TEXT_AREA, "lots of text are possible in this type of text input control");
            demoTreeDescriptor.addBranch( "Demo Control Tree", "Buttons" );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Buttons" , "Button", ControlType.BUTTON, "press me");
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Buttons" , "FileChooser", ControlType.FILE_CHOOSER, "Pick a file");
            demoTreeDescriptor.addLeaf( "Demo Control Tree:True or False" , "CheckBox", ControlType.CHECK_BOX, "True or False");
            demoTreeDescriptor.addBranch( "Demo Control Tree", "Charts" );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Charts" , "PieChart", ControlType.CHART_PIE , sliceData );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Charts" , "BarChart", ControlType.CHART_BAR, barSeries );
            demoTreeDescriptor.addBranch( "Demo Control Tree", "Lists" );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Lists" , "ChoiceBox", ControlType.CHOICE_BOX, listData );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Lists" , "List", ControlType.LIST, listData );
            demoTreeDescriptor.addLeaf( "Demo Control Tree:Lists" , "ComboBox", ControlType.COMBO_BOX, listData );
        } catch (TreeDescriptorException ex) {
            Logger.getLogger(TestRecords.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /*
        TreeNode< Button >buttonNode        = new TreeNode<  >( "Button", new Button( "Press Me" ) );
        TreeNode< TextField >textFieldNode  = new TreeNode<  >( "TextField", new TextField( "Text" ) );
        TreeNode< CheckBox >checkBoxNode  = new TreeNode<  >( "CheckBox", new CheckBox( "True or False" ) );
        TreeNode< RadioButton >radioButtonNode  = new TreeNode<  >( "RadioButton", new RadioButton( "On or Off" ) );
        TreeNode< ToggleButton >toggleButtonNode  = new TreeNode<  >( "ToggleButton", new ToggleButton( "Toggle it" ) );
        TreeNode< ChoiceBox >choiceBoxNode  = new TreeNode<  >( "ChoiceBox", new ChoiceBox( FXCollections.observableArrayList(  "One", "Two", "Three", "Four", "Five", "..." ) ) );
        TreeNode< ListView >listViewNode  = new TreeNode<  >( "ListView", new ListView( FXCollections.observableArrayList(  "First", "Second", "Third", "Fourth", "Fifth", "..." ) ) );
        TreeNode< ComboBox >comboBoxNode  = new TreeNode<  >( "ComboBox", new ComboBox( FXCollections.observableArrayList(  "List Item One", "List Item Two", "List Item Three", "List Item Four", "..." ) ) );
        TreeNode< PasswordField >passwordFieldNode  = new TreeNode<  >( "PasswordField", new PasswordField( ) );
        TreeNode< ColorPicker >colorPickerNode  = new TreeNode<  >( "ColorPicker", new ColorPicker( Color.BEIGE ) );
        TreeNode< Slider >sliderNode  = new TreeNode<  >( "Slider", new Slider( 0.0, 85.0, 100.0 ) );
            
        
        final TreeDescriptor treePropertyDescriptor = new TreeDescriptor( "Control Demo Tree" );
        try {
            treePropertyDescriptor.addBranch( "Control Demo Tree", "Controls" );
            treePropertyDescriptor.addLeaf( "Control Demo Tree:Controls", "Button", new Button(  ), "Press me" );
        } catch (TreeDescriptorException ex) {
            Logger.getLogger(TestRecords.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        
        randomProperties.add( new ObservableProperty(  ControlType.TREE_VIEW.toString(), 
                ControlType.TREE_VIEW,  SimplePropertyType.STRING, demoTreeDescriptor ) );
    }
}
